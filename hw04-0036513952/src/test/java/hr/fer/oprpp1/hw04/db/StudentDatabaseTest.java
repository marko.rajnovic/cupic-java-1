package hr.fer.oprpp1.hw04.db;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;



public class StudentDatabaseTest {
	
	
	@Test 
	public void readFromFileTest() {
		BufferedReader br = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream("/" + "problem2/database.txt")));
		
		
		try {
			while(br.readLine() != null) {
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		assertTrue(true);
	}
	
	@Test
	public void parseDatabaseTrueFilterTest() {
		BufferedReader br = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream("/" + "problem2/database.txt")));
		
		List<String> lines = new ArrayList<>();
		try {

			String line = "";
			while((line = br.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		StudentDatabase db = new StudentDatabase(lines);
		
		//System.out.println(db.forJMBAG("0000000001"));
		
		IFilter filterTrue = (record) -> true;
		
		List<StudentRecord> records = db.filter(filterTrue);

		assertEquals(63, records.size());
	}
	
	@Test
	public void parseDatabaseFalseFilterTest() {
		BufferedReader br = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream("/" + "problem2/database.txt")));
		
		List<String> lines = new ArrayList<>();
		try {

			String line = "";
			while((line = br.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		StudentDatabase db = new StudentDatabase(lines);
		
		IFilter filterFalse = (record) -> false;
		
		List<StudentRecord> records = db.filter(filterFalse);

		assertEquals(0, records.size());
	}
	
	@Test
	public void ComparisonOperatorTest() {
		String value1 = "Ivan";
		String value2 = "Ivana";
		
		IComparisonOperator op;
		
		op = ComparisonOperators.EQUALS;
		assertFalse(op.satisfied(value1, value2));
		
		op = ComparisonOperators.NOT_EQUALS;
		assertTrue(op.satisfied(value1, value2));
		
		op = ComparisonOperators.LESS;
		assertTrue(op.satisfied(value1, value2));
		
		op = ComparisonOperators.LESS_OR_EQUALS;
		assertTrue(op.satisfied(value1, value2));
		
		op = ComparisonOperators.GREATER;
		assertFalse(op.satisfied(value1, value2));
		
		op = ComparisonOperators.GREATER_OR_EQUALS;
		assertFalse(op.satisfied(value1, value2));
		
		op = ComparisonOperators.LIKE;
		assertTrue(op.satisfied(value2, value1 + "*"));
	}
	
	@Test
	public void FieldValueGettersTest() {
		StudentRecord record = new StudentRecord("000000001", "Mirko", "Mirik", 1);
		assertEquals("Mirko", FieldValueGetters.FIRST_NAME.get(record));
		assertEquals("Mirik", FieldValueGetters.LAST_NAME.get(record));
		assertEquals("000000001",FieldValueGetters.JMBAG.get(record));

	}
	
	@Test
	public void ConditionalExpressionTest(){
		ConditionalExpression expr = new ConditionalExpression(
				 FieldValueGetters.LAST_NAME,
				 "Mir*",
				 ComparisonOperators.LIKE
				);
		StudentRecord record = new StudentRecord("000000001", "Mirko", "Mirik", 1);
		boolean recordSatisfies = expr.getComparisonOperator().satisfied(
				 expr.getFieldGetter().get(record), // returns lastName from given record
				 expr.getStringLiteral() // returns "Bos*"
				);
		assertTrue(recordSatisfies);
	}
	
	@Test
	public void DBLexerTest() {
		QueryLexer lex = new QueryLexer("query firstName>\"A\" aNd lastName LIKE \"B*ć\"");
		
		assertEquals(new Token(Type.IDN, "firstName"), lex.nextToken());
		assertEquals(new Token(Type.OP, ">"), lex.nextToken());
		assertEquals(new Token(Type.STRING, "A"), lex.nextToken());
		assertEquals(new Token(Type.OP, "and"), lex.nextToken());
		assertEquals(new Token(Type.IDN, "lastName"), lex.nextToken());
		assertEquals(new Token(Type.OP, "like"), lex.nextToken());
		assertEquals(new Token(Type.STRING, "B*ć"), lex.nextToken());
	}
	
	@Test
	public void DBParserTest() {
		QueryParser qp1 = new QueryParser("query jmbag =\"0123456789\" ");
		
		assertTrue(qp1.isDirectQuery());
		assertEquals("0123456789", qp1.getQueriedJMBAG());
		assertEquals(1,qp1.getQuery().size());
		
		/*System.out.println("isDirectQuery(): " + qp1.isDirectQuery()); // true
		System.out.println("jmbag was: " + qp1.getQueriedJMBAG()); // 0123456789
		System.out.println("size: " + qp1.getQuery().size()); // 1*/
		
		QueryParser qp2 = new QueryParser("query jmbag=\"0123456789\" and lastName>\"J\"");
		assertFalse(qp2.isDirectQuery());
		assertThrows(IllegalArgumentException.class,() -> qp2.getQueriedJMBAG());
		assertEquals(2, qp2.getQuery().size());
		
		/*System.out.println("isDirectQuery(): " + qp2.isDirectQuery()); // false
		// System.out.println(); // would throw!
		System.out.println("size: " + qp2.getQuery().size()); // 2*/

	}
}
	
