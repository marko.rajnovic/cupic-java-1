package hr.fer.oprpp1.custom.collections;

/**
 * An interface which allows the user to go through each element 
 * of a Collection one by one. 
 * @author Marko
 *
 */
public interface ElementsGetter<T> {
	
	/**
	 * Checks if the collection has an element after the one the ElementsGetter 
	 * is currently referencing. 
	 * @return true if there is next element, false otherwise
	 */
	public boolean hasNextElement();
	
	/**
	 * Returns the object the ElementsGetter is currently pointing to and 
	 * moves to the next element in Collection
	 */
	public T getNextElement();
	
	
	/**
	 * Processes the remaining elements that were not passed into ElementsGetter yet
	 * @param processor
	 */
	void processRemaining(Processor<? super T> processor);
	
}
