package hr.fer.oprpp1.custom.collections;

public interface List<T> extends Collection<T>{
	
	/**
	 * Gets the element at given index.
	 * @param index
	 * @return
	 */
	Object get(int index);
	
	/**
	 * Insert element at given position, pushing all consecutive elements one position forward
	 * @param value
	 * @param position
	 */
	void insert(T value, int position);
	
	/**
	 * Returns the index of the first appearance of an element. Returns -1 if element is not found
	 * @param value
	 * @return
	 */
	int indexOf(Object value);
	
	/**
	 * removes element at given position
	 */
	void remove(int index);

}
