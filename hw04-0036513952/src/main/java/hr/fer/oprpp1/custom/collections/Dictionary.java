package hr.fer.oprpp1.custom.collections;

public class Dictionary<K,V>{
	ArrayIndexedCollection<K> colKey; //collection of keys
	ArrayIndexedCollection<V> colVal; //collection of values
	
	public Dictionary() {
		colKey = new ArrayIndexedCollection<>();
		colVal = new ArrayIndexedCollection<>();
	}
	
	/**
	 * checks if the Dictionary is empty
	 * @return
	 */
	public boolean isEmpty() {
		return colKey.isEmpty();
	}
	
	public void clear() {
		colKey.clear();
		colVal.clear();
	}
	
	/**
	 * Puts a key-value pair in the dictionary. If the dictionary already contains a pair with the current key value, that value will be replaced and returned. Returns null otherwise
	 */
	public V put(K key, V value) {
		V oldValue = null;
		
		//checks if the collection of keys contains this key
		if(colKey.contains(key)) {
			//if it does, just place the old value in the oldValue variable, and replace the old value
			int index = colKey.indexOf(key);
			oldValue = colVal.get(index);
			colVal.remove(index);
			colVal.insert(value, index);
			
		//if it doesn't, add the key-value pair
		} else {
			colKey.add(key);
			colVal.add(value);
		}
		
		
		return oldValue;
	}
	
	/**
	 * Retrieves the value with the given key. If the value isn't found, returns null
	 */
	public V get(Object key) {
		V value = null;
		
		//if there is a value with this key, find its' index and return it
		if(colKey.contains(key)) {
			int index = colKey.indexOf(key);
			value = colVal.get(index);
		}
		
		return value;
	}
	
	/**
	 * removes the key-value pair that is associated with the key. Returns the value removed. If there was no value, returns null
	 */
	public V remove(K key) {
		V value = null;
		
		//if there is a value with this key, remove the key value pair from their respective collections
		if(colKey.contains(key)) {
			int index = colKey.indexOf(key);
			value = colVal.get(index);
			colVal.remove(index);
			colKey.remove(index);
		}
		
		return value;
	}
	
	/**
	 * returns the size of the Dictionary
	 */
	int size() {
		return colKey.size();
	}
}
