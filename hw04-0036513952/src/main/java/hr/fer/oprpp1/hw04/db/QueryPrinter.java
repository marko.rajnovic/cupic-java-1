package hr.fer.oprpp1.hw04.db;

import java.util.List;

/**
 * Class which formats and prints the StudentRecords we give it
 * @author Marko
 *
 */
public class QueryPrinter {
	int jmbagWidth = 12;
	int gradeWidth = 3;
	int lastNameWidth;
	int firstNameWidth;
	List<StudentRecord> records;
	
	
	public QueryPrinter(List<StudentRecord> records) {
		int maxLastName = 0;
		int maxFirstName = 0;
		
		//we go through each record and get the largest name and last name length
		for(StudentRecord record: records) {
			
			if(record.getLastName().length() > maxLastName) {
				maxLastName = record.getLastName().length();
			}
			
			if(record.getFirstName().length() > maxFirstName) {
				maxFirstName = record.getFirstName().length();
			}
			
		}
		
		this.records = records;
		
		//we add 2 to them because each one is separated from the wall by 1 spae
		lastNameWidth = maxLastName + 2;
		firstNameWidth = maxFirstName + 2;
	}
	
	/**
	 * prints all the records which the QueryParser recieved in constructor
	 */
	public void printRecords() {
		printHeader();
		
		for(StudentRecord record : records) {
			printSingleRecord(record);
		}
		
		printHeader();
		System.out.println("Records selected: " + records.size());
	}
	
	/**
	 * prints a single record, nicely formatted
	 * @param record
	 */
	private void printSingleRecord(StudentRecord record) {
		String space = " ";
		String jmbag = record.getJmbag();
		String lastName = record.getLastName();
		String firstName = record.getFirstName();
		int grade = record.getFinalGrade();
		String s = "| " + jmbag + " | " + lastName + space.repeat(lastNameWidth - lastName.length() - 1) + "| " + firstName + space.repeat(firstNameWidth - firstName.length() - 1) + "| " + grade + " |";	
		System.out.println(s);
	}
	
	/**
	 * Prints the header or footer of the query table
	 */
	private void printHeader() {
		String equals = "=";
		String header = "+" + equals.repeat(jmbagWidth) + "+" + equals.repeat(lastNameWidth) + "+" + equals.repeat(firstNameWidth) + "+" + equals.repeat(gradeWidth) + "+";
		System.out.println(header);
	}
}
