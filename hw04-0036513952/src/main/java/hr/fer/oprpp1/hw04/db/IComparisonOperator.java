package hr.fer.oprpp1.hw04.db;

public interface IComparisonOperator {
	/**
	 * Checks if the 2 strings satisfy a certain condition
	 * @param value1 
	 * @param value2
	 * @return boolean which says if 2 strings satisfy a certain condition
	 */
	public boolean satisfied(String value1, String value2);
}
