package hr.fer.oprpp1.hw04.db;

/**
 * Class which stores the variable which we want to check, with what string literal value and with which operator
 * @author Marko
 *
 */
public class ConditionalExpression {
	IFieldValueGetter getter;
	String literal;
	IComparisonOperator op;
	
	/**
	 * 
	 * @param getter which stores which variable of the StudentRecord we want to check
	 * @param literal which stores by what value we want to check that variable
	 * @param op which stores the operator which we want to use to check
	 */
	public ConditionalExpression(IFieldValueGetter getter, String literal, IComparisonOperator op) {
		this.getter = getter;
		this.literal = literal;
		this.op = op;
	}
	
	//GETTERS AND SETTERS
	
	public IFieldValueGetter getFieldGetter() {
		return getter;
	}
	public String getStringLiteral() {
		return literal;
	}
	public IComparisonOperator getComparisonOperator() {
		return op;
	}
}
