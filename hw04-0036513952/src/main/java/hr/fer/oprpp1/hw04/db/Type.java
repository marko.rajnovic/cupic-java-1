package hr.fer.oprpp1.hw04.db;

/**
 * Enum which stores types of tokens of the QueryLexer
 * @author Marko
 *
 */
public enum Type{
	IDN,
	OP,
	STRING,
	EOF,
	ERROR
}