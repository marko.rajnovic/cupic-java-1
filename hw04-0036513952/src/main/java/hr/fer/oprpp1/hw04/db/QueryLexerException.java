package hr.fer.oprpp1.hw04.db;

/**
 * Exception which is thrown by the QueryLexer
 * @author Marko
 *
 */
public class QueryLexerException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4777342568330701395L;


	public QueryLexerException() {
		super();
	}

	public QueryLexerException(String message) {
		super(message);
	}


	public QueryLexerException(Throwable cause) {
		super(cause);
	}


	public QueryLexerException(String message, Throwable cause) {
		super(message, cause);
	}


	public QueryLexerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
