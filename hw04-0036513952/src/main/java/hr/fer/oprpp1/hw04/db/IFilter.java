package hr.fer.oprpp1.hw04.db;

public interface IFilter {
	/**
	 * Checks if a student record satisfies a certain condition
	 * @param record which we want to check
	 * @return boolean that says if a student record accepts certain condition
	 */
	public boolean accepts(StudentRecord record);

}
