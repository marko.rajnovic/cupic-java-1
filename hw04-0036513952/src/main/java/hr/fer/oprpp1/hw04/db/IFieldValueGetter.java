package hr.fer.oprpp1.hw04.db;

public interface IFieldValueGetter {
	
	/**
	 * Returns a certain variable from a StudentRecord
	 * @param record from which we want to return a certain variable
	 * @return variable which we specified in the implementation of the getter
	 */
	public String get(StudentRecord record);

}
