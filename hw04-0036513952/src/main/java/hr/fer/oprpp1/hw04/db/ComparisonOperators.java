package hr.fer.oprpp1.hw04.db;

public class ComparisonOperators {
	
	//Lambda assigned operators which we use to check 2 strings for certain conditions
	public static final IComparisonOperator LESS = (value1, value2) -> value1.compareTo(value2) < 0;
	public static final IComparisonOperator LESS_OR_EQUALS = (value1,value2) -> value1.compareTo(value2) <= 0;
	public static final IComparisonOperator GREATER = (value1, value2) -> value1.compareTo(value2) > 0;
	public static final IComparisonOperator GREATER_OR_EQUALS = (value1, value2) -> value1.compareTo(value2) >= 0;
	public static final IComparisonOperator EQUALS = (value1, value2) -> value1.compareTo(value2) == 0;
	public static final IComparisonOperator NOT_EQUALS = (value1, value2) -> value1.compareTo(value2) != 0;	
	public static final IComparisonOperator LIKE = (value1,value2) -> {
		value2 = value2.replace("*", ".*");
		
		return value1.matches(value2);
		};	
}
