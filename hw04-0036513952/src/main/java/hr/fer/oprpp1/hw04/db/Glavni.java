package hr.fer.oprpp1.hw04.db;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Main class which runs the query engine
 * @author Marko
 *
 */
public class Glavni {

	public static void main(String[] args) {
		
		//reads the DB from file;
		List<String> lines = readFromFile();
		
		//fills the db object
		StudentDatabase db = new StudentDatabase(lines);
		
		//starts the console input stream
		System.out.print("> ");
		//Enter data using BufferReader 
        BufferedReader reader =  
                   new BufferedReader(new InputStreamReader(System.in));
        String line = null;
        
        //tries to read line
		try {
			line = reader.readLine();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
		//while the read line isn't an exit command, execute commands from the query engine
        while(!line.equals("exit")) {

        	try {
            	//parses the inputted query or throws error if the input is wrong
	        	QueryParser parser = new QueryParser(line);        	
	        	List<StudentRecord> records = new ArrayList<>();
	        	
	        	//if the query is just JMBAG matching, we use the O(1) method of retrieval
				if(parser.isDirectQuery()) {
				 records.add(db.forJMBAG(parser.getQueriedJMBAG()));
				 QueryPrinter printer = new QueryPrinter(records);
				 printer.printRecords();
				} 
				
				//if it isn't, we need to filter the query
				else {
					//add all the records which satify the condition
					 for(StudentRecord r : db.filter(new QueryFilter(parser.getQuery()))) {
						records.add(r);
					 }
					 
					 //print those records which satisfy the condition
					 QueryPrinter printer = new QueryPrinter(records);
					 printer.printRecords();
				}

	        } catch(RuntimeException e) {
	        	//if there was a user error, catch it and print the error description
        		System.out.println(e.getMessage());
        	} finally {
        		//ask for next query irregardless if there was an error
		        try {
		        	System.out.print("> ");
					line = reader.readLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        }
        
        System.out.println("Goodbye!");
	}
	
	/**
	 * Reads the database from resource file. 
	 * @return
	 */
	private static List<String> readFromFile() {
		BufferedReader br = new BufferedReader(new InputStreamReader(
                Glavni.class.getResourceAsStream("/" + "problem2/database.txt")));
		
		List<String> lines = new ArrayList<>();
		try {

			String line = "";
			while((line = br.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lines;
	}

}
