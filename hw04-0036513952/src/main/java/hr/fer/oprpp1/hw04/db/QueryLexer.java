package hr.fer.oprpp1.hw04.db;

public class QueryLexer {
	Token token;
	String[] raw;
	int currentIndex;
	
	/**
	 * Constructs a lexer from a line of string input
	 * @param input
	 */
	public QueryLexer(String input) {
		
		//Delimiters which will keep the elements we split by. We split based on operators.
		String delimiter1 = "((?<==)|(?==))";
		String delimiter2 = "((?<=[aA][nN][dD])|(?=[aA][nN][dD]))";
		String delimiter3 = "((?<=<)|(?=<))";
		String delimiter4 = "((?<=>)|(?=>))";
		String delimiter5 = "((?<=<=)|(?=>=))";
		String delimiter6 = "((?<=[lL][iI][kK][eE])|(?=[lL][iI][kK][eE]))";
		
		if(!input.startsWith("query")) {
			throw new QueryLexerException("Query must start with \"query\" string");
		}
		
		//Removes the "query" elements of the string
		input = input.replace("query", "");
		
		//splits the string with operators acting as delimiters
		String[] s = input.split(delimiter1 + "|" + delimiter2 + "|" + delimiter3 + "|" + delimiter4 + "|" + delimiter5 + "|" + delimiter6);
		
		//put the operators "and" and "like" into lowercase
		for(int i = 0; i < s.length; i++) {
			s[i] = s[i].trim();
			if(s[i].toLowerCase().equals("and") || s[i].toLowerCase().equals("like")) {
				s[i] = s[i].toLowerCase();
			}
		}
		
		this.raw = s;
		currentIndex = 0;
	}
	
	public Token nextToken() {
		
		//if we reach the end of the raw input array and we do not stop, an error will be thrown
		if(this.token != null && this.token.getType() == Type.EOF) {
			throw new QueryLexerException("EOF already reached");
		}
		
		//if we have reached the end of input array, an EOF token will be generated
		if(raw.length <= currentIndex) {
			this.token = new Token(Type.EOF, "");
			return this.token;
		}
		
		//get the type of token
		Type t = getTypeOnCurrentIndex();
		
		//if there was an error, an ERROR token will be generated and we will leave the lexer
		if(t == Type.ERROR) {
			throw new QueryLexerException("Invalid input entered in lexer");
		}
		
		//return the new token
		this.token = new Token(t, raw[currentIndex]);
		++currentIndex;
		return this.token;
	}
	
	/**
	 * Gets the token type of the current token text
	 * @return
	 */
	private Type getTypeOnCurrentIndex() {
		String currentRawToken = raw[currentIndex];
		Type t = null;
		
		//check if the token is an operator or idetificator
		switch(currentRawToken) {
			case "=" :
			case ">" :
			case "<" :
			case ">=" :
			case "<=" :
			case "and" :
			case "like" :
				t = Type.OP;
				break;
			case "firstName" :
			case "lastName" :
			case "jmbag" :
				t= Type.IDN;
				break;
		}
		
		//checks if we got a string. If we did, remove the string delimiters
		if(currentRawToken.startsWith("\"") && currentRawToken.endsWith("\"")) {
			t = Type.STRING;
			raw[currentIndex] = raw[currentIndex].replace("\"", "");
		}
		
		if(t != null) {
			return t;
		}
		
		//if none of those were given, return an error type
		return Type.ERROR;
	}

	/**
	 * @return last generated token
	 */
	public Token getToken() {
		return this.token;
	}
}
