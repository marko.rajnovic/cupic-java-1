package hr.fer.oprpp1.hw04.db;

import java.util.List;

public class QueryFilter implements IFilter{
	List<ConditionalExpression> exprs;
	
	public QueryFilter(List<ConditionalExpression> exprs) {
		this.exprs = exprs;
	}
	

	@Override
	public boolean accepts(StudentRecord record) {
		boolean accepted = true;
		
		//Goes through all the conditional expressions and returns true if all are satisfied
		for(ConditionalExpression expr : exprs) {
			boolean recordSatisfies = expr.getComparisonOperator().satisfied(
					 expr.getFieldGetter().get(record),
					 expr.getStringLiteral()
					);
			accepted = accepted && recordSatisfies;
		}
		
		return accepted;
	}

}
