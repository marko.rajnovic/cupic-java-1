package hr.fer.oprpp1.hw04.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
 

/**
 * Stores the StudentDatabase records and an index of JMBAGs which allows us to retrieve each one in O(1) time
 * @author Marko
 *
 */
public class StudentDatabase {
	ArrayList<StudentRecord> listRecords;
	HashMap<String, StudentRecord> hashRecords;
	
	
	/**
	 * Takes all the records and parses them into the DB
	 */
	public StudentDatabase(List<String> records) {
		listRecords = new ArrayList<>();
		hashRecords = new HashMap<>();
		
		parseRecords(records);
	}
	
	/**
	 * Store each record as a StudentRecord and checks if the grade is between 1 and 5
	 */
	private void parseRecords(List<String> records) {
		for(String record : records) {
			
			//splits the record on the spaces
			String[] s = record.split("\\s+");
			
			int grade = Integer.parseInt(s[3]);
			
			//throws error if the grade isn't between 1 and 5
			if(grade < 1 || grade > 5) {
				throw new IllegalArgumentException("Grade must be between 1 and 5");
			}
			
			//create a new student record
			StudentRecord r = new StudentRecord(s[0], s[2], s[1], grade);
			
			//checks if there already is an entry with certain JMBAG
			if(hashRecords.containsKey(s[0])) {
				throw new IllegalArgumentException("There cannot be 2 same JMBAGs");
			}
			
			//place the JMBAG into a HashMap so we can retrieve the record in O(1) time
			hashRecords.put(s[0], r);
			
			//place the record into a list
			listRecords.add(r);
		}
	}
	
	/**
	 * @param jmbag which we search by
	 * @return StudentRecord with certain jmbag. Null if there is no student with that JMBAG
	 */
	public StudentRecord forJMBAG(String jmbag) {
		return hashRecords.get(jmbag);
	}
	
	/**
	 * filters the StudentDatabase
	 * @param filter with which we want to check all the records
	 * @return list of student records which satisfy the condition given by the filter
	 */
	public List<StudentRecord> filter(IFilter filter){
		List<StudentRecord> toReturn = new ArrayList<>();
		
		for(StudentRecord r : listRecords) {
			if(filter.accepts(r)) {
				toReturn.add(r);
			}
		}
		
		return toReturn;
	}

}
