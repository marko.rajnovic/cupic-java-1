package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.Vector2D;

public class RotateCommand implements Command{
	double angle;
	
	
	
	
	public RotateCommand(double angle) {
		this.angle = angle * Math.PI / 180.0;
	}

	
	/**
	 * Rotates the current TurleState by angle in radians given in the constructor
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		Vector2D angleVector = ctx.getCurrentState().getAngle();
		angleVector.rotate(angle);
	}

}
