package hr.fer.zemris.lsystems.impl;

import hr.fer.oprpp1.custom.collections.ObjectStack;

/**
 * Class which we use to store TurtleStates on a stack
 * @author Marko
 *
 */
public class Context {
	ObjectStack<TurtleState> stack;
	
	
	/**
	 * Create an empty stack
	 */
	public Context() {
		stack = new ObjectStack<>();
	}
	
	/**
	 * @return TurtleState on top of stack
	 */
	public TurtleState getCurrentState() {
		return stack.peek();
	}
	
	/**
	 * 
	 * @param state to be pushed on top of stack
	 */
	public void pushState(TurtleState state) {
		stack.push(state);
	}
	
	/**
	 * Remove state from top of stack
	 */
	public void popState() {
		stack.pop();
	}

}
