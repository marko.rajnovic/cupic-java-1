package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.lsystems.impl.Vector2D;

public class SkipCommand implements Command{
	private double step;
	
	
	
	public SkipCommand(double step) {
		this.step = step;
	}


	/**
	 * Skips x units the TurtleState would usually draw if the DrawCommand was given instead and puts the TurtleState at the new position
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState state = ctx.getCurrentState();
		Vector2D position = state.getPosition();
		Vector2D angle = state.getAngle();
		Vector2D angleScaled = angle.scaled(step * state.getEffLength());
		Vector2D endPosition = position.added(angleScaled);
		state.setPosition(endPosition);
	}
	
}
