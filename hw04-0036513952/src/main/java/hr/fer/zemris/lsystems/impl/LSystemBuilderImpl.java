package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

import hr.fer.oprpp1.custom.collections.Dictionary;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.commands.ColorCommand;
import hr.fer.zemris.lsystems.impl.commands.DrawCommand;
import hr.fer.zemris.lsystems.impl.commands.PopCommand;
import hr.fer.zemris.lsystems.impl.commands.PushCommand;
import hr.fer.zemris.lsystems.impl.commands.RotateCommand;
import hr.fer.zemris.lsystems.impl.commands.ScaleCommand;
import hr.fer.zemris.lsystems.impl.commands.SkipCommand;

/**
 * Class which we use to build an LSystem
 * @author Marko
 *
 */
public class LSystemBuilderImpl implements LSystemBuilder{
	//Dictionaries which we use to store commands and productions
	Dictionary<Character, Command> commands;
	Dictionary<Character, String> productions;
	
	double unitLength = 0.1;
	double unitLengthDegreeScaler = 1;
	Vector2D origin = new Vector2D(0,0);
	double angle = 0;
	String axiom = "";
	
	
	/**
	 * Creates empty LSystemBuilderImpl
	 */
	public LSystemBuilderImpl() {
		this.commands = new Dictionary<>();
		this.productions = new Dictionary<>();
	}
	
	/**
	 * Build an LSystem from assigned parametres;
	 */
	@Override
	public LSystem build() {
		
		/**
		 * Anonymous class 
		 */
		LSystem system = new LSystem(){
			
			/**
			 * Draw the desired fractal at desired elvel
			 * @param arg0 level at which we want to construct the fractal
			 * @param arg1 painter which we will use
			 */
			@Override
			public void draw(int arg0, Painter arg1) {
				
				//Create new context
				Context ctx = new Context();
				
				//Create an angle vector of length 1
				Vector2D angleVector = new Vector2D(1,0);
				
				//rotate it by desired value
				angleVector.rotate(angle);
				
				//Create a new turtle state and push it on the context
				TurtleState state = new TurtleState(origin, angleVector , Color.BLACK, unitLength * Math.pow(unitLengthDegreeScaler, arg0));
				ctx.pushState(state);
				
				//generate a string representation of the fractal
				String s = generate(arg0);
				
				//change the color if there is a command to do so
				if(commands.get('G') != null) {
					commands.get('G').execute(ctx,arg1);
				}

				//Execute commands in the order they were given in the string representation of the fractal
				for(char c : s.toCharArray()) {
					if(commands.get(c) != null) {
						Command command = commands.get(c);
						command.execute(ctx, arg1);
					}
				}
			}
			
			/**
			 * Go through each character of the axiom and generate a string of productions based on the level at which we want to draw the fractal
			 */
			@Override
			public String generate(int arg0) {
				String generated = new String();
				
				//generate the productions from the axiom
				for(char c : axiom.toCharArray()) {
					generated += recursionGen(c, arg0);
				}
				
				return generated;
			}
			
			/**
			 * Recursively go through the elements to the desired level
			 * @param elem character which we want to recursively produce productions for
			 * @param depth to which we want to go
			 * @return
			 */
			private String recursionGen(Character elem, int depth) {
				//if we reached the lowest depth or if there are no productions for an element, return the character
				if(depth == 0 || productions.get(elem) == null) {
					return String.valueOf(elem);
				} 
				
				//get the production for character
				String production = productions.get(elem);
				
				//sum up the productions by recursively going to the lowest depth
				String productionSum = new String();
				for(char c: production.toCharArray()) {
					productionSum += recursionGen(c, depth - 1);
				}
				
				return productionSum;
			}
			
		};
		return system;
	}

	/**
	 * Configure the LSystemBuilder from text by using certain keywords
	 */
	@Override
	public LSystemBuilder configureFromText(String[] arg0) {
		
		//Go through the whole String array element by element
		for(String sLine : arg0) {
			
			//split the line by splitting on whitespaces
			String[] s = sLine.split("\\s+");
			
			//the keyword is always first so we can decide what to do by looking at it
			switch(s[0]) {
				case "origin":
					setOrigin(Double.parseDouble(s[1]), Double.parseDouble(s[2]));
					break;
				case "angle":
					setAngle(Double.parseDouble(s[1]));
					break;
				case "unitLength":
					setUnitLength(Double.parseDouble(s[1]));
					break;
				case "unitLengthDegreeScaler":
					
					//this will be set as x/y so we have to manipulate the strings a bit better
					//if the length is 3, we know that the "/" character is fused, so we need to remove it from the string that has it
					if(s.length == 3) {
						s[1] = s[1].replace("/", "");
						s[2] = s[2].replace("/", "");
						setUnitLengthDegreeScaler(Double.parseDouble(s[1]) / Double.parseDouble(s[2]));
					}
					
					//if the length is 3, we know that the "/" character is not fused, so we can just take the 2 numbers and divide them
					if(s.length == 4) {
						setUnitLengthDegreeScaler(Double.parseDouble(s[1]) / Double.parseDouble(s[3]));
					}
					break;
					
				case "command":
					
					//commands can have 1 parameter or no parametres so we have to have 2 cases
					//if length is 3, the command has no parametres
					if(s.length == 3) {
						registerCommand(s[1].charAt(0), s[2]);
					}
					
					//if the length is 4, there is 1 parameter which we need to pass into the "registerCommand" function
					if(s.length == 4) {
						registerCommand(s[1].charAt(0), s[2] + " " + s[3]);
					}
					break;
					
				case "axiom":
					setAxiom(s[1]);
					break;
				case "production":
					registerProduction(s[1].charAt(0), s[2]);
					break;
				}
			
		}
		return this;
	}

	/**
	 * Create a command from the text input we get
	 */
	@Override
	public LSystemBuilder registerCommand(char arg0, String arg1) {
		//split the command from its parameter if it has one
		String[] args = arg1.split(" ");
		Command command = null;
		double val;
		
		//check the type of the command and if it has a parameter, convert it to a number and give it to the specific command
		switch(args[0]) {
			case "draw":
				val = Double.parseDouble(args[1]);
				command = new DrawCommand(val);
				break;
			case "skip":
				val = Double.parseDouble(args[1]);
				command = new SkipCommand(val);
				break;
			case "scale":
				val = Double.parseDouble(args[1]);
				command = new ScaleCommand(val);
				break;
			case "rotate":
				val = Double.parseDouble(args[1]);
				command = new RotateCommand(val);
				break;
			case "push":
				command = new PushCommand();
				break;
			case "pop":
				command = new PopCommand();
				break;
			case "color":
				Color color = Color.decode("0x" + args[1].toLowerCase());
				command = new ColorCommand(color);
				break;
		}
		
		//insert the command in the list of commands
		commands.put(arg0, command);
		return this;
	}

	//SETTER METHODS
	
	@Override
	public LSystemBuilder registerProduction(char arg0, String arg1) {
		productions.put(arg0, arg1);
		return this;
	}

	@Override
	public LSystemBuilder setAngle(double arg0) {
		this.angle = arg0 * Math.PI / 180.0;
		return this;
	}

	@Override
	public LSystemBuilder setAxiom(String arg0) {
		this.axiom = arg0;
		return this;
	}

	@Override
	public LSystemBuilder setOrigin(double arg0, double arg1) {
		this.origin = new Vector2D(arg0, arg1);
		return this;
	}

	@Override
	public LSystemBuilder setUnitLength(double arg0) {
		this.unitLength = arg0;
		return this;
	}

	@Override
	public LSystemBuilder setUnitLengthDegreeScaler(double arg0) {
		this.unitLengthDegreeScaler = arg0;
		return this;
	}
	
}
