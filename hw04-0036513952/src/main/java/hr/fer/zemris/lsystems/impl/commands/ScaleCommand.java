package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

public class ScaleCommand implements Command{
	double factor;

	public ScaleCommand(double factor) {
		super();
		this.factor = factor;
	}
	
	/**
	 * Scales the effective length the TurtleState draws at by a factor given in the constructor
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState state = ctx.getCurrentState();
		state.setEffLength(state.getEffLength() * factor);
	}
	
	
}
