package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

/**
 * Class we use to store the current position, angle, color and effective length of the "pen" we use to draw the fractal
 * @author Marko
 *
 */
public class TurtleState {
	private Vector2D position;
	private Vector2D angle;
	private Color color;
	private double effLength;
	
	//GETTERS AND SETTERS
	
	public Vector2D getPosition() {
		return position;
	}

	public void setPosition(Vector2D position) {
		this.position = position;
	}

	public Vector2D getAngle() {
		return angle;
	}

	public void setAngle(Vector2D angle) {
		this.angle = angle;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public double getEffLength() {
		return effLength;
	}

	public void setEffLength(double effLength) {
		this.effLength = effLength;
	}

	//CONSTRUCTOR
	public TurtleState(Vector2D position, Vector2D angle, Color color, double effLength) {
		this.position = position;
		this.angle = angle;
		this.color = color;
		this.effLength = effLength;
	}
	
	/**
	 * hard copies the TurtleState
	 * @return
	 */
	public TurtleState copy() {
	   
       return new TurtleState(this.position.copy(), this.angle.copy(), this.color, this.effLength);
	}
	
}
