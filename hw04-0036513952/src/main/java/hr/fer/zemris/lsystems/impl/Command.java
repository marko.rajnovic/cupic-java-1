package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.Painter;

/**
 * A command interface so we execute call different commands on the context  
 * @author Marko
 *
 */
public interface Command {
	
	/**
	 * Execute a command on the context and draw something with the painter if necessary
	 * @param ctx or context is a stack of TurtleStates that we can manipulate to draw a certain fractal
	 * @param painter which we use to draw on the screen
	 */
	void execute(Context ctx, Painter painter);
}
