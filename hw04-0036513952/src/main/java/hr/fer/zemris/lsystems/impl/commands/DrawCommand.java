package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.lsystems.impl.Vector2D;

public class DrawCommand implements Command{
	private double step;
	
	
	
	public DrawCommand(double step) {
		this.step = step;
	}


	/**
	 * Draw a line that is x units long, where x i the step that was given in the constructor. The turtle will now be at this new position
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState state = ctx.getCurrentState();
		Vector2D position = state.getPosition();
		Vector2D angle = state.getAngle();
		Vector2D angleScaled = angle.scaled(step * state.getEffLength());
		Vector2D endPosition = position.added(angleScaled);
		
		painter.drawLine(position.getX(), position.getY(), endPosition.getX(),
				endPosition.getY(), state.getColor(), (float) state.getEffLength());
		
		
		state.setPosition(endPosition);
	}
	
}
