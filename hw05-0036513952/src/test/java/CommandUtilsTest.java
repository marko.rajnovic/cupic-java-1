import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.hw05.shell.ShellIOException;
import hr.fer.oprpp1.hw05.shell.commands.CommandUtils;

public class CommandUtilsTest {
	
	
	@Test
	public void splitQuotedArgumentsTest(){
		String args = "\"ab cd\"  e f \"g h\"";
		
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(4);
		
		String[] splitted = CommandUtils.splitAndCheckArgs(args, allowedNo);
		assertEquals("ab cd", splitted[0]);
		assertEquals("e", splitted[1]);
		assertEquals("f", splitted[2]);
		assertEquals("g h", splitted[3]);
	}
	
	@Test
	public void splitQuotedArgumentsErrorTest(){
		String args = "\"abcd c d e";
		
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(1);
		allowedNo.add(2);
		
		assertThrows(ShellIOException.class,  () -> CommandUtils.splitAndCheckArgs(args, allowedNo));
	}
}
