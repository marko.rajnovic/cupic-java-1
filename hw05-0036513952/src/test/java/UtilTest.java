import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.hw05.crypto.Util;

public class UtilTest{
	
	@Test
	public void byteToHexTest() {
		
		byte[] byteArray = new byte[2];
		
		byteArray[0] = 5;
		byteArray[1] = 10;
		
		String s = Util.byteToHex(byteArray);
		
		assertEquals(s, "050a");
	}
	
	@Test
	public void hexToByteTest() {
		String hex = "aa";
		
		byte[] b = Util.hexToByte(hex);
		assertEquals(b[0], -86);
	}
}
