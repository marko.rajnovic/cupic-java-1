package hr.fer.oprpp1.hw05.shell.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellIOException;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

public class HelpShellCommand implements ShellCommand{
	String name;
	List<String> description;
	
	
	public HelpShellCommand() {
		name = "help";
		description = new ArrayList<String>();
		
		description.add("If started with no arguments, it list names of all supported commands.");
		description.add("If started with single argument, it must print name and the description of selected command (or print");
		description.add("appropriate error message if no such command exists).");
	}

	/**
	 * If started with no arguments, it list names of all supported commands.
	 * If started with single argument, it must print name and the description of selected command (or print
	 * appropriate error message if no such command exists).
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		//add the allowed number of arguments
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(0);
		allowedNo.add(1);
		
		//check if the arguments are valid and split them
		String[] cleanArgs = CommandUtils.splitAndCheckArgs(arguments, allowedNo);
		SortedMap<String, ShellCommand> commands = env.commands();
		
		//if we had no args list all the commands
		if(cleanArgs[0].equals("")) {

			for(ShellCommand command : commands.values()) {
				env.writeln(command.getCommandName());
			}
			
		} else {
			//otherwise, if we had an argument, and it is a proper command, write its description to the console
			
			ShellCommand command = commands.get(cleanArgs[0]);
			
			if(command == null) {
				throw new ShellIOException("Command " + cleanArgs[0] + " doesn't exist");
			}
			
			env.writeln(command.getCommandName());
			env.writeln("-----");
			for(String line : command.getCommandDescription()) {
				env.writeln(line);
			}
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public List<String> getCommandDescription() {
		// TODO Auto-generated method stub
		return description;
	}

}
