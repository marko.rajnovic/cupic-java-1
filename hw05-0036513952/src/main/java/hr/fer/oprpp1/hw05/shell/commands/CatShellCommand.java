package hr.fer.oprpp1.hw05.shell.commands;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellIOException;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

public class CatShellCommand implements ShellCommand{
	String name;
	List<String> description;
	
	
	public CatShellCommand() {
		description = new ArrayList<String>();
		
		name = "cat";
		description.add("Command cat takes one or two arguments. The first argument is path to some file and is mandatory. The");
		description.add("second argument is charset name that should be used to interpret chars from bytes. If not provided, a default");
		description.add("platform charset is used (see java.nio.charset.Charset class for details). This command opens");
		description.add("given file and writes its content to console.");
	}
	
	/**
	 * Command cat takes one or two arguments. The first argument is path to some file and is mandatory. The
	 * second argument is charset name that should be used to interpret chars from bytes. If not provided, a default
	 * platform charset is used (see java.nio.charset.Charset class for details). This command opens
	 * given file and writes its content to console.
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		//add the allowed number of arguments
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(1);
		allowedNo.add(2);
		
		//separate the args and see if it is the allowed number of arguments
		String[] cleanArgs = CommandUtils.splitAndCheckArgs(arguments, allowedNo);
		
		Path path = Paths.get(cleanArgs[0]);
		CommandUtils.checkIfFilesExist(path);
		Charset set;
		
		//if we have 2 arguments, the second one is the charset, so we must assign it from string. if there is only 1 argument, assign the default charset
		if(cleanArgs.length == 2) {
			
			try {
				set = Charset.forName(cleanArgs[1]);
			} catch (RuntimeException e) {
				throw new ShellIOException(e);
			}
			
		} else {
			set = Charset.defaultCharset();
		}
		
		//read the lines from file
		try(BufferedReader br = new BufferedReader(
				new java.io.InputStreamReader(
						new BufferedInputStream(
								Files.newInputStream(path)), set));) {
			String line = new String();
			while((line = br.readLine()) != null) {
				//write the lines on the console
				env.writeln(line);
			}
		} catch (IOException e) {
			throw new ShellIOException("Failed to open input stream");
		} 
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return name;
	}

	@Override
	public List<String> getCommandDescription() {
		// TODO Auto-generated method stub
		return description;
	}

}
