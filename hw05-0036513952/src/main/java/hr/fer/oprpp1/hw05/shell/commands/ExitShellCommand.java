package hr.fer.oprpp1.hw05.shell.commands;

import java.util.ArrayList;
import java.util.List;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

public class ExitShellCommand implements ShellCommand{
	String name;
	List<String> description ;
		
	public ExitShellCommand() {
		name = "exit";
		description = new ArrayList<String>();
		
		description.add("Ends the program.");
	}
	
	/**
	 * Ends the program.
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(0);
		
		CommandUtils.splitAndCheckArgs(arguments, allowedNo);
		return ShellStatus.TERMINATE;
	}

	@Override
	public String getCommandName() {
		return name;
	}

	@Override
	public List<String> getCommandDescription() {
		return description;
	}

}
