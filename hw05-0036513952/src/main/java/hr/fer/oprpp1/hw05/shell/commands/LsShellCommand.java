package hr.fer.oprpp1.hw05.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellIOException;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

public class LsShellCommand implements ShellCommand{
	String name;
	List<String> description;
	
	
	public LsShellCommand() {
		name = "ls";
		description = new ArrayList<String>();
		
		description.add("Command ls takes a single argument – directory – and writes a directory listing (not recursive).");
		
	}
	
	/**
	 * Command ls takes a single argument – directory – and writes a directory listing (not recursive).
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		//add the allowed number of arguments
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(1);
		
		
		//check if the arguments are valid and split them
		String[] cleanArgs = CommandUtils.splitAndCheckArgs(arguments, allowedNo);
		
		Path p = Paths.get(cleanArgs[0]);
		
		//see if these files exist
		CommandUtils.checkIfFilesExist(p);
		
		//open the stream of paths
		try(Stream<Path> files = Files.list(p);) {
			
			//for each file in the stream, do the following
			files.forEach((e)->{
				//determine the date in proper format
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				BasicFileAttributeView faView = Files.getFileAttributeView(
						e, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS
						);
				
				//read the attributes
				BasicFileAttributes attributes = null;
				try {
					attributes = faView.readAttributes();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				FileTime fileTime = attributes.creationTime();
				String formattedDateTime = sdf.format(new Date(fileTime.toMillis()));
				
				String metadata = new String();
				
				//get the metadata
				if(Files.isDirectory(e)) {
					metadata = "d";
				} else {
					metadata = "-";
				}
				
				if(Files.isReadable(e)) {
					metadata += "r";
				} else {
					metadata += "-";
				}
				
				if(Files.isWritable(e)) {
					metadata += "w";
				} else {
					metadata += "-";
				}
				
				if(Files.isExecutable(e)) {
					metadata += "x";
				} else {
					metadata += "-";
				}
				
				//get the file size
				long s = 0;
				try {
					s = Files.size(e);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				//format the output
				String size = String.format("%10d ", s);
				
				String result = metadata + size + formattedDateTime + " " + e.getFileName();
				
				//write the output
				env.writeln(result);
			});
		} catch (IOException e) {
			throw new ShellIOException("Failed to open input stream");
		} 
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return name;
	}

	@Override
	public List<String> getCommandDescription() {
		return description;
	}

}
