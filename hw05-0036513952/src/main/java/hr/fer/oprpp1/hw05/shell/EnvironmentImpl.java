package hr.fer.oprpp1.hw05.shell;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.oprpp1.hw05.shell.commands.CatShellCommand;
import hr.fer.oprpp1.hw05.shell.commands.CharsetsShellCommand;
import hr.fer.oprpp1.hw05.shell.commands.CopyShellCommand;
import hr.fer.oprpp1.hw05.shell.commands.ExitShellCommand;
import hr.fer.oprpp1.hw05.shell.commands.HelpShellCommand;
import hr.fer.oprpp1.hw05.shell.commands.HexDumpShellCommand;
import hr.fer.oprpp1.hw05.shell.commands.LsShellCommand;
import hr.fer.oprpp1.hw05.shell.commands.MkdirShellCommand;
import hr.fer.oprpp1.hw05.shell.commands.SymbolShellCommand;
import hr.fer.oprpp1.hw05.shell.commands.TreeShellCommand;

public class EnvironmentImpl implements Environment{
	Character promptSymbol;
	Character moreLinesSymbol;
	Character multilineSymbol;
	SortedMap<String, ShellCommand> commands;
	
	/**
	 * Initiate the Environment
	 */
	public EnvironmentImpl() {
		promptSymbol = '>';
		moreLinesSymbol = '\\';
		multilineSymbol = '|';
		
		//initialize the commands
		commands = new TreeMap<>();
		commands.put("cat", new CatShellCommand() );
		commands.put("charsets", new CharsetsShellCommand());
		commands.put("copy", new CopyShellCommand());
		commands.put("help", new HelpShellCommand());
		commands.put("hexdump", new HexDumpShellCommand());
		commands.put("ls", new LsShellCommand());
		commands.put("mkdir", new MkdirShellCommand());
		commands.put("symbol", new SymbolShellCommand());
		commands.put("tree", new TreeShellCommand());
		commands.put("exit", new ExitShellCommand());
		commands = Collections.unmodifiableSortedMap(commands);
	}
	
	@Override
	public String readLine() throws ShellIOException {
		String input = null;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			
			//writes the prompt symbol and then asks for console input
			write(String.valueOf(promptSymbol + " "));
			input = br.readLine();
			
			//while we have input that end with a symbol that asks for more input, continue reading
			while(input.endsWith(String.valueOf(moreLinesSymbol))) {
				write(String.valueOf(multilineSymbol + " "));
				input = input + " " + br.readLine();
			}
		} catch (IOException e) {
			throw new ShellIOException("An error ocurred while reading a line");
		}
		
		//clean the input from all line ending signs
		input = input.replace(String.valueOf(moreLinesSymbol), "");
		return input;
	}

	@Override
	public void write(String text) throws ShellIOException {
		System.out.print(text);
	}

	@Override
	public void writeln(String text) throws ShellIOException {
		write(text);
		System.out.print("\n");
	}

	@Override
	public SortedMap<String, ShellCommand> commands() {
		return Collections.unmodifiableSortedMap(commands);
	}

	@Override
	public Character getPromptSymbol() {
		return promptSymbol;
	}

	@Override
	public void setPromptSymbol(Character promptSymbol) {
		this.promptSymbol = promptSymbol;
	}

	@Override
	public Character getMoreLinesSymbol() {
		return moreLinesSymbol;
	}

	@Override
	public void setMoreLinesSymbol(Character moreLinesSymbol) {
		this.moreLinesSymbol = moreLinesSymbol;
	}

	@Override
	public Character getMultilineSymbol() {
		return multilineSymbol;
	}

	@Override
	public void setMultilineSymbol(Character multilineSymbol) {
		this.multilineSymbol = multilineSymbol;
	}




}
