package hr.fer.oprpp1.hw05.shell.commands;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

public class CopyShellCommand implements ShellCommand{
	String name;
	List<String> description ;
	
	
	public CopyShellCommand() {
		name = "copy";
		description = new ArrayList<String>();
		
		description.add("The copy command expects two arguments: source file name and destination file name (i.e. paths and ");
		description.add("names). If destination file exists, the user is asked if it is allowed to overwrite it. The copy command works");
		description.add("only with files (no directories). If the second argument is directory, the shell assumes that user wants");
		description.add("to copy the original file into that directory using the original file name.");
	}
	
	
	/**
	 * The copy command expects two arguments: source file name and destination file name (i.e. paths and
	 * names). If destination file exists, the user is asked if it is allowed to overwrite it. The copy command works
	 * only with files (no directories). If the second argument is directory, the shell assumes that user wants
	 * to copy the original file into that directory using the original file name.
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		//add the allowed number of arguments
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(2);
		
		//check if the arguments are valid and split them
		String[] cleanArgs = CommandUtils.splitAndCheckArgs(arguments, allowedNo);
		
		Path p1 = Paths.get(cleanArgs[0]);
		Path p2 = Paths.get(cleanArgs[1]);
		
		//see if these files exist
		CommandUtils.checkIfFilesExist(p1, p2);
		
		byte[] source;
		
		//check if the path to the destination file is a directory. if it is, we will not replace it, but place the copy inside it
		if(Files.isDirectory(p2)) {
			p2 = p2.resolve(p1);
		}
		
		//Ask the user if he wants to overwrite the file
		boolean allowedToOverwrite = true;
		if(Files.exists(p2) && !Files.isDirectory(p2)) {
			
			env.writeln("The destination file already exists, are you allowed to overwrite? y/n");
			String answer = env.readLine();
			
			//he MUST enter "y" or "n"
			while(!(answer.toLowerCase().equals("y") || answer.toLowerCase().equals("n"))) {
				env.writeln("The destination file already exists, are you allowed to overwrite? y/n");
				answer = env.readLine();
			}
			
			if(answer.equals("y")) {
				allowedToOverwrite = true;
			} else {
				env.writeln("Cancelling copy");
				allowedToOverwrite = false;
			}
		}
		
		//if we are allowed to overwrite the file, transfer the file using the streams
		if(allowedToOverwrite) {
			try(InputStream is = Files.newInputStream(p1); OutputStream os = Files.newOutputStream(p2);){
				source = is.readNBytes(4096/8);
				while(source.length >= 1) {
					os.write(source);
					source = is.readNBytes(4096/8);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return name;
	}

	@Override
	public List<String> getCommandDescription() {
		return description;
	}

}
