package hr.fer.oprpp1.hw05.shell.commands;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellIOException;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

public class CharsetsShellCommand implements ShellCommand{
	String name;
	List<String> description ;
	
	
	public CharsetsShellCommand() {
		name = "charsets";
		description = new ArrayList<String>();
		
		description.add("Command charsets takes no arguments and lists names of supported charsets for your Java platform (see");
		description.add("Charset.availableCharsets()). A single charset name is written per line");
	}
	
	/**
	 * Command charsets takes no arguments and lists names of supported charsets for your Java platform (see
	 * Charset.availableCharsets()). A single charset name is written per line
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		
		//add the allowed number of arguments
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(0);
		
		//check if the arguments are valid and split them
		CommandUtils.splitAndCheckArgs(arguments, allowedNo);
		
		//see all the available charsets in this Java env
		SortedMap<String, Charset> charsets = Charset.availableCharsets();
		
		//write them all on the console
		for(String charset : charsets.keySet()) {
			try {
				env.writeln(charset);
			} catch (ShellIOException e) {
				e.printStackTrace();
			}
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return this.name;
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(this.description);
	}

}
