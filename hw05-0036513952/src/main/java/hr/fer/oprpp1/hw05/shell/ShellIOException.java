package hr.fer.oprpp1.hw05.shell;

/**
 * Exception which is thrown if there was an error in our shell
 * @author Marko
 *
 */
public class ShellIOException extends RuntimeException {

	private static final long serialVersionUID = -4218153564431557846L;

	public ShellIOException() {
		super();
	}

	public ShellIOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ShellIOException(String message, Throwable cause) {
		super(message, cause);
	}

	public ShellIOException(String message) {
		super(message);
	}

	public ShellIOException(Throwable cause) {
		super(cause);
	}
	
	
}
