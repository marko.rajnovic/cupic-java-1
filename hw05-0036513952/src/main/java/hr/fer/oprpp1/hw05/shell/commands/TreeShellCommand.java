package hr.fer.oprpp1.hw05.shell.commands;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellIOException;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

public class TreeShellCommand implements ShellCommand{
	String name;
	List<String> description;
	
	
	public TreeShellCommand() {
		name = "tree";
		description = new ArrayList<String>();
		
		description.add("Prints tree representation of the given directory");
	}

	/**
	 * Prints tree representation of the given directory
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		//add the allowed number of arguments
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(1);
		
		//check if args are valid
		String[] cleanArgs = CommandUtils.splitAndCheckArgs(arguments, allowedNo);
		
		Path p = Paths.get(cleanArgs[0]);
		
		//check if the file exists
		CommandUtils.checkIfFilesExist(p);
		
	     try {
	    	 //walk the file tree
			Files.walkFileTree(p, new SimpleFileVisitor<Path>(){
				 int level = 0;
				 
				 
			     @Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			    	 //add a level, and then before visiting the directory, write appropriate number of spaces and write the name of the dir
			    	 ++level;
			         System.out.println(" ".repeat(level * 2) + dir.getFileName());
			         return FileVisitResult.CONTINUE;
				}
				@Override
			     public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
			         throws IOException
			     {
					 //upon visiting the write, write appropriate number of spaces and write the name of the file
			         System.out.println(" ".repeat(level * 2) + file.getFileName());
			         return FileVisitResult.CONTINUE;
			     }
			     @Override
			     public FileVisitResult postVisitDirectory(Path dir, IOException e)
			         throws IOException
			     {
			    	 //reduce the level
			         --level;
			         return FileVisitResult.CONTINUE;
			     }
			 });
		}catch (IOException e) {
			throw new ShellIOException("Failed to open input stream");
		} 
	     
	     return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public List<String> getCommandDescription() {
		// TODO Auto-generated method stub
		return description;
	}

}
