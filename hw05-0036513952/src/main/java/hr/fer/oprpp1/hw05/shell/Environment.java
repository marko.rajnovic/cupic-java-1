package hr.fer.oprpp1.hw05.shell;

import java.util.SortedMap;

/**
 * Environment we use to model the shell 
 * @author Marko
 *
 */
public interface Environment {
	
	/**
	 * Reads a line from the console
	 * @return The line after it has been inputted in console
	 * @throws ShellIOException
	 */
	String readLine() throws ShellIOException;
	
	/**
	 * Writes a piece of text to console
	 * @param text to be printed
	 * @throws ShellIOException
	 */
	void write(String text) throws ShellIOException;
	
	/**
	 * Writes a piece of text to console and starts a new line
	 * @param text to be printed
	 * @throws ShellIOException
	 */
	void writeln(String text) throws ShellIOException;
	
	/**
	 * Returns the commands we can use in the shell
	 * @return
	 */
	SortedMap<String, ShellCommand> commands();
	
	//GETTERS AND SETTERS
	
	
	Character getMultilineSymbol();
	void setMultilineSymbol(Character symbol);
	Character getPromptSymbol();
	void setPromptSymbol(Character symbol);
	Character getMoreLinesSymbol();
	void setMoreLinesSymbol(Character symbol);
}
