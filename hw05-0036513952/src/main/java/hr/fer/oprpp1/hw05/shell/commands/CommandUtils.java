package hr.fer.oprpp1.hw05.shell.commands;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Vector;

import hr.fer.oprpp1.hw05.shell.ShellIOException;

public class CommandUtils {
	
	/**
	 * Splits the arguments and checks if the are written in a valid way. Can recieve quoted strings 
	 * @param arguments
	 * @param allowedNumberOfArguments list which contains all the allowed numbers of arguments
	 * @return clean String[] of arguments
	 */
	public static String[] splitAndCheckArgs(String arguments, List<Integer> allowedNumberOfArguments) {
		
		String[] result = null;
		
		//If there were no quotation marks, the separation of the args is simple
		if(!arguments.contains("\"")) {
			
			result = arguments.trim().split("\\s+");
			
		} else {

			//Separates the string on the " symbol and keeps it
			String delimiter = "((?<=\")|(?=\"))";
			String[] splitted = arguments.split(delimiter);
			
			Vector<String> strings = new Vector<>();
			
			try {
				
				//go through all the splitted strings
				for(int i = 0; i < splitted.length; i++) {
					
					//if we encounter a " symbol, skip it and add the next String in the sequence to the list which we convert to String[] and then return.
					//Only way something goes wrong is if there is an uneven number of quotation marks. Then the program throws an exception
					if(splitted[i].equals("\"")){
						strings.add(splitted[i+1]);
						
						if(!splitted[i+2].equals("\"")) {
							throw new ShellIOException("Pogrešan unos stringova");
						}
						i = i + 2;
					} else {
						String[] secondSplit = splitted[i].trim().split("\\s+");
						for(String s : secondSplit) {
							strings.add(s);
						}
					}
					
				}
				} catch(IndexOutOfBoundsException e) {
					
					throw new ShellIOException("Pogrešan unos stringova");
					
				}
			
			//get the resulting String[]
			result = strings.toArray(new String[strings.size()]);
		}
		
		//if we have too few or too many arguments, there will be an exception
		if(!(allowedNumberOfArguments.contains(result.length) || result.length == 1 && result[0].equals(""))) {
			throw new ShellIOException("This number of arguments isn't allowed. Use the help function to see the allowed number of arguments");
		} 
		
		return result;
	}
	
	/**
	 * Checks if the file exists. If it doesn't, throws an error
	 * @param paths
	 */
	public static void checkIfFilesExist(Path... paths) {
		for(Path p : paths) {
			if(!Files.exists(p)) {
				throw new ShellIOException("File or directory doesn't exist");
			}
		}
	}
}