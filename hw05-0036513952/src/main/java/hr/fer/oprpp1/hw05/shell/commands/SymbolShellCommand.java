package hr.fer.oprpp1.hw05.shell.commands;

import java.util.ArrayList;
import java.util.List;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellIOException;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

public class SymbolShellCommand implements ShellCommand{
	String name;
	List<String> description;
	
	
	public SymbolShellCommand() {
		name = "symbol";
		description = new ArrayList<String>();
		
		description.add("The symbol command accepts 1 or 2 parametres. The first parameter is either PROMPT,");
		description.add("MORELINES or MULTILINE. The second, if it is present, i the new value of the specified parameter");
		
	}
	
	/**
	 * The symbol command accepts 1 or 2 parametres. The first parameter is either PROMPT,
	 * MORELINES or MULTILINE. The second, if it is present, i the new value of the specified parameter
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		//add the allowed number of arguments
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(1);
		allowedNo.add(2);
		
		//check if the arguments are valid and split them
		String[] cleanArgs = CommandUtils.splitAndCheckArgs(arguments, allowedNo);
		
		//if we got only 1 arg, get the symbol
		if(cleanArgs.length == 1) {
			switch(cleanArgs[0]) {
				case "PROMPT":
					env.writeln("Symbol for PROMPT is " + "\'" + env.getPromptSymbol() + "\'");
					break;
				case "MORELINES":
					env.writeln("Symbol for MORELINES is " + "\'" + env.getMoreLinesSymbol() + "\'");
					break;
				case "MULTILINE":
					env.writeln("Symbol for MUTILINE is " + "\'" + env.getMultilineSymbol()+ "\'");
					break;
				default:
					throw new ShellIOException("No such symbol exists. Symbols are: PROMPT, MORELINES and MULTILINE");
			}
		} else if(cleanArgs.length == 2 && cleanArgs[1].length() == 1) {
			//otherwise, assign the new symbol
			Character c = cleanArgs[1].charAt(0);
			switch(cleanArgs[0]) {
			case "PROMPT":
				env.writeln("Symbol for PROMPT changed from " + "\'" + env.getPromptSymbol() + "\'" + " to " + "\'" + c + "\'");
				env.setPromptSymbol(c);
				break;
			case "MORELINES":
				env.writeln("Symbol for MORELINES changed from " + "\'" + env.getMoreLinesSymbol() + "\'" + " to " + "\'" + c + "\'");
				env.setMoreLinesSymbol(c);
				break;
			case "MULTILINE":
				env.writeln("Symbol for MULTILINE changed from " + "\'" + env.getMultilineSymbol() + "\'" + " to " + "\'" + c + "\'");
				env.setMultilineSymbol(c);
				break;
			}

		}
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return name;
	}

	@Override
	public List<String> getCommandDescription() {
		return description;
	}

}
