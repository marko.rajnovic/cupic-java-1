package hr.fer.oprpp1.hw05.shell.commands;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellIOException;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

public class HexDumpShellCommand implements ShellCommand{
	String name;
	List<String> description;
	
	
	public HexDumpShellCommand() {
		name = "hexdump";
		description = new ArrayList<String>();
		
		description.add("The hexdump command expects a single argument: file name, and produces hex-output");
		description.add(". On the right side of the output only a standard subset of characters is shown; for all other characters a");
		description.add("'.' is printed instead (i.e. replace all bytes whose value is less than 32 or greater than 127 with '.').");
		
	}
	
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		//add the allowed number of arguments
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(1);
		
		//check if the arguments are valid and split them
		String[] cleanArgs = CommandUtils.splitAndCheckArgs(arguments, allowedNo);
		
		Path path = Paths.get(cleanArgs[0]);
		
		//see if these files exist
		CommandUtils.checkIfFilesExist(path);
		
		//read from the file
		try(InputStream is = Files.newInputStream(path);){
			
			//line we are currently on. is incremented by 16, signifying how many bytes we already read
			int line = 0;
			
			//the value of the current byte being read
			int currByte = 0;
			
			//while we haven't reached EOF:
			while(currByte != -1) {
				
				//write the current value of the line element in HEX format
				env.write(String.format("%08x:", line));
				
				//the array in which we store groups of 16 bytes
				byte[] source = new byte[16];
				
				//read 16 bytes from the file
				for(int i = 0; i < 16 ; i++) {
					
					currByte= is.read();
					source[i] = (byte) currByte;
					
					//if the current byte isn't EOF, write it on the console
					if(currByte != -1) {
						
						env.write(String.format(" %02X", currByte));
						if(i == 7) {
							env.write(" |");
						}
						
					} else {
						
						//if it is, write only space characters so the vertical lines match
						env.write("   ");
						if(i == 7) {
							env.write(" |");
						}
						
					}
						
				}
				
				env.write(" | " );
				
				//now we print the UTF-8 representations of the chars.
				for(int i = 0; i < 16; i++) {
					
					//if EOF is reached, stop the writing
					if(source[i] == -1) {
						break;
					}
					
					//if the character is writable in UTF-8, write it
					if(source[i] >= 32 && source[i] <= 127) {
						
						String alpha = String.valueOf(((char) source[i]));
						env.write(alpha);
						
					} else {
						
						//otherwise, write a dot character
						env.write(".");
						
					}
				}
				
				line = line + 16;
				env.writeln("");
			
			}
			
		} catch (IOException e) {
			throw new ShellIOException("Failed to open input stream");
		} 
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return name;
	}

	@Override
	public List<String> getCommandDescription() {
		return description;
	}

}
