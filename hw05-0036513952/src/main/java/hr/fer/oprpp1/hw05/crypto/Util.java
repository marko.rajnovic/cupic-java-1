package hr.fer.oprpp1.hw05.crypto;

public class Util {
	
	/**
	 * Converts hex value to byte value
	 * @param keyText
	 * @return
	 */
	public static byte[] hexToByte(String keyText) {
		int len = keyText.length();
		
		if(len % 2 != 0) {
			throw new IllegalArgumentException("key text must be multiple of 2");
		}
		
		keyText = keyText.toLowerCase();
		
		if(!keyText.matches("[a-f0-9]*")) {
			throw new IllegalArgumentException("key contains illegal characters");
		}
		
		byte[] byteArray = new byte[len/2];
		
		for(int i = 0; i < len / 2; i++) {
			byteArray[i] = (byte) ((Character.digit(keyText.charAt(i * 2), 16) << 4) + Character.digit(keyText.charAt(i*2+1), 16));
		}
		
		return byteArray;
	}
	
	/**
	 * Converts byte to hex
	 * @param byteArray
	 * @return
	 */
	public static String byteToHex(byte[] byteArray) {
		String keyText = new String();
		
		for(byte b : byteArray) {
			keyText += String.format("%02x", b);
		}
		
		return keyText;
	}
}
