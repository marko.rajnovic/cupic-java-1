package hr.fer.oprpp1.hw05.crypto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {
	
	/**
	 * Get the sha-256 value of the filePath
	 * @param filePath
	 */
	public static void checkSha(Path filePath) {
		
		//Get the sha-256 digest from user
		String keyText = new String();
		try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in));) {
			System.out.print("Please provide expected sha-256 digest for hw05test.bin:\r\n> " );
			keyText = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//instantiate the digest class
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
			
		byte[] digest = null;
		byte[] toBeDigested = null;
		
		//read the file and digest it in chunks
		try(InputStream is = Files.newInputStream(filePath)) {
			toBeDigested = is.readNBytes(4096/8);
			while(toBeDigested.length >= 1) {
				messageDigest.update(toBeDigested, 0, toBeDigested.length);
				toBeDigested = is.readNBytes(4096/8);
			}
			
			//store the digested bytes
			digest = messageDigest.digest();	
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//convert bytes to hex values and then compare them to supplied value by user
		String digestedText = Util.byteToHex(digest);
		
		if(keyText.equals(digestedText)) {
			System.out.println("Digesting completed. Digest of hw05test.bin matches expected digest.");
		} else {
			System.out.println("Digesting completed. Digest of hw05test.bin does not match the expected digest. Digest was: " + digestedText);
		}
	}
	
	/**
	 * Class which encrypts or decrypts a given file
	 * @param filePath1
	 * @param filePath2
	 * @param encrypt
	 */
	private static void encryptOrDecrypt(Path filePath1, Path filePath2, boolean encrypt) {
		String keyText = new String();
		String ivText = new String();
		
		//get the password and initialization vector from user
		try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in));) {
			System.out.print("Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):\r\n> " );
			keyText = br.readLine();
			
			System.out.print("Please provide initialization vector as hex-encoded text (32 hex-digits):\r\n> ");
			ivText = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		SecretKeySpec keySpec = new SecretKeySpec(Util.hexToByte(keyText), "AES");
		AlgorithmParameterSpec paramSpec = new IvParameterSpec(Util.hexToByte(ivText));
		
		//initate cipher and then encrypt or decrypt the file
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);
			
			//storage containers
			byte[] encrypted = null;
			byte[] toBeEncrypted = null;
			
			//read from input file, encrypt/decrypt and then write to the output file
			try(InputStream is = Files.newInputStream(filePath1); OutputStream os = Files.newOutputStream(filePath2);){
				toBeEncrypted = is.readNBytes(4096/8);
				
				while(toBeEncrypted.length >= 1) {
					encrypted = cipher.update(toBeEncrypted, 0, toBeEncrypted.length);

					os.write(encrypted);
					toBeEncrypted = is.readNBytes(4096/8);
				}
				
				os.write(cipher.doFinal());
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				e.printStackTrace();
			} catch (BadPaddingException e) {
				e.printStackTrace();
			}
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String endMessage = new String();
		
		//write to user if they encryption/decryption succeeded
		if(encrypt) {
			endMessage = "Encryption";
		} else {
			endMessage = "Decryption";
		}
		
		endMessage = endMessage + " completed. Generated file " + filePath2.getFileName().toString() + " based on file " + filePath1.getFileName().toString();
		System.out.println(endMessage);
	}
	
	
	
	public static void main(String[] args) {
		
		//check if we have correct number of args
		if(args.length < 2 || args.length > 3) {
			System.out.println("Neispravan broj argumenata");
			return;
		}
		
		String action = args[0];
		Path filePath1 = null;
		Path filePath2 = null;
		boolean encrypt = false;
		
		//decide what we have to do based on args. Decide if we will checksha, encrypt or decrypt, and which file.
		switch(action){
			case "checksha":
					filePath1 = Paths.get(args[1]);
					checkSha(filePath1);
					return;
			case "encrypt":
				encrypt = true;
			case "decrypt":
				filePath1 = Paths.get(args[1]);
				filePath2 = Paths.get(args[2]);
				encryptOrDecrypt(filePath1, filePath2, encrypt);
		}

	}
}
