package hr.fer.oprpp1.hw05.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellIOException;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

public class MkdirShellCommand implements ShellCommand{
	String name;
	List<String> description;
	
	
	public MkdirShellCommand() {
		name = "mkdir";
		description = new ArrayList<String>();
		
		description.add("The mkdir command takes a single argument: directory name, and creates the appropriate directory structure");
		
	}

	/**
	 * The mkdir command takes a single argument: directory name, and creates the appropriate directory structure
	 */
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		//add the allowed number of arguments
		List<Integer> allowedNo = new ArrayList<Integer>();
		allowedNo.add(1);
		
		//check if the arguments are valid and split them
		String cleanArg = CommandUtils.splitAndCheckArgs(arguments, allowedNo)[0];
		
		Path p = Paths.get(cleanArg);   
		
		//create the directory structure
		try {
			Files.createDirectories(p);
		} catch (IOException e) {
			throw new ShellIOException("Failed to open input stream");
		} 

		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return name;
	}

	@Override
	public List<String> getCommandDescription() {
		return description;
	}

}
