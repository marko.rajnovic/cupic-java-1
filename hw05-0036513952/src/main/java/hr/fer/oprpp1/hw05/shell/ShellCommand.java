package hr.fer.oprpp1.hw05.shell;

import java.util.List;

/**
 * Interface which models a shell command
 * @author Marko
 *
 */
public interface ShellCommand {
	
	/**
	 * Execute the desired command located in a certain env
	 * @param env
	 * @param arguments
	 * @return ShellStatus which tells us if we should terminate the shell or not
	 */
	ShellStatus executeCommand(Environment env, String arguments);
	
	//GETTERS 
	String getCommandName();
	List<String> getCommandDescription();

}
