package hr.fer.oprpp1.hw05.shell;


import java.util.SortedMap;

public class MyShell {
	static String PROMPT = ">";
	static String MORELINES = "\\";
	static String MULTILINE = "|";
	
	
	public static void main(String[] args) {
		
		//Create the environment
		Environment env = new EnvironmentImpl();
		
		try {
			env.writeln("Welcome to MyShell v 1.0");
		} catch (ShellIOException e1) {
			e1.printStackTrace();
		}
		
		ShellStatus status = null;
		
		//get the commands
		SortedMap<String, ShellCommand> commands = env.commands();
		
		do {
			
			try {
				
				String l = null;
				
				//read a line
				l = env.readLine();
				
				//split it 
				String[] splitted = l.split("\\s+");
				
				//get the command name
				String commandName = splitted[0];
				
				//trim the command name
				splitted[0] = splitted[0].trim();
				
				//replace the command name with "" to only get the arguments
				String arguments = l.replace(splitted[0], "");
				
				//get the command with the name commandName
				ShellCommand command = commands.get(commandName);
				
				//check if such command exists and act accordingly
				if(command != null) {
					status = command.executeCommand(env, arguments);
				} else {
					env.writeln("Command doesn't exist");
				}
				
			} catch(ShellIOException e) {
				env.writeln(e.getMessage());
			}
			
		} while(status != ShellStatus.TERMINATE);
	}
}
