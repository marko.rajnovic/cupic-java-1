package hr.fer.oprpp1.hw02.prob1;


/**
 * The types of tokens of the Lexer object
 * @author Marko
 *
 */
public enum TokenType{
	EOF,
	WORD,
	NUMBER,
	SYMBOL
}
