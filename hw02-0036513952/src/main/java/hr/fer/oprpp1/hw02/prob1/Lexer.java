package hr.fer.oprpp1.hw02.prob1;


public class Lexer {
	protected char[] data; // ulazni tekst
	protected Token token; // trenutni token
	protected int currentIndex; // indeks prvog neobrađenog znaka
	protected LexerState state;
	protected boolean numericAsWord;
	
	/**
	 * Lexer accepts the text that needs to be tokenized
	 * @param text
	 */
	public Lexer(String text) {
		this.data = text.toCharArray();
		currentIndex = 0;
		this.state = LexerState.BASIC;
	};
	
	/**
	 * Generates and returns next token
	 * @throws LexerException if there is an error
	 */
	public Token nextToken() {
	
		checkIfValid();
		removeSpaces();
		
		//checks if the EOF is reached
		if(data.length <= currentIndex) {
			this.token = new Token(TokenType.EOF, null);
			return this.token;
		}
		
		
		String tempToken = new String();	
		
		//assigns a token type
		TokenType type = assignToken(data[currentIndex]); 
		
		numericAsWord = false;
			
		//iterates through the char array while there are no empty spaces and the token type is the same as the previous elements'
		while((currentIndex < data.length) && isWithoutSpaces(data[currentIndex]) && type == assignToken(data[currentIndex]) ) {
			
			//checks if the next number is escaped
			if(numericAsWord == true) {
				++currentIndex;
				numericAsWord = false;
			}
			
			//adds the current char to the string and moves the current index by 1
			tempToken = tempToken + String.valueOf(data[currentIndex]);
			++currentIndex;
			
			//if a symbol is reached, stop the current tokenizing and return the token
			if(type == TokenType.SYMBOL && state == LexerState.BASIC) {
				break;
			}
			
		}
		
		removeSpaces();
		
		//if the token type is a number, checks if it can parse it as long or double
		if(type == TokenType.NUMBER) {
			
			try {
				this.token = new Token(type, Long.parseLong(tempToken));
			} catch(NumberFormatException e) {
				throw new LexerException(e.getMessage());
			}
			
		} else {
			this.token = new Token(type, tempToken);
		}
		
		return this.token;
	}

	
	/**
	 * Checks if EOF is reached. if it is, throws an error
	 * @throws LexerException
	 */
	protected void checkIfValid() {
		if(token != null && token.getType() == TokenType.EOF) {
			throw new LexerException("EOF already reached");
		}		
	}
	
	
	/**
	 * Returns the last generated token
	 */
	public Token getToken() {
		return this.token;
	}
	
	/**
	 * returns true if the char is not '\r', '\t', '\n' or ' '
	 */
	protected boolean isWithoutSpaces(char c) {
		if(c == '\r' || c== '\t' || c == '\n' || c == ' ') {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * removes any spaces that are left between one character and the next
	 */
	protected void removeSpaces() {
		while((currentIndex < data.length) && !isWithoutSpaces(data[currentIndex])) {
			++currentIndex;
		}
	}
	
	/**
	 * Assigns an appropriate token to the current indexed character. Can look ahead to do so. Also checks if the number is escaped
	 * @param c
	 * @return
	 */
	protected TokenType assignToken(char c) {
		if(state == LexerState.BASIC) {
			
			//checks if the element is escaped properly
			if(c == '\\') {
				numericAsWord = true;
				
				if((currentIndex + 1 < data.length) && ( data[currentIndex + 1] == '\\' || assignToken(data[currentIndex + 1]) == TokenType.NUMBER)) {
					return TokenType.WORD;
				}
				throw new LexerException("Invalid escape char");
			}
			else if(Character.isLetter(c)) {
				return TokenType.WORD;
			}
			else if(Character.isDigit(c)) {
				return TokenType.NUMBER;
			} 
			else {
				return TokenType.SYMBOL;
			}
			
		} 
		else if (data[currentIndex] == '#') {
			return TokenType.SYMBOL;
		} 
		else {
			return TokenType.WORD;
		}
	}
	
	/**
	 * Sets the state of the lexer to one of two {@link LexerState}
	 * @param state
	 */
	public void setState(LexerState state) {
		if(state == null) {
			throw new NullPointerException("State cannot be null");
		}
		
		this.state = state;
	}
}
