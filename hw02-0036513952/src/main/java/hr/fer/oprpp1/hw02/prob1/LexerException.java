package hr.fer.oprpp1.hw02.prob1;

/**
 * Exception which is thrown by the Lexer object
 * @author Marko
 *
 */
public class LexerException extends RuntimeException {
	private static final long serialVersionUID = 7718828512143293558L;
	
	public LexerException() {
		super();
	}
	
	public LexerException(String message) {
		super(message);
	}
}
