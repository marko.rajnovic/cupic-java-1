package hr.fer.oprpp1.hw02.prob1;

/**
 * States of the Lexer object
 * @author Marko
 *
 */
public enum LexerState {
	BASIC, 
	EXTENDED,
}
