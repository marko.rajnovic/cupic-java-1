package hr.fer.oprpp1.hw02.prob1;

/**
 * Token of the Lexer object. Has a value and a type
 * @author Marko
 *
 */
public class Token {
	Object value;
	TokenType type;

	public Token(TokenType type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	public Object getValue(){
		return this.value;
	}
	
	public TokenType getType() {
		return this.type;
	}
	
}
