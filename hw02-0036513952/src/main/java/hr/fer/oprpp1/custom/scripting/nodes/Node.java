package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.collections.ArrayIndexedCollection;

/**
 * Generic node object which is used to construct a syntax tree
 * @author Marko
 *
 */
public class Node {
	ArrayIndexedCollection arr;
	
	String value;
	
	public Node() {
		arr = new ArrayIndexedCollection();
	}
	
	public Node(String value) {
		this.value = value;
		arr = new ArrayIndexedCollection();
	}
	
	public void addChildNode(Node child) {
		if(arr == null) {
			arr = new ArrayIndexedCollection();
		}
		
		arr.add(child);
	}
	
	public int numberOfChildren() {
		return arr.size();
	}
	
	public Node getChild(int index) {
		return (Node)arr.get(index);
	}
	
	public String printNode() {
		return new String();
	}
}
