package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Storage class for variables
 * @author Marko
 *
 */
public class ElementVariable extends Element{
	String name;
	
	public ElementVariable(String name) {
		this.name = name;
	}
	
	@Override
	public String asText() {
		return this.name;
	}
	
	
}
