package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Storage class for strings
 * @author Marko
 *
 */
public class ElementString extends Element{
	String value;
	
	public ElementString(String value) {
		this.value = value;
	}
	
	@Override
	public String asText() {
		return this.value;
	}
	
	
}
