package hr.fer.oprpp1.custom.scripting.lexer;

/**
 * Token types of the SmartLexer object
 * @author Marko
 *
 */
public enum SmartTokenType{
	CONSTANT,
	SPECIAL,
	OPERATOR,
	KEYWORD,
	IDN,
	TEXT,
	EOF,
	FUNCTION,
	STRING,
	INTEGER,
	DOUBLE
}
