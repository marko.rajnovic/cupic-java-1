package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.scripting.elems.Element;

/**
 * Node which stored dynamically allocated elements of the parser
 * @author Marko
 *
 */
public class EchoNode extends Node{
	Element[] elements;

	public EchoNode(Element[] elements) {
		this.elements = elements;
	}

	public int size() {
		return this.elements.length;
	}
	
	@Override
	public String printNode() {
		String s = new String();
		s = "{$=";
		
		for(int i = 0; i < elements.length; ++ i) {
			s = s + elements[i].asText() + " ";
		}
		s = s + "$}\n";
		return s;
	}
}
