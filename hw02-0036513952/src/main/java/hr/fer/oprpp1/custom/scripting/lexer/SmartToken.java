package hr.fer.oprpp1.custom.scripting.lexer;

/**
 * A token class used by the SmartLexer object
 * @author Marko
 *
 */
public class SmartToken{
	SmartTokenType type;
	Object value;

	public SmartToken(SmartTokenType type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	public Object getValue(){
		return this.value;
	}
	
	public SmartTokenType getType() {
		return this.type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmartToken other = (SmartToken) obj;
		if (type != other.type)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	
}
