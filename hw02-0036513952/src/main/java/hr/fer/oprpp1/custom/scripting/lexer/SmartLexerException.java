package hr.fer.oprpp1.custom.scripting.lexer;

/**
 * An exception thrown by the SmartScriptLexer class
 * @author Marko
 *
 */
public class SmartLexerException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5240435484143529186L;

	public SmartLexerException() {
		super();
	}

	public SmartLexerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SmartLexerException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmartLexerException(String message) {
		super(message);
	}

	public SmartLexerException(Throwable cause) {
		super(cause);
	}

	
}
