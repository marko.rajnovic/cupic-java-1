package hr.fer.oprpp1.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;


public class LinkedListIndexedCollection implements List {
	private int size;
	private ListNode first;
	private ListNode last;
	private long modificationCount;
	
	/**
	 * Class that is used to store each list element in a separate node. Data in it cannot be changed.
	 * @author Marko
	 *
	 */
	private static class ListNode{
		private Object data;
		private ListNode next;
		
		public ListNode(Object data) {
			this.data = data;
			this.next = null;
		}
		
		public Object getData() {
			return data;
		}
		
		public ListNode getNext() {
			return next;
		}
		public void setNext(ListNode next) {
			this.next = next;
		}
		
	}
	
	/**
	 * Instantiates an empty LinkedListIndexedCollection.
	 */
	public LinkedListIndexedCollection() {
		first = null;
		last = null;
		size = 0;
		modificationCount = 0;
	}
	
	/**
	 * Instantiates a LinkedListInexed collection from some other Collection
	 * @param other
	 */
	public LinkedListIndexedCollection(Collection other) {
		this.addAll(other);
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void add(Object value) {
		if(value == null) {
			throw new NullPointerException("value cannot be null");
		}
		
		//create new node
		ListNode node = new ListNode(value);
		
		//if the linked list is empty
		if(first == null) {
			first = node;
			last = node;
		} else {
			//the newest node added becomes last
			last.setNext(node);
			last = node;
		}
		
		++size;
		++modificationCount;
	}

	@Override
	public boolean contains(Object value) {
		ListNode currentNode = first;
		
		//go through all the nodes
		for(int i = 0; i < size; i++) {
			if(currentNode.getData().equals(value)) {
				//if the node is found, return true
				return true;
			} else {
				//go to the next node
				currentNode = currentNode.getNext();
			}
		}
		
		return false;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		
		//go to first node
		ListNode currentNode = first;
		
		//cycle through all the nodes
		for(int i = 0; i < size; i++) {
			//put the data from current node into the object array
			array[i] = currentNode.getData();
			
			//go to the next node
			currentNode = currentNode.getNext();
		}
		
		return array;
	}

	@Override
	public void clear() {
		first = null;
		last = null;
		size = 0;
		++modificationCount;
	}
	
	
	/**
	 * Returns an object at given index. 
	 * Executes in O(n) time
	 * @param index
	 * @return Object at index
	 */
	public Object get(int index) {
		if(index < 0 || index >= this.size()) {
			throw new IndexOutOfBoundsException("Index must be between inclusive 0 and size-1");
		}
		
		//go to first node
		ListNode currentNode = first;
		
		//goes through all the nodes until we reach the one at specified index
		for(int i = 0; i < index; i++) {
			currentNode = currentNode.getNext();
		}
		
		return currentNode.getData();
	}
	
	/**
	 * Inserts value at given position in an array.
	 * Does not overwrite the given value at the given position in array .
	 * The legal positions are 0 to size (both are included). 
	 * If position is invalid, IndexOutOfBoundsException will be thrown.
	 * If value is null, NullPointerException will be thrown
	 * This method is of O(n) complexity
	 * @param value
	 * @param position
	 * @throws NullPointerException
	 * @throws IndexOutOfBoundsException
	 */
	public void insert(Object value, int position) {
		if(value == null) {
			throw new NullPointerException("value cannot be null");
		}
		
		if(position < 0 || position >= this.size()) {
			throw new IndexOutOfBoundsException("Index must be between inclusive 0 and size-1");
		}
		
		//create new node
		ListNode node = new ListNode(value);
		
		//if the list is empty, add the node
		if(first == null) {
			first = node;
			last = node;
		} else {
			
			//go to first node
			ListNode currentNode = first;
			
			//go to the position just behind the node where we want to insert
			for(int i = 0; i < position - 1; i++) {
				
				//get the node that must be shifted
				currentNode = currentNode.getNext();
			}
			
			//if the shifted node is the first one
			if(currentNode == first) {
				
				//the new node becomes the first one
				first = node;
				
				//the node that was previously first is moved one slot further in the list
				node.setNext(currentNode);
				++size;
				++modificationCount;
				return;
			}
			
			//the new node sets the sets the node the index position as its next one
			node.setNext(currentNode.getNext());
			
			//the node behind the position now points to the new node
			currentNode.setNext(node);
			
			//if the node we now replaced was last, the last node becomes the new one
			if(currentNode == last) {
				last = node;
			}
		}
		++size;
		++modificationCount;
	}
	
	/**
	 * Searches the collection and returns the index of the first occurrence of the given value or -1 if the value is
	 * not found. Argument can be null and the result must be that this element is not found (since the collection
	 * can not contain null).
	 * This method is of O(n) complexity
	 * @param value
	 * @return
	 */
	public int indexOf(Object value) {
		ListNode currentNode = first;
		
		//goes through the list searching for the element with value of value
		for(int i = 0; i < size; i++) {
			if(currentNode.getData().equals(value)) {
				return i;
			}
			currentNode = currentNode.getNext();
		}
		
		//if it isn't found, return -1
		return -1;
	}
	
	/**
	 * Removes element at specified index from collection. Element that was previously at location index+1
	 * after this operation is on location index, etc. Legal indexes are 0 to size-1. In case of invalid index,
	 * IndexOutOfBoundsException is thrown.
	 * 
	 * @param index
	 * @throws IndexOutOfBoundsException
	 */
	public void remove(int index) {
		if(index < 0 || index >= this.size()) {
			throw new IndexOutOfBoundsException("Index must be between inclusive 0 and size-1");
		}
		
		//goes to first element of list
		ListNode currentNode = first;
		
		//if we are removing the first element of list
		if(index == 0) {
			//if the first element is the only element
			if(first == last) {
				first = null;
				last = null;
				size--;
				++modificationCount;
			}else {
				//if the first element isn't the only element
				first = currentNode.getNext();
				--size;
				++modificationCount;
			}
			return;
		}
		
		//go to the  element that is just before the one we want to remove
		for(int i = 1; i < index - 1; i++) {
			currentNode = currentNode.getNext();
		}
		
		ListNode nodeToRemove = currentNode.getNext();
		
		if(nodeToRemove == last) {
			last = currentNode;
		} else {
			//set the next node as the one that goes after the node we want to remove
			currentNode.setNext(nodeToRemove.getNext());
		}
		size--;
		++modificationCount;
	}
	
	@Override
	public boolean remove(Object value) {
		int index = indexOf(value);
		if(index != -1) {
			remove(index);
			return true;
		}
		
		return false;
	}
	
	private long getModificationCount() {
		return this.modificationCount;
	}
	
	
	private static class LinkedListIndexedCollectionElementsGetter implements ElementsGetter{
		private LinkedListIndexedCollection col;
		private int currentIndex;
		private long savedModificationCount;
		ListNode node;
		
		public LinkedListIndexedCollectionElementsGetter(LinkedListIndexedCollection col) {
			this.col = col;
			currentIndex = -1;
			savedModificationCount = col.getModificationCount();
			node = this.col.first;
		}
		
		/**
		 * {@inheritDoc}
		 * @return
		 */
		@Override
		public boolean hasNextElement() {
			checkModificationCount();
			
			return currentIndex < this.col.size;
		}
		
		/**
		 * {@inheritDoc}
		 * @return
		 */
		@Override
		public Object getNextElement() {
			checkModificationCount();
			
			try {
				Object data = node.getData();
				node = node.getNext();
				return data;
			} catch(IndexOutOfBoundsException e) {
				throw new NoSuchElementException("There were no more elements in the Collection");
			}
		}
		
		private void checkModificationCount() {
			if(col.getModificationCount() != this.savedModificationCount) {
				throw new ConcurrentModificationException("The collection has been modified and the action cannot be completed");
			};
		}
		
		@Override
		public void processRemaining(Processor p) {
			while(hasNextElement()) {
				p.process(getNextElement());
			}	
		}
	}
	
	@Override
	public ElementsGetter createElementsGetter() {
		return new LinkedListIndexedCollectionElementsGetter(this);
		
	}
	
	@Override
	public void addAllSatisfying(Collection col, Tester tester) {
		ElementsGetter getter =col.createElementsGetter();
		Collection temp = new ArrayIndexedCollection();
		
		while(getter.hasNextElement()) {
			Object obj = getter.getNextElement();
			if(tester.test(obj)) {
				temp.add(obj);
			}
		}
		
		this.addAll(temp);
	}


}
