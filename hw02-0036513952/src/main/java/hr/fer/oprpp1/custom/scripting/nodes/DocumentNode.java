package hr.fer.oprpp1.custom.scripting.nodes;

/**
 * Root node of the parser
 * @author Marko
 *
 */
public class DocumentNode  extends Node{

	public DocumentNode(String value) {
		super(value);
	}
	
	public String toString() {
		String sumStrings = new String();
		
		
		for(int i = 0; i < this.numberOfChildren(); i++){
			sumStrings = sumStrings + getChildString(this.getChild(i));
		}
		
		return sumStrings;
	}

	private String getChildString(Node child) {
		if(child.numberOfChildren() == 0) {
			return child.printNode();
		}
		
		String s = new String();
		s = child.printNode();
		for(int i = 0; i < child.numberOfChildren(); i++) {
			s = s + getChildString(child.getChild(i));
		}
		s = s + "{$END$}\n";
		return s;
	}
}
