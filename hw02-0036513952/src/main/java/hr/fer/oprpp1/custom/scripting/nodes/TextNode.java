package hr.fer.oprpp1.custom.scripting.nodes;

/**
 * Node for storing text elements
 * @author Marko
 *
 */
public class TextNode extends Node{

	public TextNode(String value) {
		super(value);
	}
	
	
	public String printNode() {
		return new String(value + "\n");
	}
}
