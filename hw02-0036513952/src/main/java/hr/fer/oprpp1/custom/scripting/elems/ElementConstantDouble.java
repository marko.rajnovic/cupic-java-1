package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Storage class for doubles
 * @author Marko
 *
 */
public class ElementConstantDouble extends Element{
	double value;
	
	public ElementConstantDouble(double value) {
		this.value = value;
	}
	
	@Override
	public String asText() {
		return String.valueOf(value);
	}
}
