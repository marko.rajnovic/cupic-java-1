package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Storage class for operators
 * @author Marko
 *
 */
public class ElementOperator extends Element{
	String symbol;
	
	
	public ElementOperator(String symbol) {
		this.symbol = symbol;
	}
	
	@Override
	public String asText() {
		return this.symbol;
	}
}
