package hr.fer.oprpp1.custom.collections;

/**
 * Used to test certain elements of an object
 * @author Marko
 *
 */
public interface Tester {
	boolean test(Object obj);
}
