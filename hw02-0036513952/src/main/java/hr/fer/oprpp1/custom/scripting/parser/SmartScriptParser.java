package hr.fer.oprpp1.custom.scripting.parser;

import java.util.Vector;

import hr.fer.oprpp1.custom.collections.ObjectStack;
import hr.fer.oprpp1.custom.scripting.elems.Element;
import hr.fer.oprpp1.custom.scripting.elems.ElementConstantDouble;
import hr.fer.oprpp1.custom.scripting.elems.ElementConstantInteger;
import hr.fer.oprpp1.custom.scripting.elems.ElementFunction;
import hr.fer.oprpp1.custom.scripting.elems.ElementOperator;
import hr.fer.oprpp1.custom.scripting.elems.ElementString;
import hr.fer.oprpp1.custom.scripting.elems.ElementVariable;
import hr.fer.oprpp1.custom.scripting.lexer.SmartLexerException;
import hr.fer.oprpp1.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.oprpp1.custom.scripting.lexer.SmartTokenType;
import hr.fer.oprpp1.custom.scripting.nodes.DocumentNode;
import hr.fer.oprpp1.custom.scripting.nodes.EchoNode;
import hr.fer.oprpp1.custom.scripting.nodes.ForLoopNode;
import hr.fer.oprpp1.custom.scripting.nodes.Node;
import hr.fer.oprpp1.custom.scripting.nodes.TextNode;
import hr.fer.oprpp1.hw02.prob1.LexerState;

public class SmartScriptParser {
	SmartScriptLexer lex;
	DocumentNode docNode;
	ObjectStack stack;
	
	
	public SmartScriptParser(String docBody) {
		parse(docBody);
	}
	
	
	/**
	 * Returns the root node of the parser
	 * @return
	 */
	public DocumentNode getDocumentNode() {
		return this.docNode;
	}
	
	/**
	 * Generates a syntax tree of the given text
	 * @param docBody
	 */
	private void parse(String docBody) {
		try {
		stack = new ObjectStack();
		docNode = new DocumentNode("root");
		lex = new SmartScriptLexer(docBody);
		lex.nextToken();
		stack.push(docNode);
		
		//Generate the tree until end of file is reached
		while(lex.getToken().getType() != SmartTokenType.EOF) {
			
			//if the stack got emptied, that means too many END tags were given
			if(stack.isEmpty()) {
				throw new SmartScriptParserException("Last end tag reached, but there is still code in the file");
			}
			
			//If we got a SPECIAL token, change state of the SmartLexer
			if(lex.getToken().getType() == SmartTokenType.SPECIAL) {
				String value = (String)lex.getToken().getValue();
				
				//check which one we got, {$ or $}
				if(value.equals("{$")) {
					lex.setState(LexerState.EXTENDED);
				} else {
					lex.setState(LexerState.BASIC);
				}
			}
			
			//if we got a keyword (=, FOR, or END)
			if(lex.getToken().getType() == SmartTokenType.KEYWORD) {
				String value = (String)lex.getToken().getValue();
				
				//assign a new ForLoopNode with its own elements. All nodes will be assigned to it until an END keyword is recieved
				if(value.toLowerCase().equals("for")) {
					
					//if we got an identification as first parameter, continue. Otherwise, throw an error
					if(lex.nextToken().getType() == SmartTokenType.IDN) {
						Element[] elementsOfForLoop = new Element[4];
						elementsOfForLoop[0] = new ElementVariable((String)lex.getToken().getValue());
						lex.nextToken();
						
						//The FOR loop should contain 4 elements
						for(int i = 1; i < 4; i++) {
							if(lex.getToken().getType() == SmartTokenType.DOUBLE) {
								elementsOfForLoop[i] = new ElementConstantDouble((double)lex.getToken().getValue());
							}
							else if(lex.getToken().getType() == SmartTokenType.INTEGER) {
								elementsOfForLoop[i] = new ElementConstantInteger((int)lex.getToken().getValue());
							}
							else if(lex.getToken().getType() == SmartTokenType.STRING) {
								elementsOfForLoop[i] = new ElementString((String)lex.getToken().getValue());
							}
							else if(lex.getToken().getType() == SmartTokenType.IDN){
								elementsOfForLoop[i] = new ElementVariable((String)lex.getToken().getValue());
							}
							else {
								throw new SmartScriptParserException("Invalid token in for loop or not enough tokens in for loop");
							}
							lex.nextToken();
						}
						
						//If we STILL haven't reached a SPECIAL token, that means somebody messed up!
						if(lex.getToken().getType() != SmartTokenType.SPECIAL) {
							throw new SmartScriptParserException("For loop has too many tokens");
						}
						
						//Add all the forLoop elements into the node. Push it on top of the stack
						ForLoopNode forNode = new ForLoopNode(elementsOfForLoop[0],elementsOfForLoop[1],elementsOfForLoop[2],elementsOfForLoop[3]);
						Node currentTopNode = (Node)stack.peek();
						currentTopNode.addChildNode(forNode);
						stack.push(forNode);
						
						continue;
					} else {
						throw new SmartScriptParserException("First element of FOR loop must be an identificator");
					}
					
					//if we have reached an END tag, pop a node off the stack.
				} else if (value.toLowerCase().equals("end")){
					
					lex.nextToken();
					stack.pop();
					continue;
					
					//otherwise, we have gotten an "=" tag
				} else {
					Vector<Element> elements = new Vector<Element>();
					lex.nextToken();
					
					//The "=" tag can have as many elements as it likes!
					while(lex.getToken().getType() != SmartTokenType.SPECIAL) {
						if(lex.getToken().getType() == SmartTokenType.DOUBLE) {
							elements.add(new ElementConstantDouble((double)lex.getToken().getValue()));
						}
						else if(lex.getToken().getType() == SmartTokenType.INTEGER) {
							elements.add(new ElementConstantInteger((int)lex.getToken().getValue()));
						}
						else if(lex.getToken().getType() == SmartTokenType.STRING) {
							elements.add(new ElementString((String)lex.getToken().getValue()));
						}
						else if(lex.getToken().getType() == SmartTokenType.IDN){
							elements.add(new ElementVariable((String)lex.getToken().getValue()));
						}
						else if(lex.getToken().getType() == SmartTokenType.OPERATOR){
							elements.add(new ElementOperator((String)lex.getToken().getValue()));
						}
						else if(lex.getToken().getType() == SmartTokenType.FUNCTION){
							elements.add(new ElementFunction((String)lex.getToken().getValue()));
						}
						
						lex.nextToken();
					}
					Element[] elementsArray = elements.toArray(new Element[elements.size()]);
					
					//add the echo node to its parent node
					EchoNode echo = new EchoNode(elementsArray);
					Node currentTopNode = (Node)stack.peek();
					currentTopNode.addChildNode(echo);
					continue;
				}
			}
			
			
			//If we have gotten a text token, just add it to its parent node
			if(lex.getToken().getType() == SmartTokenType.TEXT) {
				TextNode textNode = new TextNode((String)lex.getToken().getValue());
				Node currentTopNode = (Node)stack.peek();
				currentTopNode.addChildNode(textNode);
			}
			
			lex.nextToken();
		}
	}catch(SmartLexerException e) {
		throw new SmartScriptParserException(e);
	}
}
}