package hr.fer.oprpp1.custom.scripting.lexer;


import hr.fer.oprpp1.hw02.prob1.LexerException;
import hr.fer.oprpp1.hw02.prob1.LexerState;

public class SmartScriptLexer {
	protected char[] data; // ulazni tekst
	protected SmartToken token; // trenutni token
	protected int currentIndex; // indeks prvog neobrađenog znaka
	protected LexerState state;
	protected boolean escaped;
	
	/**
	 * Recieves a text to tokenize
	 * @param text
	 */
	public SmartScriptLexer(String text) {
		this.data = text.toCharArray();
		currentIndex = 0;
		this.state = LexerState.BASIC;
	};
	

	
	/**
	 * Returns the last generated token
	 * @return
	 */
	public SmartToken getToken() {
		return this.token;
	}

	
	/**
	 * Sets the state of the SmartLexer object
	 * @param state
	 */
	public void setState(LexerState state) {
		if(state == null) {
			throw new NullPointerException("State cannot be null");
		}
		
		this.state = state;
	}

	/**
	 * Generates and returns the next token of the given text
	 * @return
	 */
	public SmartToken nextToken() {
		checkIfValid();
		
		//checks if end is reached and return EOF token if it is
		if(data.length <= currentIndex) {
			this.token = new SmartToken(SmartTokenType.EOF, null);
			return this.token;
		}
		
		String tempToken = new String();	
		SmartTokenType type = assignType();
		
		//checks if a special token is recieved
		if(type == SmartTokenType.SPECIAL) {
			tempToken = String.valueOf(data[currentIndex]) + String.valueOf(data[currentIndex+1]);
			this.token = new SmartToken(type, tempToken);
			currentIndex = currentIndex + 2;
			return this.token;
		}

		//The basic state
		if(state == LexerState.BASIC) {
			type = SmartTokenType.TEXT;
			
			//if  a special symbol is reached, break at the spot of the symbol and return everything before it. Also checks if the symbol is escaped. If it is, doesn't break
			//otherwise, break at the end of file.
			while((currentIndex < data.length)) {
				if(currentIndex + 3 < data.length && !isEscaped() && data[currentIndex + 1] == '{' && data[currentIndex + 2] == '$') {
					tempToken = tempToken + data[currentIndex];
					++currentIndex;
					break;
				}
				
				if(currentIndex + 3 < data.length && isEscaped()){
					if((data[currentIndex + 1] == '{' || data[currentIndex+1] == '\\')) {
						tempToken = tempToken + String.valueOf(data[currentIndex + 1]) + String.valueOf(data[currentIndex + 2]);
						currentIndex = currentIndex + 3;
					} else {
						throw new SmartLexerException("Illegal escape made!");
					}
				} 
				tempToken = tempToken + String.valueOf(data[currentIndex]);
				++currentIndex;
			}
			
			this.token = new SmartToken(type, tempToken);
		}
		
		//If the lexer is in the extended state
		else if(state == LexerState.EXTENDED) {
			removeSpaces();
			
			//checks if a special token is recieved
			if(assignType() == SmartTokenType.SPECIAL) {
				type = assignType();
				tempToken = String.valueOf(data[currentIndex]) + String.valueOf(data[currentIndex+1]);
				this.token = new SmartToken(type, tempToken);
				currentIndex = currentIndex + 2;
				return this.token;
			}
			
			
			if(data[currentIndex] == '\\') {
				throw new SmartLexerException("Cannot escape inside tags");
			}
			
			isValidSmartToken();
			type = assignType();
			
			//If we have found a keyword (=, FOR or END) break and return it
			if(type == SmartTokenType.KEYWORD) {
				if(data[currentIndex] == '=') {
					tempToken = String.valueOf("=");
					this.token = new SmartToken(type, tempToken);
					currentIndex = currentIndex + 1;
					return this.token;
				}
				
				tempToken = String.valueOf(data[currentIndex]) + String.valueOf(data[currentIndex + 1]) + String.valueOf(data[currentIndex + 2]);
				this.token = new SmartToken(type, tempToken);
				currentIndex = currentIndex + 3;
				return this.token;
			}
			
			//If we have found a function (variable that starts with @) break and return it
			if(type == SmartTokenType.FUNCTION) {
				while((currentIndex < data.length) && isWithoutSpaces(data[currentIndex])) {
					tempToken = tempToken + String.valueOf(data[currentIndex]);
					++currentIndex;
				}
				this.token = new SmartToken(type, tempToken);
				return this.token;
			}
			
			//if we have found an operator, break and return it
			if(type == SmartTokenType.OPERATOR) {
				tempToken = String.valueOf(data[currentIndex]);
				++currentIndex;
				this.token = new SmartToken(type, tempToken);
				return this.token;
			}
			
			//if we have found a string, break and return it
			if(type == SmartTokenType.STRING) {
				while(currentIndex < data.length) {
					
					//loads the initial '"' tag
					tempToken = tempToken + String.valueOf(data[currentIndex]);
					++currentIndex;
					
					//checks if there are any escaped tags. The allowed escape tags are \\\, \", \n, \t and \r. Everything else throws exception
					if(isEscaped()) {
						if(data[currentIndex + 1] == '\\' || data[currentIndex + 1] == '"' || 
								data[currentIndex + 1] == 'n' || data[currentIndex + 1] == 't' || data[currentIndex + 1] == 'r') {
							tempToken = tempToken + String.valueOf(data[currentIndex] + 1);
							currentIndex = currentIndex + 2;
						} else {
							throw new SmartLexerException("Illegal escape made!");
						}

					}
					
					//If end of string is reached, break
					if(data[currentIndex] == '\"') {
						tempToken = tempToken + String.valueOf(data[currentIndex]);
						++currentIndex;
						removeSpaces();
						break;
					}
				}
				
				this.token = new SmartToken(type, tempToken);
				return this.token;
			}
			
			//if we have found an identification, break and return it
			if(type == SmartTokenType.IDN){
				//allows the variable to contain numbers and the _ char AFTER a letter
				while((currentIndex < data.length) && 
						(Character.isAlphabetic(data[currentIndex]) || Character.isDigit(data[currentIndex]) || data[currentIndex] == '_') && 
						isWithoutSpaces(data[currentIndex])) {
					
					tempToken = tempToken + String.valueOf(data[currentIndex]);
					++currentIndex;
				}
				this.token = new SmartToken(type, tempToken);
			}
			
			//if we have found a number, assign it either int or double, break and return it
			if(type == SmartTokenType.CONSTANT) {
			while((currentIndex < data.length) && assignType() == type && isWithoutSpaces(data[currentIndex])) {

				tempToken = tempToken + String.valueOf(data[currentIndex]);
				++currentIndex;
				}
			
			try {
				int number = Integer.parseInt(tempToken);
				type = SmartTokenType.INTEGER;
				this.token = new SmartToken(type, number);
			} catch (RuntimeException e) {
				double number = Double.parseDouble(tempToken);
				type = SmartTokenType.DOUBLE;
				this.token = new SmartToken(type, number);
			}
			return this.token;
			}
			
		}
		
		return this.token;
	}
	
	/**
	 * checks if end of file is reached. if a new token is attempted, throws an error
	 */
	protected void checkIfValid() {
		if(token != null && token.getType() == SmartTokenType.EOF) {
			throw new SmartLexerException("EOF already reached");
		}		
	}
	
	/**
	 * returns true if the char is not '\r', '\t', '\n' or ' '
	 */
	private boolean isWithoutSpaces(char c) {
		if(c == '\r' || c== '\t' || c == '\n' || c == ' ') {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Skips over any spaces and puts the index on first non-whitespace char
	 */
	private void removeSpaces() {
		while((currentIndex < data.length) && !isWithoutSpaces(data[currentIndex])) {
			++currentIndex;
		}
	}
	
	/**
	 * Checks if the IDN begins with '_'
	 */
	private void isValidSmartToken() {
		if(data[currentIndex] == '_') {
			throw new LexerException("Identificator cannot begin with _");
		}
		
	}

	/**
	 * Assigns a token type to the given char. Can look ahead to determine the type
	 * @param c
	 * @return
	 */
	private SmartTokenType assignType() {
		
		if(checkIfSpecial()) {
			return SmartTokenType.SPECIAL; 
		}
		else if(checkIfFunction()) {
			return SmartTokenType.FUNCTION;
		}
		else if(checkIfKeyword()) {
			return SmartTokenType.KEYWORD;
		}
		else if(checkIfOperator()) {
			return SmartTokenType.OPERATOR;
		}
		else if(checkIfConstant()) {
			return SmartTokenType.CONSTANT;
		}
		else if(checkIfIdn()) {
			return SmartTokenType.IDN;
		}
		else if(checkIfString()) {
			return SmartTokenType.STRING;
		}
		else {
			return SmartTokenType.TEXT;
		}
	}

	/**
	 * Checks if the token should be treated as string
	 * @param c
	 * @return
	 */
	private boolean checkIfString() {
		if(data[currentIndex] == '\"') {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the token should be treated as char
	 * @param c
	 * @return
	 */
	private boolean isEscaped() {
		if(data[currentIndex] == '\\') {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the token should be treated as function
	 * @param c
	 * @return
	 */
	private boolean checkIfFunction() {
		if(data[currentIndex] == '@') {
			return true;
		}
		return false;
	}


	/**
	 * Checks if the token should be treated as constant
	 * @param c
	 * @return
	 */
	private boolean checkIfConstant() {
		if(Character.isDigit(data[currentIndex]) || data[currentIndex] == '-' || data[currentIndex]=='.') {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the token should be treated as identification
	 * @param c
	 * @return
	 */
	private boolean checkIfIdn() {
		if(Character.isLetter(data[currentIndex])) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the token should be treated as operator
	 * @param c
	 * @return
	 */
	private boolean checkIfOperator() {
		//the && condition at the '-' operator is here because the '-' can be the beggining of int
		if(data[currentIndex] == '=' || data[currentIndex] == '+' || data[currentIndex] == '-' && !Character.isDigit(data[currentIndex + 1])  || 
				data[currentIndex]=='*' || data[currentIndex]=='/' || data[currentIndex] == '^') {
			return true;
		}
		return false;
	}

	/**
	 * Checks if the token should be treated as keyword
	 * @return
	 */
	private boolean checkIfKeyword() {
		if(data[currentIndex] == '=') {
			return true;
		}
		
		if(currentIndex +2 < data.length) {
			if((data[currentIndex] == 'F' || data[currentIndex] == 'f') && 
					(data[currentIndex +1] == 'O' || data[currentIndex + 1] == 'o')&& 
					(data[currentIndex + 2] == 'R' || data[currentIndex + 2] == 'r')) {
					return true;
			}
			else if((data[currentIndex] == 'E' || data[currentIndex] == 'e') && 
					(data[currentIndex +1] == 'N' || data[currentIndex + 1] == 'n')&& 
					(data[currentIndex + 2] == 'D' || data[currentIndex + 2] == 'd')) {
					return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the token should be treated as a special character
	 * @return
	 */
	private boolean checkIfSpecial() {
		if(currentIndex +1 < data.length) {
			if(data[currentIndex] == '{' && data[currentIndex + 1] == '$') {
				return true;
			}
			else if(data[currentIndex] == '$' && data[currentIndex + 1] == '}') {
				return true;
			}
		}
		return false;
	}
}
