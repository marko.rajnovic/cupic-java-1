package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Storage class for functions
 * @author Marko
 *
 */
public class ElementFunction extends Element{
	String name;
	
	public ElementFunction(String name) {
		this.name = name;
	}
	
	@Override
	public String asText() {
		return this.name;
	}
	
	
}
