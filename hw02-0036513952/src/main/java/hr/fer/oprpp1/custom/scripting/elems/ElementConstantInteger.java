package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Storage class for integers
 * @author Marko
 *
 */
public class ElementConstantInteger extends Element{
	int value;
	
	public ElementConstantInteger(int value) {
		this.value = value;
	}
	
	@Override
	public String asText() {
		return String.valueOf(value);
	}
}
