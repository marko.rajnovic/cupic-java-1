package hr.fer.oprpp1.custom.collections;

public interface Collection {
	
	
	/**
	 * Checks if the collection contains any objects
	 * @return true if collection contains an object, false otherwise
	 */
	public default boolean isEmpty() {
		return size() == 0;
	};
	
	/**
	 * Returns the number of currently stored objects in this collection
	 */
	public int size();

	/**
	 * Adds the given object into this collection.
	 * @param value to be added into this collection
	 */
	public void add(Object value);
	
	
	/**
	 * Returns true only if the collection contains given value, as determined by equals method.
	 * @param value which we want to check if the collection contains
	 * @return This implementation always returns false.
	 */
	public boolean contains(Object value);
	
	/**
	 * Returns true only if the collection contains given value as determined by equals method and removes
	 * one occurrence of it 
	 * @param value which we want to remove from the collection
	 * @return This implementation always returns false.
	 */
	public boolean remove(Object value);

	/**
	 * Allocates new array with size equals to the size of this collections, fills it with collection content and
	 *	returns the array. This method never returns null.
	 * @return this operation is not supported
	 * @throws UnsupportedOperationException
	 */
	public Object[] toArray();
	
	/**
	 * Method calls processor.process(.) for each element of this collection. The order in which elements
 	 * will be sent is undefined in this class.
	 * @param processor
	 */
	public default void forEach(Processor processor) {
		ElementsGetter getter = this.createElementsGetter();
		
		getter.processRemaining(processor);
	};
	
	/**
	 * Method adds into the current collection all elements from the given collection. This other collection
	 * remains unchanged.
	 * @param other
	 */
	public default void addAll(Collection other) {
		class AddAllProcessor implements Processor{
			
			public void process(Object value) {
				add(value);
			}
		}
		
		other.forEach(new AddAllProcessor());
	};
	
	/**
	 * Removes all elements from this collection.
	 */
	void clear();
	
	
	/**
	 * Create a getter for a given Collection. A collection can have
	 * several INDEPENDENT getters.
	 * @return ElementsGetter to be used on a given collection
	 */
	public ElementsGetter createElementsGetter();
	
	/**
	 * Adds all elements in Collection col to this collection that satisfy a certain condition
	 * defined in tester param
	 * @param col
	 * @param tester
	 */
	void addAllSatisfying(Collection col, Tester tester);
}
