package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Generic Element class in which all the data types which must be supported in the Parser will be stored
 * @author Marko
 *
 */
public class Element {
	
	public String asText() {
		return new String();
	};
}
