package hr.fer.oprpp1.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class ArrayIndexedCollection implements List{
	private static int DEFAULT_CAPACITY = 16;
	
	private int size = 0;
	private Object[] elements;
	private long modificationCount;
	
	/**
	 * Creates a new ArrayIndexedCollection from some other Collection and initialCapacacity of the new collection. If the 
	 * capacity is lower than the size of the other collection, the initialCapacit argument is ignored
	 * 
	 * @param other Collection
	 * @param initialCapacity
	 */
	public ArrayIndexedCollection(Collection other, int initialCapacity) {
		
		if(other == null) {
			throw new NullPointerException("other collection cannot be null");
		}
		
		if(initialCapacity < other.size()) {
			initialCapacity = other.size();
		}
		
		elements = new Object[initialCapacity];
		
		this.addAll(other);
	}
	
	/**
	 * Creates a new ArrayIndexedCollection from some other Collection and initialCapacacity of the new collection.
	 * 
	 * @param other Collection
	 * @param initialCapacity
	 */
	public ArrayIndexedCollection(Collection other) {	
		this(other, DEFAULT_CAPACITY);
	}
	
	
	/**
	 * Creates a new ArrayIndexedCollection with a capacity of initalCapacity. If someone tries to input a number <1, an error will be thrown
	 * @param initialCapacity
	 * @throws IllegalArgumentException
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		if(initialCapacity < 1) {
			throw new IllegalArgumentException("Initial capacity cannot be less than 1");
		}
		
		elements = new Object[initialCapacity];
	}
	
	/**
	 * Creates an ArrayIndexedCollection with default capacity
	 */
	public ArrayIndexedCollection() {
		this(DEFAULT_CAPACITY);
	}
	
	
	/**
	 * 
	 * Returns the object that is stored in backing array at position index. Valid indexes are 0 to size-1. If index
	 * is invalid, the method throws the IndexOutOfBoundsException.
	 * This method executes in O(1) time
	 * 
	 * @throws IndexOutOfBoundsException
	 */
	public Object get(int index) {	
		
		if(index < 0 || index > this.size() - 1) {
			throw new IndexOutOfBoundsException("Index must be between inclusive 0 and size-1");
		}
		
		return elements[index];
	}
	
	
	/**
	 * Inserts value at given position in an array.
	 * Does not overwrite the given value at the given position in array .
	 * The legal positions are 0 to size (both are included). 
	 * If position is invalid, IndexOutOfBoundsException will be thrown.
	 * If value is null, NullPointerException will be thrown
	 * This method is of O(n) complexity
	 * @param value
	 * @param position
	 * @throws NullPointerException
	 * @throws IndexOutOfBoundsException
	 */
	public void insert(Object value, int position) {
		if(value == null) {
			throw new NullPointerException("cannot add object of type null into collection");
		}
		
		if(position < 0 || position > size) {
			throw new IndexOutOfBoundsException("position cannot be less than 0 or greater than size");
		}
		
		//if limit of the elements length is reached, double the capacity
		if(elements.length == size) {
			doubleCapacity();
		}
		
		for(int i = size; i >= position; i--) {
			elements[i + 1] = elements[i];
		}
		elements[position] = value;
		size++;
		this.modificationCount++;
	}
	
	
	/**
	 * Searches the collection and returns the index of the first occurrence of the given value or -1 if the value is
	 * not found. Argument can be null and the result must be that this element is not found (since the collection
	 * can not contain null).
	 * This method is of O(n) complexity
	 * @param value
	 * @return
	 */
	public int indexOf(Object value) {
		if(value == null) {
			return -1;
		}
		
		for(int i = 0; i < size; i++) {
			if(elements[i].equals(value)) {
				return i;
			}
		}
		return -1;
	}
	
	
	/**
	 * Removes element at specified index from collection. Element that was previously at location index+1
	 * after this operation is on location index, etc. Legal indexes are 0 to size-1. In case of invalid index,
	 * IndexOutOfBoundsException is thrown.
	 * 
	 * @param index
	 * @throws IndexOutOfBoundsException
	 */
	public void remove(int index) {
		if(index < 0 || index > size) {
			throw new IndexOutOfBoundsException("index cannot be less than 0 or greater than size");
		}
		
		for(int i = index; i < size - 1; i++) {
			elements[i] = elements[i + 1];
		}
		size--;
		this.modificationCount++;
	}
	
	/**
	 * Adds the given object into this collection (reference is added into first empty place in the elements array;
	 * if the elements array is full, it is reallocated by doubling its size). The method refuses to
	 * add null as element by throwing NullPointerException 
	 * This method executes in O(1) time
	 * 
	 * @throws NullPointerException
	 */
	@Override
	public void add(Object value) {
		if(value == null) {
			throw new NullPointerException("cannot add object of type null into collection");
		}
		
		if(elements.length == size) {
			doubleCapacity();
		}
		
		elements[size++] = value;
		this.modificationCount++;
	}


	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int size() {
		return size;
	}

	/**
	 * Checks if the ArrayIndexedCollection contains a certain object. 
	 * This method executes in O(n) time
	 */
	@Override
	public boolean contains(Object value) {
		for(int i = 0; i < size; i++) {
			if(elements[i].equals(value)) {
				return true;
			}
		}
		return false;
	}

	
	@Override
	public Object[] toArray() {
		Object[] toSend = new Object[size];
		
		for(int i = 0; i < size; i++) {
			toSend[i] = elements[i];
		}
		
		return toSend;
	}
	
	
	@Override
	public void clear() {
		class ClearProcessor implements Processor{

			@Override
			public void process(Object value) {
				value = null;
			}	
		}
		
		this.forEach(new ClearProcessor());
		size = 0;
		this.modificationCount++;
	}
	
	@Override
	public boolean remove(Object value) {
		int index = indexOf(value);
		if(index != -1) {
			remove(index);
			return true;
		}
		
		return false;
	}
	
	private long getModificationCount() {
		return this.modificationCount;
	}
	
	/**
	 * Doubles capacity of the collection by allocating a new Object[] array that is double the previous one's size
	 */
	private void doubleCapacity() {
		Object[] newElements = new Object[elements.length * 2];
		
		for(int i = 0; i < elements.length; i++) {
			newElements[i] = elements[i];
		}
		elements = newElements;
	}
	
	
	
	private static class ArrayIndexedCollectionElementsGetter implements ElementsGetter{
		private ArrayIndexedCollection col;
		private int currentIndex;
		private long savedModificationCount;
		
		public ArrayIndexedCollectionElementsGetter(ArrayIndexedCollection col) {
			this.col = col;
			currentIndex = -1;
			savedModificationCount = col.getModificationCount();
		}
		
		/**
		 * {@inheritDoc}
		 * @return
		 */
		@Override
		public boolean hasNextElement() {
			checkModificationCount();
			
			return currentIndex < this.col.size;
		}
		
		/**
		 * {@inheritDoc}
		 * @return
		 */
		@Override
		public Object getNextElement() {
			checkModificationCount();
			
			try {
				return col.get(++currentIndex);
			} catch(IndexOutOfBoundsException e) {
				throw new NoSuchElementException("There were no more elements in the Collection");
			}
		}
		
		private void checkModificationCount() {
			if(col.getModificationCount() != this.savedModificationCount) {
				throw new ConcurrentModificationException("The collection has been modified and the action cannot be completed");
			};
		}

		@Override
		public void processRemaining(Processor p) {
			while(hasNextElement()) {
				p.process(getNextElement());
			}	
		}


		
	}

	@Override
	public ElementsGetter createElementsGetter() {
		return new ArrayIndexedCollectionElementsGetter(this);
		
	}

	@Override
	public void addAllSatisfying(Collection col, Tester tester) {
		ElementsGetter getter =col.createElementsGetter();
		Collection temp = new ArrayIndexedCollection();
		
		while(getter.hasNextElement()) {
			Object obj = getter.getNextElement();
			if(tester.test(obj)) {
				temp.add(obj);
			}
		}
		
		this.addAll(temp);
	}
	
};