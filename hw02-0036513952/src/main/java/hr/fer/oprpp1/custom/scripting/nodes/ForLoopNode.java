package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.scripting.elems.Element;

/**
 * Node which stores the For Loop of the parser
 * @author Marko
 *
 */
public class ForLoopNode extends Node{
	private Element variable;
	private Element startExpression;
	private Element endExpression;
	private Element stepExpression;

	public ForLoopNode(Element variable, Element startExpression, Element endExpression, Element stepExpression) {
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}
	
	
	@Override
	public String printNode() {
		String s = new String();
		s = "{$FOR " + variable.asText() +" " + startExpression.asText() + " " + endExpression.asText() + " " + stepExpression.asText() + " }\n";
		return s;
	}
	
}
