package hr.fer.oprpp1.custom.scripting.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.custom.scripting.nodes.DocumentNode;
import hr.fer.oprpp1.custom.scripting.nodes.EchoNode;
import hr.fer.oprpp1.custom.scripting.nodes.ForLoopNode;
import hr.fer.oprpp1.custom.scripting.nodes.TextNode;

public class SmartScriptParserTest {
	private String readExample(int n) {
		  try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("extra/primjer"+n+".txt")) {
		    if(is==null) throw new RuntimeException("Datoteka extra/primjer"+n+".txt je nedostupna.");
		    byte[] data = is.readAllBytes();
		    String text = new String(data, StandardCharsets.UTF_8);
		    return text;
		  } catch(IOException ex) {
		    throw new RuntimeException("Greška pri čitanju datoteke.", ex);
		  }
		}
	
	
	
	@Test
	void testExamples(){
		for(int i = 1; i <= 9; i++ ) {
			String example = readExample(i);
			
			try {
				 SmartScriptParser parser = new SmartScriptParser(example);
				 System.out.println("Example " + i + " Passed");
				} catch(SmartScriptParserException e) {
				 System.out.println(e.getMessage());
				} catch(Exception e) {
				 System.out.println("If this line ever executes, you have failed this class!");
				 System.exit(-1);
				}
		}
	}
	
	
	@Test
	void testTextNode() {
		SmartScriptParser parser = new SmartScriptParser("This should be in the text node");
		DocumentNode docNode = parser.getDocumentNode();
		assert(docNode.getChild(0) instanceof TextNode);
	}
	
	@Test
	void testForLoopNodeDistribution() {
		SmartScriptParser parser = new SmartScriptParser("This should be in first node {$FOR i 1 1 1 $} "
				+ "This should be inside FOR node {$END$} This should be in document node");
		
		DocumentNode docNode = parser.getDocumentNode();
		
		assert(docNode.getChild(0) instanceof TextNode);
		assert(docNode.getChild(1) instanceof ForLoopNode);
		assert(docNode.getChild(1).getChild(0) instanceof TextNode);
		assert(docNode.getChild(2) instanceof TextNode);
	}
	
	@Test
	void testEchoNode() {
		SmartScriptParser parser = new SmartScriptParser("{$=i @sin * 5 $}");
		
		DocumentNode docNode = parser.getDocumentNode();
		assert(docNode.getChild(0) instanceof EchoNode);
		
		EchoNode echo = (EchoNode)docNode.getChild(0);
		assertEquals(4, echo.size());
	}
	
	@Test
	void testDocumentPrinter() {
		String docBody = "This should be in first node {$FOR i 1 1 1 $} "
				+ "This should be inside FOR node {$FOR i 1 1 1 $} This should be inside second FOR node {$END$} "
				+ "This should be between nodes {$END$} This should be in document node"
				+ "{$=this is @in echo node 0 0$}";
		SmartScriptParser parser = null;
		try {
		 parser = new SmartScriptParser(docBody);
		} catch(SmartScriptParserException e) {
		 System.out.println("Unable to parse document!");
		 System.exit(-1);
		} catch(Exception e) {
		 System.out.println("If this line ever executes, you have failed this class!");
		 System.exit(-1);
		}
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = document.toString();
		System.out.println(originalDocumentBody);
	}
}
