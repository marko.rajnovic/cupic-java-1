package hr.fer.oprpp1.custom.scripting.lexer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.hw02.prob1.LexerState;

public class SmartLexerTest {

	
	@Test
	public void testTextAndForLoop() {
		SmartScriptLexer lexer = new SmartScriptLexer("This should parse as one token \n {$ FOR i 1 2 $} \n last one was FOR loop");
		
		
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.TEXT, "This should parse as one token \n "));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.SPECIAL, "{$"));
		lexer.setState(LexerState.EXTENDED);
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.KEYWORD, "FOR"));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.IDN, "i"));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.INTEGER, 1));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.INTEGER, 2));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.SPECIAL, "$}"));
		lexer.setState(LexerState.BASIC);
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.TEXT,  " \n last one was FOR loop"));
	}
	
	
	@Test
	public void testTag() {
		SmartScriptLexer lexer = new SmartScriptLexer("{$= i i*@sin \"0.000\" @decfmt $}");

		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.SPECIAL, "{$"));
		lexer.setState(LexerState.EXTENDED);
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.KEYWORD, "="));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.IDN, "i"));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.IDN, "i"));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.OPERATOR, "*"));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.FUNCTION, "@sin"));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.STRING, "\"0.000\""));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.FUNCTION, "@decfmt"));
		assertEquals(lexer.nextToken(), new SmartToken(SmartTokenType.SPECIAL, "$}"));
	}
}
