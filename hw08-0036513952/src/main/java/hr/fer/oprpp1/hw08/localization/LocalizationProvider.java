package hr.fer.oprpp1.hw08.localization;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class which controls the language we use to translate a string
 * @author Marko
 *
 */
public class LocalizationProvider extends AbstractLocalizationProvider{
	
	String language = "en";
	ResourceBundle bundle;
	private static final LocalizationProvider provider = new LocalizationProvider();
	
	private LocalizationProvider() {
		super();
		Locale locale = Locale.forLanguageTag(language);
		bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.localization.prijevodi", locale);
	}
	
	public static LocalizationProvider getInstance() {
		return provider;
	}

	@Override
	public String getString(String s) {
		return bundle.getString(s);
	}

	
	public void setLanguage(String s) {
		if(s.equals(language)) {
			return;
		}
		language = s;
		Locale locale = Locale.forLanguageTag(s);
		bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.localization.prijevodi", locale);
		fire();
	}
}
