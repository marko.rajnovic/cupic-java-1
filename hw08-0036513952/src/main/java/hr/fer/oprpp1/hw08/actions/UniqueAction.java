package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Arrays;

import javax.swing.Action;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Action used to remove duplicates from selected text
 * @author Marko
 *
 */
public class UniqueAction extends LocalizableAction{

	private static final long serialVersionUID = 1L;
	private MultipleDocumentModel multiDocument;
	JTextArea editor;
	
	public UniqueAction(MultipleDocumentModel multiDocument, ILocalizationProvider flp) {
		
		
		//localize names
		super("unique_action", "unique_action_description", flp);
		
		//initialize variables
		this.multiDocument = multiDocument;
		//add keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control F7")); 
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_T);  
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//get the text area
		final JTextArea editor = multiDocument.getCurrentDocument().getTextComponent();
		Document doc = editor.getDocument();
		int positionMark = 0;
		int positionCaret = 0;
		

		
		//try to retrieve the position caret and the position mark. calculate the length of the desired string and offset
		try {
			positionCaret = editor.getLineStartOffset(editor.getLineOfOffset(editor.getCaret().getDot()));
			positionMark = editor.getLineStartOffset(editor.getLineOfOffset(editor.getCaret().getMark()));
			int len = Math.abs(positionMark - positionCaret);
			int offset = 0;
			
			if(len!=0) {
				offset = Math.min(positionCaret,positionMark);
			} else {
				len = doc.getLength();
			}
			
			String text = doc.getText(offset, len);
			text = removeDuplicate(text);
			doc.remove(offset, len);
			doc.insertString(offset, text, null);
		} catch(BadLocationException ex) {
			ex.printStackTrace();
		}
		
	}

	private String removeDuplicate(String text) {
		//split the string into lines
		String[] splitted = text.split("\n");
			
		String[] distinct = Arrays.stream(splitted).distinct().toArray(String[]::new);
		
		String result = new String();
		
		//join the array together again with \n added to each line
		for(String s : distinct) {
			result = result + s + "\n";
		}
		return new String(result);
	}
}
