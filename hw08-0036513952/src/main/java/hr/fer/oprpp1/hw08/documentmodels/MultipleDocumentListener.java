package hr.fer.oprpp1.hw08.documentmodels;

/**
 * Listener used to observe the MultipleDocumentModel class
 * @author Marko
 *
 */
public interface MultipleDocumentListener {
	
	/**
	 * Fires if we have changed the document 
	 * @param previousModel  that will be changed from
	 * @param currentModel that we are changing to
	 */
	void currentDocumentChanged(SingleDocumentModel previousModel,SingleDocumentModel currentModel);
	
	/**
	 * Fires if a document has been added to the model
	 * @param model
	 */
	void documentAdded(SingleDocumentModel model);
	
	/**
	 * Fires if a document has been removed from the model
	 * @param model
	 */
	void documentRemoved(SingleDocumentModel model);
}

