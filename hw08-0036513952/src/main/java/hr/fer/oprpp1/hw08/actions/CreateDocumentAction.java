package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;


import javax.swing.Action;
import javax.swing.KeyStroke;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadapp.JNotepadPP;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Action used to create a new tab window and a blank document
 * @author Marko
 *
 */
public class CreateDocumentAction extends LocalizableAction{

	private static final long serialVersionUID = 1L;
	MultipleDocumentModel multiModel;
	JNotepadPP notepad;
	
	public CreateDocumentAction(MultipleDocumentModel multiModel, JNotepadPP notepad, ILocalizationProvider flp) {
		
		//localize names
		super("create_action", "create_action_description", flp);
		
		//initialize variables
		this.multiModel=multiModel;
		this.notepad = notepad;
		
		//initialize keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control W")); 
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_W); 
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//create a new document
		multiModel.createNewDocument();
	}
}
