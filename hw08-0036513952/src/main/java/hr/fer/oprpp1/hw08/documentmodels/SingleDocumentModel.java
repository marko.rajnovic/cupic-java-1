package hr.fer.oprpp1.hw08.documentmodels;

import java.nio.file.Path;

import javax.swing.JTextArea;

/**
 * Used to model a single instance of a document 
 * @author Marko
 *
 */
public interface SingleDocumentModel {
	
	/**
	 * Returns the JTextArea that contains the text that was written
	 * @return JTextArea area
	 */
	JTextArea getTextComponent();
	
	/**
	 * Returns the file path where the text was loaded from and/or saved. 
	 * @return Path if one exists, null otherwise
	 */
	Path getFilePath();
	
	/**
	 * Sets the file path of the document
	 * @param path
	 */
	void setFilePath(Path path);
	
	/**
	 * @return whether or not the document was modified
	 */
	boolean isModified();
	
	/**
	 * Sets the modified value to either true or false
	 * @param modified
	 */
	void setModified(boolean modified);
	
	/**
	 * Adds a listener to the document
	 * @param l
	 */
	void addSingleDocumentListener(SingleDocumentListener l);
	
	/**
	 * Removes a listener from the document
	 * @param l
	 */
	void removeSingleDocumentListener(SingleDocumentListener l);
}

