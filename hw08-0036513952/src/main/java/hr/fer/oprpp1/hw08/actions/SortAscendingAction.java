package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.KeyStroke;

import hr.fer.oprpp1.hw08.actions.ActionUtils.UtilSort;
import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Sort the marked lines in an ascending order
 * @author Marko
 *
 */
public class SortAscendingAction extends LocalizableAction{

	private static final long serialVersionUID = 1L;
	private MultipleDocumentModel multiDocument;
	
	public SortAscendingAction(MultipleDocumentModel multiDocument, ILocalizationProvider flp) {
		
		//localize names
		super("sort_ascending_action", "sort_ascending_action_description", flp);
		
		//initialize variables
		this.multiDocument = multiDocument;
		
		//add keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control F6")); 
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_T);  
	}		

	@Override
	public void actionPerformed(ActionEvent e) {
		ActionUtils.sort(this, multiDocument, UtilSort.ASCENDING);
	}



}
