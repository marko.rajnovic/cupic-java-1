package hr.fer.oprpp1.hw08.documentmodels;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class DefaultSingleDocumentModel implements SingleDocumentModel{
	JTextArea editor;
	boolean modified;
	Path filePath;
	List<SingleDocumentListener> listeners;
	
	
	public DefaultSingleDocumentModel(Path filePath, String textContent) {
		
		//initialize the text area
		this.filePath = filePath;
		editor = new JTextArea();
		editor.setText(textContent);
		
		//adds a listener to the document, that will track when we do anything with it and set the modified value to true
		editor.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				setModified(true);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				setModified(true);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				setModified(true);
			}
		});
		listeners = new ArrayList<>();
	}

	@Override
	public JTextArea getTextComponent() {
		return editor;
	}

	@Override
	public Path getFilePath() {
		return filePath;
	}

	@Override
	public void setFilePath(Path path) {
		
		//set the new file path
		this.filePath = path;
		
		//fire the appropriate listener
		for(SingleDocumentListener l: listeners) {
			l.documentFilePathUpdated(this);
		}
	}

	@Override
	public boolean isModified() {
		return modified;
	}

	@Override
	public void setModified(boolean modified) {
		
		//change the modified value if it is different to the previous one
		if(modified != this.modified) {
			this.modified = modified;
			
			//fire the appropriate listener
			for(SingleDocumentListener l: listeners) {
				l.documentModifyStatusUpdated(this);
			}
		}
	}

	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		listeners.add(l);
	}

	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		listeners.remove(l);
	}

}
