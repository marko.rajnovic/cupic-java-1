package hr.fer.oprpp1.hw08.localization;

/**
 * Inteface which models a LocalizationProvider
 * @author Marko
 *
 */
public interface ILocalizationProvider {
	
	/**
	 * Adds a listener that listens if a language has been changed
	 * @param l
	 */
	public void addLocalizationListener(ILocalizationListener l);
	
	/**
	 * Removes a listener from the provider
	 * @param l
	 */
	public void removeLocalizationListener(ILocalizationListener l);
	
	/**
	 * Gets the translated string
	 * @param s
	 * @return
	 */
	public String getString(String s);
}
