package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import hr.fer.oprpp1.hw08.actions.ActionUtils.UtilCase;
import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentListener;
import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.documentmodels.SingleDocumentModel;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Change all the letters marked to the lower case
 * @author Marko
 *
 */
public class ToLowercaseAction extends LocalizableAction{

		private static final long serialVersionUID = 1L;
		private MultipleDocumentModel multiDocument;
		JTextArea editor;
		
		public ToLowercaseAction(MultipleDocumentModel multiDocument, ILocalizationProvider flp) {
			
			//localize names
			super("lower_case_action", "lower_case_action_description", flp);
			
			//initialize variables
			this.multiDocument = multiDocument;
			
			//add keybindings
			this.putValue(
					Action.ACCELERATOR_KEY, 
					KeyStroke.getKeyStroke("control F4")); 
			this.putValue(
					Action.MNEMONIC_KEY, 
					KeyEvent.VK_T);  
			
			//get text editor
			if(multiDocument.getCurrentDocument() != null) {
				this.editor = multiDocument.getCurrentDocument().getTextComponent();
			}
			
			//add a caret listener that updates the text on the labels
			CaretListener caretListener = new CaretListener() {
				
				@Override
				public void caretUpdate(CaretEvent e) {
					if(editor.getCaret().getDot() - editor.getCaret().getMark() == 0) {
						ToLowercaseAction.this.setEnabled(false);
					} else {
						ToLowercaseAction.this.setEnabled(true);
					}
				}
			};
			
			//add a document listener
			multiDocument.addMultipleDocumentListener(new MultipleDocumentListener() {
				
				@Override
				public void documentRemoved(SingleDocumentModel model) {
					
				}
				
				@Override
				public void documentAdded(SingleDocumentModel model) {
				}
				
				@Override
				public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
					if(previousModel != null) {
						previousModel.getTextComponent().removeCaretListener(caretListener);
					}
					//when the document changes, switch the caret listener
					currentModel.getTextComponent().addCaretListener(caretListener);
					editor = currentModel.getTextComponent();
				}
			});
		}		

		@Override
		public void actionPerformed(ActionEvent e) {
			ActionUtils.changeCase(this, multiDocument, UtilCase.LOWER);
		}
		
}
