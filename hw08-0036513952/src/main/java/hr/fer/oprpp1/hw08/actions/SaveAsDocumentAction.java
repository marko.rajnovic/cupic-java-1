package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadapp.JNotepadPP;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Saves the file to a new location if the file is already saved
 * @author Marko
 *
 */
public class SaveAsDocumentAction extends LocalizableAction{
	private static final long serialVersionUID = 1L;
	
	private JNotepadPP notepad;
	MultipleDocumentModel multiDocument;
	ILocalizationProvider flp;
	
	public SaveAsDocumentAction(MultipleDocumentModel multiDocument, JNotepadPP notepad, ILocalizationProvider flp) {
		
		//localize names
		super("save_as_action", "save_as_action_description", flp);
		
		//initialize variables
		this.notepad = notepad;
		this.multiDocument = multiDocument;
		this.flp = flp;
		
		//add keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control shift S")); 
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_S); 
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		Path path = null;	
		
		//choose a file
		JFileChooser jfc = new JFileChooser();
		jfc.setDialogTitle(flp.getString("save_as_action_description"));
		if(jfc.showSaveDialog(notepad)!=JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(
					notepad, 
					flp.getString("nothing_saved"), 
					flp.getString("warning"), 
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		
        path = jfc.getSelectedFile().toPath();
        
        //if a file already exists at that path, ask if we want to overwrite it
        if(Files.exists(path)){
            int result = JOptionPane.showConfirmDialog(jfc,flp.getString("file_exists_override"),flp.getString("file_exists"),JOptionPane.YES_NO_CANCEL_OPTION);
            switch(result){          		
                case JOptionPane.NO_OPTION:
                    return;
                case JOptionPane.CLOSED_OPTION:
                    return;
                case JOptionPane.CANCEL_OPTION:
                    return;
                default:
                	break;
            }
        }
		multiDocument.getCurrentDocument().setFilePath(path);
		

			
		//try to save it. If we fail, notify user
		try {
			multiDocument.saveDocument(multiDocument.getCurrentDocument(), path);
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(
					notepad, 
					flp.getString("error_writing_file") + " "+path.toFile().getAbsolutePath()+". " + flp.getString("warning_unknown_state"), 
					flp.getString("error"), 
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		//file sucessfully saved!
		JOptionPane.showMessageDialog(
				notepad, 
				flp.getString("file_saved"), 
				flp.getString("info"), 
				JOptionPane.INFORMATION_MESSAGE);
	}
	

}
