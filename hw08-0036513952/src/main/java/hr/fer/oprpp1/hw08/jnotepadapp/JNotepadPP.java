package hr.fer.oprpp1.hw08.jnotepadapp;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.oprpp1.hw08.actions.CopyAction;
import hr.fer.oprpp1.hw08.actions.CreateDocumentAction;
import hr.fer.oprpp1.hw08.actions.CutAction;
import hr.fer.oprpp1.hw08.actions.DeleteSelectedPartAction;
import hr.fer.oprpp1.hw08.actions.ExitAction;
import hr.fer.oprpp1.hw08.actions.OpenDocumentAction;
import hr.fer.oprpp1.hw08.actions.PasteAction;
import hr.fer.oprpp1.hw08.actions.SaveAsDocumentAction;
import hr.fer.oprpp1.hw08.actions.SaveDocumentAction;
import hr.fer.oprpp1.hw08.actions.SortAscendingAction;
import hr.fer.oprpp1.hw08.actions.SortDescendingAction;
import hr.fer.oprpp1.hw08.actions.ToLowercaseAction;
import hr.fer.oprpp1.hw08.actions.ToUppercaseAction;
import hr.fer.oprpp1.hw08.actions.ToggleCaseAction;
import hr.fer.oprpp1.hw08.actions.UniqueAction;
import hr.fer.oprpp1.hw08.documentmodels.DefaultMultipleDocumentModel;
import hr.fer.oprpp1.hw08.localization.FormLocalizationProvider;
import hr.fer.oprpp1.hw08.localization.ILocalizationListener;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;
import hr.fer.oprpp1.hw08.localization.LocalizationProvider;

/**
 * Class which models a notepad that allows us to add tabs, write text, save, load files, change languages and much more.
 * @author Marko
 *
 */
public class JNotepadPP extends JFrame {

	private static final long serialVersionUID = 1L;
	private DefaultMultipleDocumentModel multiDocument;
	private StatusBar statusBar;
	private FormLocalizationProvider flp;
	
	AbstractAction openDocumentAction;
	AbstractAction saveDocumentAction;
	AbstractAction exitAction;
	AbstractAction deleteSelectedPartAction;
	AbstractAction toggleCaseAction;
	AbstractAction createDocumentAction;
	AbstractAction saveAsDocumentAction;
	AbstractAction copyAction;
	AbstractAction cutAction;
	AbstractAction pasteAction;
	AbstractAction lowerCaseAction;
	AbstractAction upperCaseAction;
	AbstractAction sortAscendingAction;
	AbstractAction sortDescendingAction;
	AbstractAction uniqueAction;
	
	
	public JNotepadPP() { 
		
		//initialize variables
		this.flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);
		this.multiDocument = new DefaultMultipleDocumentModel(this, flp);
		this.statusBar = new StatusBar(multiDocument, this);
		
		//initialize actions
		createDocumentAction = new CreateDocumentAction(multiDocument, this, flp);
		openDocumentAction = new OpenDocumentAction(multiDocument, this, flp);
		saveDocumentAction = new SaveDocumentAction(multiDocument, this, flp);
		saveAsDocumentAction = new SaveAsDocumentAction(multiDocument, this, flp);
		exitAction = new ExitAction(multiDocument, this, flp);
		deleteSelectedPartAction = new DeleteSelectedPartAction(multiDocument, flp);
		toggleCaseAction = new ToggleCaseAction(multiDocument, flp);
		lowerCaseAction = new ToLowercaseAction(multiDocument, flp);
		upperCaseAction = new ToUppercaseAction(multiDocument, flp);
		copyAction = new CopyAction(multiDocument, this, flp);
		cutAction = new CutAction(multiDocument, this, flp);
		pasteAction = new PasteAction(multiDocument, this, flp);
		sortAscendingAction = new SortAscendingAction(multiDocument, flp);
		sortDescendingAction = new SortDescendingAction(multiDocument, flp);
		uniqueAction = new UniqueAction(multiDocument, flp);
		
		//adds a window listener, so we can use exitAction to close the file, instead of it closing itself
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				exitAction.actionPerformed(null);
			}
			
		});
		
		//do nothing when we click the exit button. Let ExitAction take care of it
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setLocation(0, 0);
		setSize(800, 600);
		initGUI();
	}
	
	private void initGUI() {
				
		this.getContentPane().setLayout(new BorderLayout());

		createMenus();
		createToolbars();
		this.getContentPane().add(multiDocument);
		this.getContentPane().add(statusBar, BorderLayout.SOUTH);
		
	}


	private void createMenus() {

		
		//initialize the menus
		JMenuBar menuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu(flp.getString("file"));
		menuBar.add(fileMenu);
		
		fileMenu.add(new JMenuItem(createDocumentAction));
		fileMenu.add(new JMenuItem(openDocumentAction));
		fileMenu.add(new JMenuItem(saveDocumentAction));
		fileMenu.add(new JMenuItem(saveAsDocumentAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(exitAction));
		
		JMenu editMenu = new JMenu(flp.getString("edit"));
		menuBar.add(editMenu);
		editMenu.add(new JMenuItem(copyAction));
		editMenu.add(new JMenuItem(cutAction));
		editMenu.add(new JMenuItem(pasteAction));
		editMenu.add(new JMenuItem(deleteSelectedPartAction));

		
		JMenu toolsMenu = new JMenu(flp.getString("tools"));
		menuBar.add(toolsMenu);
		toolsMenu.add(new JMenuItem(toggleCaseAction));
		toolsMenu.add(new JMenuItem(lowerCaseAction));
		toolsMenu.add(new JMenuItem(upperCaseAction));
		toolsMenu.add(new JMenuItem(uniqueAction));
		
		JMenu language = new JMenu(flp.getString("language"));
		menuBar.add(language);
		initializeLanguageMenu(language);
		
		JMenu sort = new JMenu(flp.getString("sort"));
		menuBar.add(sort);
		sort.add(new JMenuItem(sortAscendingAction));
		sort.add(new JMenuItem(sortDescendingAction));
		
		this.setJMenuBar(menuBar);
		
		flp.addLocalizationListener(new ILocalizationListener() {
			
			@Override
			public void localizationChanged() {
				fileMenu.setText(flp.getString("file"));
				editMenu.setText(flp.getString("edit"));
				language.setText(flp.getString("language"));
				sort.setText(flp.getString("sort"));
				toolsMenu.setText(flp.getString("tools"));
			}
		});
	}

	private void initializeLanguageMenu(JMenu language) {
		
		//initialize the menu responsible for languages
		language.add(new LocalizableAction("en", "en_description", flp) {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage("en");
			}
		});
		
		language.add(new LocalizableAction("de", "de_description", flp) {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage("de");
			}
		});
		language.add(new LocalizableAction("hr", "hr_description", flp) {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage("hr");
			}
		});
	}

	private void createToolbars() {
		
		//initialize toolbars
		JToolBar toolBar = new JToolBar("Tools");
		toolBar.setFloatable(true);
		
		toolBar.add(new JButton(createDocumentAction));
		toolBar.add(new JButton(openDocumentAction));
		toolBar.add(new JButton(saveDocumentAction));
		toolBar.add(new JButton(saveAsDocumentAction));
		toolBar.addSeparator();
		toolBar.add(new JButton(copyAction));
		toolBar.add(new JButton(cutAction));
		toolBar.add(new JButton(pasteAction));
		toolBar.add(new JButton(deleteSelectedPartAction));
		toolBar.add(new JButton(toggleCaseAction));
		toolBar.add(new JButton(lowerCaseAction));
		toolBar.add(new JButton(upperCaseAction));
		
		this.getContentPane().add(toolBar, BorderLayout.PAGE_START);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				//start the app
				new JNotepadPP().setVisible(true);
			}
		});
	}

}
