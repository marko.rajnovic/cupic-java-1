package hr.fer.oprpp1.hw08.actions;

import java.text.Collator;
import java.util.Arrays;
import java.util.Locale;

import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentListener;
import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.documentmodels.SingleDocumentModel;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

public class ActionUtils{

	static JTextArea editor;
	
	public enum UtilCase{
		TOGGLE,
		LOWER,
		UPPER
	}
	
	public enum UtilSort{
		ASCENDING,
		DESCENDING
	}
	
	public ActionUtils() {
		
	}
	
	
	public static void changeCase(LocalizableAction action, MultipleDocumentModel multiDocument, UtilCase utilCase) {
		//get text editor
		editor = multiDocument.getCurrentDocument().getTextComponent();
		Document doc = editor.getDocument();
		
		//add a caret listener that updates the text on the labels
		CaretListener caretListener = new CaretListener() {
			
			@Override
			public void caretUpdate(CaretEvent e) {
				System.out.println("Hello!");
			}
		};
		System.out.println("Hello!");
		
		//add a document listener
		multiDocument.addMultipleDocumentListener(new MultipleDocumentListener() {
			
			@Override
			public void documentRemoved(SingleDocumentModel model) {
				
			}
			
			@Override
			public void documentAdded(SingleDocumentModel model) {
			}
			
			@Override
			public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
				if(previousModel != null) {
					previousModel.getTextComponent().removeCaretListener(caretListener);
				}
				System.out.println("Hello!");
				//when the document changes, switch the caret listener
				currentModel.getTextComponent().addCaretListener(caretListener);
				editor = currentModel.getTextComponent();
			}
		});
		
		
		//get lenth and offset
		int len = Math.abs(editor.getCaret().getDot()-editor.getCaret().getMark());
		int offset = 0;
		if(len!=0) {
			offset = Math.min(editor.getCaret().getDot(),editor.getCaret().getMark());
		} else {
			len = doc.getLength();
		}
		try {
			//try to change case
			String text = doc.getText(offset, len);
			text = updateString(text, utilCase);
			doc.remove(offset, len);
			doc.insertString(offset, text, null);
		} catch(BadLocationException ex) {
			ex.printStackTrace();
		}
	}
	
	private static String updateString(String text, UtilCase utilCase) {
		//go through each character and change its case to the opposite one
		
		String result = new String();
		if(utilCase == UtilCase.UPPER) {
			result = text.toUpperCase();
		} else if (utilCase == UtilCase.LOWER) {
			result = text.toLowerCase();
		} else if (utilCase == UtilCase.TOGGLE) {
			char[] znakovi = text.toCharArray();
			for(int i = 0; i < znakovi.length; i++) {
				char c = znakovi[i];
				if(Character.isLowerCase(c)) {
					znakovi[i] = Character.toUpperCase(c);
				} else if(Character.isUpperCase(c)) {
					znakovi[i] = Character.toLowerCase(c);
				}
			}
			result = new String(znakovi);
		}
		return result;
	}
	
	public static void sort(LocalizableAction action, MultipleDocumentModel multiDocument, UtilSort utilSort) {
		//get the text area
		final JTextArea editor = multiDocument.getCurrentDocument().getTextComponent();
		Document doc = editor.getDocument();
		int positionMark = 0;
		int positionCaret = 0;
		

		
		//try to retrieve the position caret and the position mark. calculate the length of the desired string and offset
		try {
			positionCaret = editor.getLineStartOffset(editor.getLineOfOffset(editor.getCaret().getDot()));
			positionMark = editor.getLineStartOffset(editor.getLineOfOffset(editor.getCaret().getMark()));
			int len = Math.abs(positionMark - positionCaret);
			int offset = 0;
			
			if(len!=0) {
				offset = Math.min(positionCaret,positionMark);
			} else {
				len = doc.getLength();
			}
			
			String text = doc.getText(offset, len);
			text = updateLineOrder(text, utilSort);
			doc.remove(offset, len);
			doc.insertString(offset, text, null);
		} catch(BadLocationException ex) {
			ex.printStackTrace();
		}
	}
	
	public static String updateLineOrder(String text, UtilSort utilSort) {
		//split the string into lines
		String[] splitted = text.split("\n");
		
		//get the hr locale and collator
		Locale hrLocale = new Locale("hr");
		Collator hrCollator = Collator.getInstance(hrLocale);
		
		//sort the array
		if(utilSort == UtilSort.ASCENDING) {
			Arrays.sort(splitted, hrCollator);
		}
		else if(utilSort == UtilSort.DESCENDING) {
			Arrays.sort(splitted, hrCollator.reversed());
		}
		
		String sorted = new String();
		
		//join the array together again with \n added to each line
		for(String s : splitted) {
			sorted = sorted + s + "\n";
		}
		return new String(sorted);
	}

}
