package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.documentmodels.SingleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadapp.JNotepadPP;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Used to exit the whole JNotepadPP. Asks to close each modified file individually before exiting. Upon pressing cancel,
 * stops the exit process and doesn't close any windows
 * @author Marko
 *
 */
public class ExitAction extends LocalizableAction{
	
	private static final long serialVersionUID = 1L;
	
	JNotepadPP notepad;
	MultipleDocumentModel multiDocument;
	ILocalizationProvider flp;
	
	public ExitAction(MultipleDocumentModel multiDocument, JNotepadPP notepad, ILocalizationProvider flp) {
		
		//localize names
		super("exit_action", "exit_action_description", flp);
		
		//initialize variables
		this.notepad = notepad;
		this.multiDocument = multiDocument;
		this.flp = flp;
		
		//add keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control X"));
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_X); 
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		//get the iterator of the SingleDocumentModels present in MultiDocumentModel
		Iterator<SingleDocumentModel> it = multiDocument.iterator();
		
		//go through each SingleDocumentModel
		while(it.hasNext()) {
			SingleDocumentModel singleDocument = it.next();
			
			//if the model has been modified
			if(singleDocument.isModified()) {
				
				//get the model name 
				String fileName;
				if(singleDocument.getFilePath() != null) {
					fileName = singleDocument.getFilePath().getFileName().toString();
				} else {
					fileName = "(unnamed)";
				}
				
				//ask the user if he wishes to save the model
				int result = JOptionPane.showConfirmDialog(
						notepad, 
						flp.getString("file") + " " + fileName + " " + flp.getString("modified"), 
						"Spremi datoteku", 
						JOptionPane.YES_NO_CANCEL_OPTION);
				
				//if he does, save it
				if(result == JOptionPane.YES_OPTION) {
					AbstractAction saveAction = new SaveDocumentAction(multiDocument, notepad, flp);
					saveAction.actionPerformed(null);
					//multiDocument.closeDocument(singleDocument);
				} else if (result == JOptionPane.NO_OPTION) {
					//if he doesn't, skip it
				} else if (result == JOptionPane.CANCEL_OPTION) {
					//if he cancels in any of the windows, don't exit the JNotepadPP
					return;
				}
			}
		}
		
		System.exit(0);
	}

}
