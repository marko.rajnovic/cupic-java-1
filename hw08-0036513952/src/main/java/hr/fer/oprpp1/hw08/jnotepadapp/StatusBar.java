package hr.fer.oprpp1.hw08.jnotepadapp;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.SimpleDateFormat;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentListener;
import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.documentmodels.SingleDocumentModel;

/**
 * Component displayed at the bottom of the JNotepadPP app. Used to monitor the length, line, column and selection, as well as a clock
 * @author Marko
 *
 */
public class StatusBar extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JLabel lenLabel;
	private JLabel lineLabel;
	private JLabel colLabel;
	private JLabel selLabel;
	private JLabel clock;
	private JLabel separator;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	private MultipleDocumentModel multiDocument;
	JTextArea editor;
	private JNotepadPP notepad;
	
	
	public StatusBar(MultipleDocumentModel multiDocument, JNotepadPP notepad) {
		
		//initialize variables
		this.multiDocument = multiDocument;
		this.notepad = notepad;
		
		if(multiDocument.getCurrentDocument() != null) {
			this.editor = multiDocument.getCurrentDocument().getTextComponent();
		}
		
		//set the labels to default values
		this.lenLabel = new JLabel("Len: 0");
		this.lineLabel = new JLabel(" Ln: 1");
		this.colLabel = new JLabel(" Col: 1");
		this.selLabel = new JLabel(" Sel: 0");
		this.clock = new JLabel();
		this.separator = new JLabel("              |");
		
		setBorder(new BevelBorder(BevelBorder.LOWERED));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		initGUI();
		
	}

	private void initGUI() {
		this.setPreferredSize(new Dimension(notepad.getWidth(), 20));
		
		//add all the labels to the status bar
		this.add(lenLabel);
		this.add(separator);
		this.add(lineLabel);
		this.add(colLabel);
		this.add(selLabel);
		
		//paste the clock to the end of the line
		this.add(Box.createHorizontalGlue());
		this.add(clock, BorderLayout.PAGE_END);
		
		initClock();
		
		//add a caret listener that updates the text on the labels
		CaretListener caretListener = new CaretListener() {
			
			@Override
			public void caretUpdate(CaretEvent e) {
				lenLabel.setText("Len: " + editor.getText().length());
				try {
					int position = editor.getCaretPosition();
					int line = editor.getLineOfOffset(position);
					lineLabel.setText(" Ln: " + (line + 1));
					colLabel.setText(" Col: " + (position - editor.getLineStartOffset(line) + 1));
					selLabel.setText(" Sel: " + Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark()));
				} catch (BadLocationException e1) {
					e1.printStackTrace();
				}
			}
		};
		
		
		//add a document listener
		multiDocument.addMultipleDocumentListener(new MultipleDocumentListener() {
			
			@Override
			public void documentRemoved(SingleDocumentModel model) {
				
			}
			
			@Override
			public void documentAdded(SingleDocumentModel model) {
			}
			
			@Override
			public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
				if(previousModel != null) {
					previousModel.getTextComponent().removeCaretListener(caretListener);
				}
				
				//when the document changes, switch the caret listener
				currentModel.getTextComponent().addCaretListener(caretListener);
				editor = currentModel.getTextComponent();
			}
		});
		
	}

	private void initClock() {
		Thread tickTock = new Thread(() -> {
			while (true) {
				try {
					Thread.sleep(500);
					SwingUtilities.invokeLater(() -> clock.setText(sdf.format(new java.util.Date())));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		tickTock.setDaemon(true);
		tickTock.start();
	}

}
