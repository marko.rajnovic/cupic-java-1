package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.KeyStroke;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadapp.JNotepadPP;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Action which copies currently selected text into a clipboard
 * @author Marko
 *
 */
public class CopyAction extends LocalizableAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JNotepadPP notepad;
	MultipleDocumentModel multiDocument;
	
	public CopyAction(MultipleDocumentModel multiDocument, JNotepadPP notepad, ILocalizationProvider flp) {
		
		//localize names
		super("copy_action", "copy_action_description", flp);
		
		//initialize variables
		this.notepad = notepad;
		this.multiDocument = multiDocument;
		
		//initialize keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control C"));
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_C); 
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//copies the selected text into clipboard
		multiDocument.getCurrentDocument().getTextComponent().copy();
	}
}
