package hr.fer.oprpp1.hw08.localization;

/**
 * Interface that observes if we changed the language of the localization
 * @author Marko
 *
 */
public interface ILocalizationListener {
	public void localizationChanged();
}
