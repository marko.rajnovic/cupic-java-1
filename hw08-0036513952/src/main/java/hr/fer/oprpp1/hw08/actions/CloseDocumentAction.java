package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.documentmodels.SingleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadapp.JNotepadPP;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Action for closing a tab in the JNotepadPP
 * @author Marko
 *
 */
public class CloseDocumentAction extends LocalizableAction{

	private static final long serialVersionUID = 1L;
	MultipleDocumentModel multiDocument;
	JNotepadPP notepad;
	SingleDocumentModel singleDocument;
	ILocalizationProvider flp;
	
	public CloseDocumentAction(MultipleDocumentModel multiDocument, SingleDocumentModel singleDocument, JNotepadPP notepad, ILocalizationProvider flp) {
		
		//add name and description
		super("close_action", "close_action_description", flp);
		
		//initialize required objects
		this.multiDocument=multiDocument;
		this.notepad = notepad;
		this.singleDocument = singleDocument;
		this.flp = flp;
		
		//add keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control F1")); 
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_W); 
	}




	@Override
	public void actionPerformed(ActionEvent e) {
		
		//if a document has been modified, call a saving action. Otherwise, just close it
		if(singleDocument.isModified()) {
			int result = JOptionPane.showConfirmDialog(
					notepad, 
					flp.getString("file_modified"), 
					flp.getString("file_modified_title"), 
					JOptionPane.YES_NO_CANCEL_OPTION);
			
			//choose desired action
			if(result == JOptionPane.YES_OPTION) {
				AbstractAction saveAction = new SaveDocumentAction(multiDocument, notepad, flp);
				saveAction.actionPerformed(null);
				multiDocument.closeDocument(singleDocument);
			} else if (result == JOptionPane.NO_OPTION) {
				multiDocument.closeDocument(singleDocument);
			} else if (result == JOptionPane.CANCEL_OPTION) {
				return;
			}
		} else {
			multiDocument.closeDocument(singleDocument);
		}
	}
}
