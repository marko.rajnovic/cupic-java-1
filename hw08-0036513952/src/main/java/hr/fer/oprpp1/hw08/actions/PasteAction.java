package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.KeyStroke;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadapp.JNotepadPP;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Paste text from the system clipboard
 * @author Marko
 *
 */
public class PasteAction extends LocalizableAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JNotepadPP notepad;
	MultipleDocumentModel multiDocument;
	
	public PasteAction(MultipleDocumentModel multiDocument, JNotepadPP notepad, ILocalizationProvider flp) {
		
		//localize names
		super("paste_action", "paste_action_description", flp);
		
		//initialize variables
		this.notepad = notepad;
		this.multiDocument = multiDocument;
		
		//add keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control V"));
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_V); 
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//paste the text at the desired location
		multiDocument.getCurrentDocument().getTextComponent().paste();
	}
}
