package hr.fer.oprpp1.hw08.localization;

import javax.swing.AbstractAction;

/**
 * Abstract action that supports changing the language of a name and the short description associated with it
 * @author Marko
 *
 */
public abstract class LocalizableAction extends AbstractAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public LocalizableAction(String name, String description, ILocalizationProvider provider) {
		putValue(NAME, provider.getString(name));
		putValue(SHORT_DESCRIPTION, provider.getString(description));
		provider.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				putValue(NAME, provider.getString(name));
				putValue(SHORT_DESCRIPTION, provider.getString(description));
			}
		});
	}

}
