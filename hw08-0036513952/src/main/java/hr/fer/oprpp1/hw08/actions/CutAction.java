package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.KeyStroke;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadapp.JNotepadPP;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Action used to cut selected text and copy it to clipboard
 * @author Marko
 *
 */
public class CutAction extends LocalizableAction{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JNotepadPP notepad;
	MultipleDocumentModel multiDocument;
	
	public CutAction(MultipleDocumentModel multiDocument, JNotepadPP notepad, ILocalizationProvider flp) {
		
		//localize names
		super("cut_action", "cut_action_description", flp);
		
		//initialize variables
		this.notepad = notepad;
		this.multiDocument = multiDocument;
		
		//initialize keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control X"));
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_X); 
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//cut out selected text and copy to clipboard
		multiDocument.getCurrentDocument().getTextComponent().cut();
	}
}
