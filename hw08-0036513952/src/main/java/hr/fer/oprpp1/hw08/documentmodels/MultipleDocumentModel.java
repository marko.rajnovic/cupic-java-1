package hr.fer.oprpp1.hw08.documentmodels;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Class which models the set of all the documents present in a notepad
 * @author Marko
 *
 */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel> {
	
	/**
	 * Creates a new document and adds it to the list of documents
	 * @return
	 */
	SingleDocumentModel createNewDocument();
	
	/**
	 * Gets the document currently being edited
	 * @return
	 */
	SingleDocumentModel getCurrentDocument();
	
	/**
	 * Loads a document from the file system
	 * @param path
	 * @return document loaded
	 */
	SingleDocumentModel loadDocument(Path path);
	
	/**
	 * Saves a document to the file system
	 * @param model being saved
	 * @param newPath to save the document to
	 * @throws IOException if the saving fails
	 */
	void saveDocument(SingleDocumentModel model, Path newPath) throws IOException;
	
	/**
	 * closes the document we pass to it
	 * @param model
	 */
	void closeDocument(SingleDocumentModel model);
	
	/**
	 * Adds a listener to the model
	 * @param l
	 */
	void addMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Removes a listener from the model
	 * @param l
	 */
	void removeMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Returns the number of documents present
	 * @return
	 */
	int getNumberOfDocuments();
	
	/**
	 * Gets the document at the specified index
	 * @param index
	 * @return
	 */
	SingleDocumentModel getDocument(int index);
}

