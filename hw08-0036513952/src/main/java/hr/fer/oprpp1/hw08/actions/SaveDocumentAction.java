package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Path;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadapp.JNotepadPP;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Save the file. If already has a path, save it to that path
 * @author Marko
 *
 */
public class SaveDocumentAction extends LocalizableAction{

	private static final long serialVersionUID = 1L;
	
	private JNotepadPP notepad;
	MultipleDocumentModel multiDocument;
	ILocalizationProvider flp;
	
	public SaveDocumentAction(MultipleDocumentModel multiDocument, JNotepadPP notepad, ILocalizationProvider flp) {
		
		//localize names
		super("save_action", "save_action_description", flp);
		
		//initialize variables
		this.notepad = notepad;
		this.multiDocument = multiDocument;
		this.flp = flp;
		
		//add keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control S")); 
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_S);  
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		//checks if we have a path already designated
		Path path = multiDocument.getCurrentDocument().getFilePath();
		
		//if we don't, ask the user to input one
		if(path==null) {
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle(flp.getString("save_action_description"));
			if(jfc.showSaveDialog(notepad)!=JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(
						notepad, 
						flp.getString("nothing_saved"), 
						flp.getString("warning"), 
						JOptionPane.WARNING_MESSAGE);
				return;
			}
			path = jfc.getSelectedFile().toPath();
			multiDocument.getCurrentDocument().setFilePath(path);
		}
		
		//try to save. If it fails, notify user
		try {
			multiDocument.saveDocument(multiDocument.getCurrentDocument(), path);
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(
					notepad, 
					flp.getString("error_loading") + " " +path.toFile().getAbsolutePath()+". " + flp.getString("warning_unknown_state"), 
					flp.getString("error"), 
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		JOptionPane.showMessageDialog(
				notepad, 
				flp.getString("file_saved"), 
				flp.getString("info"), 
				JOptionPane.INFORMATION_MESSAGE);
	}
	

	}
