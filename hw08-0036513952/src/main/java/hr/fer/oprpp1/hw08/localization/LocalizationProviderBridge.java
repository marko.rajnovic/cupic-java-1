package hr.fer.oprpp1.hw08.localization;

/**
 * Bridge that is used between a frame and a localization provider so we don't get memory leaks
 * @author Marko
 *
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider{
	
	ILocalizationProvider provider;
	boolean connected = false;
	
	//if the localization has changed, fire the AbstractLocalizationProvider fire method
	private ILocalizationListener listener = new ILocalizationListener() {
		
		@Override
		public void localizationChanged() {
			fire();
		}
		
	};
	
	public LocalizationProviderBridge(ILocalizationProvider provider) {
		this.provider = provider;
	}
	
	public void connect() {
		
		//connect the frame and provider
		if(connected) {
			return;
		}
		
		connected= true;
		provider.addLocalizationListener(listener);
	}
	
	public void disconnect() {
		
		//disconnect the frame and provider
		if(!connected) {
			return;
		}
		
		connected = false;
		provider.removeLocalizationListener(listener);
	}

	@Override
	public String getString(String s) {
		
		//get the string for the translation
		return provider.getString(s);
	}

}
