package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import javax.swing.KeyStroke;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.documentmodels.SingleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadapp.JNotepadPP;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;

/**
 * Opens a document from the file system
 * @author Marko
 *
 */
public class OpenDocumentAction extends LocalizableAction{

	private static final long serialVersionUID = 1L;
	MultipleDocumentModel multiDocument;
	JNotepadPP notepad;
	ILocalizationProvider flp;
	
	public OpenDocumentAction(MultipleDocumentModel multiDocument, JNotepadPP notepad, ILocalizationProvider flp) {
		
		//localize names
		super("open_action", "open_action_description", flp);
		
		//initialize variables
		this.multiDocument=multiDocument;
		this.notepad = notepad;
		this.flp = flp;
		
		//add keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control O")); 
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_O); 
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		//chose a file from the file system
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle(flp.getString("open_file"));
		
		//if he didn't click on open, leave the action
		if(fc.showOpenDialog(notepad)!=JFileChooser.APPROVE_OPTION) {
			return;
		}
		
		//get the selected file path
		File fileName = fc.getSelectedFile();
		Path filePath = fileName.toPath();
		
		//if the file path is readable, continue, otherwise, notify the user
		if(!Files.isReadable(filePath)) {
			JOptionPane.showMessageDialog(
					notepad, 
					flp.getString("file") + " " +fileName.getAbsolutePath()+ flp.getString("doesnt_exist"), 
					flp.getString("error"), 
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		//load the document using a multiDocument function
		SingleDocumentModel singleDocument = multiDocument.loadDocument(filePath);
		
		//if there was an error, notify the user and stop the action
		if(singleDocument == null) {
		JOptionPane.showMessageDialog(
					notepad, 
					flp.getString("error_loading") +" " +fileName.getAbsolutePath()+".", 
					flp.getString("error"), 
					JOptionPane.ERROR_MESSAGE);
			return;
		}
	}
}
