package hr.fer.oprpp1.hw08.documentmodels;

/**
 * A listener used to observe the SingleDocumentModel
 * @author Marko
 *
 */
public interface SingleDocumentListener {
	
	/**
	 * Fires if the document has been modified
	 * @param model
	 */
	void documentModifyStatusUpdated(SingleDocumentModel model);
	
	/**
	 * Fires if the document file path has been changed
	 * @param model
	 */
	void documentFilePathUpdated(SingleDocumentModel model);
}

