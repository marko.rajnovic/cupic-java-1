package hr.fer.oprpp1.hw08.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.oprpp1.hw08.documentmodels.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;
import hr.fer.oprpp1.hw08.localization.LocalizableAction;


/**
 * Action used to delete selected parts of a text
 * @author Marko
 *
 */
public class DeleteSelectedPartAction extends LocalizableAction{

	private static final long serialVersionUID = 1L;
	private MultipleDocumentModel multiDocument;
	
	public DeleteSelectedPartAction(MultipleDocumentModel multiDocument, ILocalizationProvider flp) {
		
		//localize names
		super("delete_action", "delete_action_description", flp);
		this.multiDocument = multiDocument;
		
		//add keybindings
		this.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("F2")); 
		this.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_D); 
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		//get the text editor
		JTextArea editor = multiDocument.getCurrentDocument().getTextComponent();
		
		//get the document associated with that editor
		Document doc = editor.getDocument();
		
		//determine the length and offset of the selected text from the mark and the dot of the caret
		int len = Math.abs(editor.getCaret().getDot()-editor.getCaret().getMark());
		if(len==0) return;
		int offset = Math.min(editor.getCaret().getDot(),editor.getCaret().getMark());
		try {
			//try to remove selected text
			doc.remove(offset, len);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
		
	}

}
