package hr.fer.oprpp1.hw08.documentmodels;

import java.awt.Component;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import hr.fer.oprpp1.hw08.actions.CloseDocumentAction;
import hr.fer.oprpp1.hw08.jnotepadapp.JNotepadPP;
import hr.fer.oprpp1.hw08.localization.ILocalizationProvider;

public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	List<SingleDocumentModel> documents;
	SingleDocumentModel currentDocument;
	List<MultipleDocumentListener> listeners;
	JNotepadPP notepad;
	ImageIcon redSaveIcon;
	ImageIcon greenSaveIcon;
	ILocalizationProvider flp;
	
	public DefaultMultipleDocumentModel(JNotepadPP notepad, ILocalizationProvider flp) {
		
		//initialize variables
		this.notepad = notepad;
		documents = new ArrayList<>();
		listeners = new ArrayList<>();
		this.flp = flp;
		
		//add a listener to the JTabbedPane, that will switch to a new document when we change tabs
		this.addChangeListener(new ChangeListener () {
			

			@Override
			public void stateChanged(ChangeEvent e) {
				
				if(DefaultMultipleDocumentModel.this.getSelectedIndex() != -1 && documents.size() > 0) {
					SingleDocumentModel documentNewlyClicked = getDocument(DefaultMultipleDocumentModel.this.getSelectedIndex());
					
					for(MultipleDocumentListener l : listeners) {
						l.currentDocumentChanged(currentDocument, documentNewlyClicked);
					} 
					currentDocument = documentNewlyClicked;
				}
			}
			
		});
		
		//initialize the save icons
		loadSaveIcons();
	}

	
	private void loadSaveIcons() {
		
		//get the red save icon
		InputStream is = this.getClass().getResourceAsStream("icons/redSaveIcon.png");
		if(is==null) {
			throw new IllegalArgumentException();
		}
		byte[] bytes = null;
		try {
			bytes = is.readAllBytes();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		//scale it so it is small
		redSaveIcon =  new ImageIcon(new javax.swing.ImageIcon(bytes).getImage().getScaledInstance(15, 15, Image.SCALE_SMOOTH));
		
		
		//get the green save icon
		is = this.getClass().getResourceAsStream("icons/greenSaveIcon.png");
		if(is==null) {
			throw new IllegalArgumentException();
		}
		try {
			bytes = is.readAllBytes();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//scale it so it is small
		greenSaveIcon =  new ImageIcon(new javax.swing.ImageIcon(bytes).getImage().getScaledInstance(15, 15, Image.SCALE_SMOOTH));
	}
	

	@Override
	public Iterator<SingleDocumentModel> iterator() {
		return documents.iterator();
	}

	@Override
	public SingleDocumentModel createNewDocument() {
		
		//create a new single document
		SingleDocumentModel newDocument = new DefaultSingleDocumentModel(null, null);
		
		//add it to the others
		documents.add(newDocument);
		
		//add a tab that contains it
		addTab(newDocument, null);
		
		//set the current document to this one
		currentDocument= newDocument;
		return newDocument;
	}


	private void addTab(SingleDocumentModel newDocument, Path path) {
		
		//wrap the document in a scrollable pane
		JScrollPane scrollablePane = new JScrollPane(newDocument.getTextComponent());
		addTab(null, null, scrollablePane, "");
		JPanel tab = new JPanel();
		
		//create the button for the tab
		createTabButton(newDocument, path, scrollablePane, tab, documents.size() - 1);
		
		SingleDocumentListener singleListener = new SingleDocumentListener() {
			
			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				
				//remove remove the icon and the name of the file from the tab
				tab.remove(0);
				tab.remove(0);
				
				//get the button component
				Component button = tab.getComponent(0);
				
				//remove the button as well
				tab.remove(0);
				
				//get the name and the path, change them appropriately
				String fullPath;
				String name;
				Path path = newDocument.getFilePath();
				if(path == null) {
					name = "(unnamed)";
					fullPath = "";
				} else {
					name = path.getFileName().toString();
					fullPath = path.toString();
				}
				
				//change the icon, based on modify status
				JLabel iconLabel = new JLabel();
				if(newDocument.isModified()) {
					iconLabel.setIcon(redSaveIcon);	
				} else {
					iconLabel.setIcon(greenSaveIcon);
				}
				
				//add the icon, name and the button, as well as reset the tooltip text
				tab.add(iconLabel);
				tab.add(new JLabel(name));
				tab.setToolTipText(fullPath);
				tab.add(button);
				repaint();
			}

			@Override
			public void documentFilePathUpdated(SingleDocumentModel model) {
				// TODO Auto-generated method stub
				
			}
			
		};
		
		//add the listener to the coument
		newDocument.addSingleDocumentListener(singleListener);
		
		//fire the listeners for adding documents and current document changes
		for(MultipleDocumentListener l : listeners) {
			l.documentAdded(newDocument);
			l.currentDocumentChanged(newDocument, currentDocument);
		}
	}


	
	private void createTabButton(SingleDocumentModel newDocument, Path path, JScrollPane scrollablePane,
			JPanel tab, int index) {
		
		
		String fullPath;
		String name;
		
		//set the full path and name from the document path variable
		if(path == null) {
			name = "(unnamed)";
			fullPath = "";
		} else {
			name = path.getFileName().toString();
			fullPath = path.toString();
		}
		
		//change the save icon
		JLabel iconLabel = new JLabel();
		if(newDocument.isModified()) {
			iconLabel.setIcon(redSaveIcon);	
		} else {
			iconLabel.setIcon(greenSaveIcon);
		}
		
		//add the icon, label and close button to the JPanel we will add to the tab
		tab.add(iconLabel);
		tab.add(new JLabel(name));
		tab.setToolTipText(fullPath);
		JButton closeButton = new JButton("X");
		closeButton.setBorder(BorderFactory.createEmptyBorder());
		closeButton.setMargin(new Insets(0, 0, 0, 0));
		
		//place a listener to the close button that will close the file
		closeButton.addActionListener((e) -> {
			int count = documents.size();
			AbstractAction action = new CloseDocumentAction(this, newDocument, notepad, flp);
			action.actionPerformed(null);
			
			if(count != documents.size()) {
				this.remove(scrollablePane);
			}
		});
		
		tab.add(closeButton);
		tab.setOpaque(false);
		tab.setFocusable(false);

		//set the tab component to the desired index
		this.setTabComponentAt(index, tab);
		
		//The JPanel is not clickable. To go around this, I decided to search the tab that was clicked, and click it. This is a workaround. 
		//If I found a better solution to makin the JPanel, lets say, click-throughable, so out click ignores it, i would.
		tab.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				int componentIndex = 0;
				
				for(int i = 0; i < documents.size(); i++) {
					Component comp = DefaultMultipleDocumentModel.this.getTabComponentAt(i);
					if(comp == tab) {
						componentIndex = i;
						break;
					}
				}
				
				if(documents.size() > componentIndex) {
				DefaultMultipleDocumentModel.this.setSelectedIndex(componentIndex);
				}
			}

		});
	}
	
	@Override
	public SingleDocumentModel loadDocument(Path path) {
		
		//read all the bytes into a byte array
		byte[] okteti;
		try {
			okteti = Files.readAllBytes(path);
		} catch(Exception ex) {
			return null;
		}
		
		//change the byte array into a utf8 string
		String tekst = new String(okteti, StandardCharsets.UTF_8);
		
		//create a new document and add it to the others
		SingleDocumentModel newDocument = new DefaultSingleDocumentModel(path, tekst);
		documents.add(newDocument);
		
		//add a tab with the document
		addTab(newDocument, path);
		
		//change the current document to this one
		currentDocument = newDocument;
		return newDocument;
	}

	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) throws IOException {
		
		//get the text as a byte array
		byte[] podatci = model.getTextComponent().getText().getBytes(StandardCharsets.UTF_8);
		
		//write the byte array into the path
		Files.write(newPath, podatci);
		
		//sets the modified variable to false, as it was just saved
		model.setModified(false);
		
		JPanel comp = (JPanel) DefaultMultipleDocumentModel.this.getTabComponentAt(getSelectedIndex());
		JLabel lab = (JLabel) comp.getComponent(1);
		lab.setText(newPath.getFileName().toString());
	}

	@Override
	public void closeDocument(SingleDocumentModel document) {
		
		//remove the document, and fire the document removed listener
		documents.remove(document);
		for(MultipleDocumentListener l : listeners) {
			l.documentRemoved(document);
		}
		repaint();
	}

	@Override
	public SingleDocumentModel getCurrentDocument() {
		return currentDocument;
	}

	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.add(l);
	}

	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.remove(l);
	}

	@Override
	public int getNumberOfDocuments() {
		return documents.size();
	}

	@Override
	public SingleDocumentModel getDocument(int index) {
		return documents.get(index);
	}
}
