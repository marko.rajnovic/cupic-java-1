package hr.fer.oprpp1.math;

/**
 * Class which models a 2D math vector
 * @author Marko
 *
 */
public class Vector2D {
	private double x;
	private double y;

	//CONSTRUCTOR, AND GETTERS
	
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	
	/**
	 * adds the offset vector to the current one
	 * @param offset
	 */
	public void add(Vector2D offset) {
		this.x = this.x + offset.x;
		this.y = this.y + offset.y;
	}
	
	/**
	 * Creates a new vector that is the sum of the offset and current vector
	 * @param offset
	 * @return
	 */
	public Vector2D added(Vector2D offset) {
		return new Vector2D(this.x + offset.x, this.y + offset.y);
	}
	
	
	/**
	 * Rotates the current vector
	 * @param angle in radians
	 */
	public void rotate(double angle) {
		double x1 = this.x;
		double y1 = this.y;
		
		this.x = x1 * Math.cos(angle) - y1 * Math.sin(angle);
		this.y = x1 * Math.sin(angle) + y1 * Math.cos(angle);
		
	}
	
	/**
	 * Rotates the current vector
	 * @param angle in radians
	 * @return new Vector2D that has been rotated
	 */
	public Vector2D rotated(double angle) {
		double x1 = this.x;
		double y1 = this.y;
		
		return new Vector2D(x1 * Math.cos(angle) - y1 * Math.sin(angle),
				x1 * Math.sin(angle) + y1 * Math.cos(angle));
				
	}
	
	/**
	 * Scale the current vector
	 * @param scaler
	 */
	public void scale(double scaler) {
		this.x = this.x * scaler;
		this.y = this.y * scaler;
	}
	
	/**
	 * Scale the current vector
	 * @param scaler
	 * @return Vector2D that has been rotated
	 */
	public Vector2D scaled(double scaler) {
		return new Vector2D(this.x * scaler, this.y * scaler);
	}
	
	
	/**
	 * Hard copies the vector
	 */
	public Vector2D copy() {
		return new Vector2D(this.x,this.y);
	}
}
