package hr.fer.oprpp1.custom.collections;

/**
 * Interface which is implemented to do a certain action on an object. Has only one method, process(Object value)
 * @author Marko
 *
 */
public interface Processor<T> {
	
	/**
	 * This method is used to do an action on a given object.
	 * @param value
	 */
	public void process(T value);
	
}
