package hr.fer.oprpp1.custom.collections;

public class ObjectStack<T> {
	
	private ArrayIndexedCollection<Object> stack;
	
	
	public ObjectStack(){
		stack = new ArrayIndexedCollection<Object>();
	}
	
	/**
	 * checks if stack is empty
	 * @return
	 */
	public boolean isEmpty() {
		return stack.isEmpty();
	}
	
	/**
	 * returns the size of stack
	 * @return size
	 */
	public int size() {
		return stack.size();
	}
	
	/**
	 * push a value on the front of the stack
	 * @param value
	 */
	public void push(T value) {
		stack.add(value);
	}
	
	/**
	 * remove a value from the front of the stack and return it
	 * @return T obj
	 */
	@SuppressWarnings("unchecked")
	public T pop() {
		Object obj = new Object();
		obj = stack.get(stack.size() - 1);
		stack.remove(stack.size() - 1);
		return (T)obj;
	}
	
	/**
	 * returns a value from the front of the stack without removing it
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T peek() {
		Object obj = new Object();
		obj = stack.get(stack.size() - 1);
		return (T)obj;
	}
	
	/**
	 * removes all elements from stack
	 */
	public void clear() {
		stack.clear();
	}
	
	
}