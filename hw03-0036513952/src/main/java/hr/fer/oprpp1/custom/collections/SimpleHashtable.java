package hr.fer.oprpp1.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimpleHashtable<K,V> implements Iterable<SimpleHashtable.TableEntry<K,V>>{
	TableEntry<K,V>[] table;
	int size;
	private int modificationCount;
	
	/**
	 * Class that is used internally to store each key-value pair that we input
	 * @author Marko
	 *
	 * @param <K>
	 * @param <V>
	 */
	static class TableEntry<K,V>{
		private K key;
		private V value;
		private TableEntry<K,V> next;
		
		public TableEntry(K key, V value) {
			this.key = key;
			this.value = value;
			this.next = null;
		}

		public K getKey() {return key;}
		//public void setKey(K key) {this.key = key;}
		public V getValue() {return value;}
		public void setValue(V value) {this.value = value;}
		public TableEntry<K, V> getNext() {return next;}
		public void setNext(TableEntry<K, V> next) {this.next = next;}	
	}
	
	/**
	 * The capacity will be brought to the next number that is the power of 2. For example, if the capacity is set to 13, it will be actually set to 16
	 * @param capacity
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashtable(int capacity) {
		if(capacity < 1) {
			throw new IllegalArgumentException("Capacity of a SimpleHashtabel cannot be less than 1");
		}
		
		//we get the log2 of the capacity, then we get its' ceiling, and then we finally get the final capacity by taking 2^power
		int power = (int) Math.ceil(Math.log10(capacity) / Math.log10(2.));
		capacity = (int) Math.pow(2, power);
		
		table = (TableEntry<K,V>[])new TableEntry[capacity];
		size = 0;
		modificationCount = 0;
	}
	
	/**
	 * Default constructor. Capacity will be set to 16
	 */
	public SimpleHashtable() {
		this(16);
	}

	/**
	 * Puts a key-value pair in the hashtable. If the dictionary already contains a pair with the current key value, that value will be replaced and returned. Returns null otherwise
	 */
	public V put(K key, V value) {
		if(key == null) {
			throw new NullPointerException("Key cannot be null");
		}
		
		V valueToReturn = null;
		
		//find the hash value for the key
		int hash = Math.abs(key.hashCode() % table.length);
		
		TableEntry<K,V> newEntry = new TableEntry<>(key, value);
		
		//if there is already an element in the table at the current hash position
		if(table[hash] != null) {
			//get that element
			TableEntry<K,V> entry = table[hash];
			
			//if we find that the key is already set, replace the value
			if(entry.getKey().equals(key)) {
				entry.setValue(value);
				valueToReturn = value;
			} else {
				while(entry.getNext() != null){
					//we iterate further
					entry = entry.getNext();
					//if we find that the key is already set, replace the value
					if(entry.getKey().equals(key)) {
						valueToReturn = entry.getValue();
						entry.setValue(value);
						break;
					}
				}
			}
				
			if(valueToReturn == null) {
				entry.setNext(newEntry);
				increaseSize();
				modificationCount++;
			}
		} else {
			table[hash] = newEntry;
			increaseSize();
			modificationCount++;
		}
		
		return valueToReturn;
		
	}
	
	public V get(Object key) {
		int hash = Math.abs(key.hashCode() % table.length);
		V valueToReturn = null;
		
		if(table[hash] != null) {
			TableEntry<K,V> entry = table[hash];
			
			//if we find the key, set the value
			if(entry.getKey().equals(key)) {
				valueToReturn = entry.getValue();
			} else {
				while(entry.getNext() != null){
					//iterate further
					entry = entry.getNext();
					//if we find the key, set the value
					if(entry.getKey().equals(key)) {
						valueToReturn = entry.getValue();
						break;
					}
				} 
			}		
		}
		
		return valueToReturn;
	}
	
	/**
	 * returns how many elements there currently are in the hashTable
	 * @return
	 */
	public int size() {
		return this.size;
	}
	
	/**
	 * Check if the hashtable contains a key
	 * @param key
	 * @return
	 */
	public boolean containsKey(Object key) {
		//we get the hash value
		int hash = Math.abs(key.hashCode() % table.length);
		
		TableEntry<K,V> entry = table[hash];
		
		//check if there is an entry at the current hash value in the table
		if(entry != null) {
			//iterate over the entries. if we find the key, return true
			while(entry != null) {
				if(entry.getKey().equals(key)) {
					return true;
				}
				entry = entry.getNext();
			}
		}
		return false;
	}
	
	public boolean containsValue(Object value) {
		
		//iterate over the entire hashtable
		for(int i = 0; i < table.length; i++) {
			if(table[i] == null) {
				continue;
			}
			
			TableEntry<K,V> entry = table[i];
			
			//iterate over the entries. if we find the value, return true
			while(entry != null) {
				if(entry.getValue().equals(value)) {
					return true;
				}
				entry = entry.getNext();
			}
		}
		return false;
		
	}
	
	/**
	 * Removes the key-value pair associated with the key, and returns the value. If there was no such pair, returns null
	 * @param key
	 * @return
	 */
	public V remove(Object key) {
		//get the hash code
		int hash = Math.abs(key.hashCode() % table.length);
		V valueToReturn = null;
		
		//if there is an element at the current table[hash]
		if(table[hash] != null) {
			TableEntry<K,V> entry = table[hash];
			
			//iterate over the entries at the current hash
			if(entry.getKey().equals(key)) {
				//if the first element needs to be removed, just adjust the table[hash] to the next element
				table[hash] = entry.getNext();
			} else {
				while(entry != null && entry.getNext() != null){
					//checks if the next element is the one that needs to be removes
					if(entry.getNext().getKey().equals(key)) {
						//if the next element needs to be removed, sets the next element of the current element to the next-next element
						entry.setNext(entry.getNext().getNext());
						break;
					}
					//iterate
					entry = entry.getNext();
				}
			}		
		}
		
		++modificationCount;
		--size;
		return valueToReturn;
	}
	
	/**
	 * Returns true if the hashtable is empty
	 */
	public boolean isEmpty() {
		return size == 0;
	}
	public String toString() {
		String s = new String("[");
		for(int i = 0; i < table.length; i++) {
			if(table[i] == null) {
				continue;
			}

			
			TableEntry<K,V> entry = table[i];
			s = s + (entry.getKey() + "=" + entry.getValue() + ", ");
			while(entry.getNext() != null){
				entry = entry.getNext();
				s = s + (entry.getKey() + "=" + entry.getValue() + ", ");
			}
		}
		s = s.substring(0, s.length() - 2) + "]";
		return s;
	}
	
	/**
	 * Returns an array of TableEntries 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public TableEntry<K,V>[] toArray(){
		TableEntry<K,V>[] elements = (TableEntry<K,V>[])new TableEntry[size];
		int counter = 0;
		for(int i = 0; i < table.length; i++) {
			if(table[i] == null) {
				continue;
			}
			
			TableEntry<K,V> entry = table[i];
			
			while(entry != null) {
				elements[counter++] = entry;
				entry = entry.getNext();
			}
		}
		
		return elements;
	}
	
	/**
	 * Removes all elements from the collection
	 */
	public void clear() {
		for(int i = 0; i < table.length; i++) {
			table[i] = null;
		}
	}

	/**
	 * Adds to size of the hashtable. If the size reaches a certain limit, double the capacity
	 */
	private void increaseSize() {
		size++;
		if(size >= 0.75 * table.length) {
			doubleCapacity();
		}
	}

	/**
	 * Doubles the capacity of the hashtable
	 */
	@SuppressWarnings("unchecked")
	private void doubleCapacity() {
		TableEntry<K,V>[] entries = this.toArray();
		table = (TableEntry<K,V>[])new TableEntry[table.length * 2];
		
		//puts all the entries into the new table that is 2 times larger
		for(int i = 0; i < entries.length; i++) {
			--size;
			this.put(entries[i].getKey(), entries[i].getValue());
		}
		
		return;
	}

	@Override
	public Iterator<TableEntry<K, V>> iterator() {
		Iterator<TableEntry<K,V>> it = new IteratorImpl();
		return it;
	}
	
	private class IteratorImpl implements Iterator<SimpleHashtable.TableEntry<K,V>> {
		
		private int currentTableIndex = 0;
		private TableEntry<K,V> entry = null;
		private int count = size;
		private int iteratorModCount = modificationCount;
		private int removeCount = 0;
		
		
		@Override
		public boolean hasNext() {
			return count > 0;
		}

		@Override
		public TableEntry<K, V> next() {
			//if we modified the hashtable since the iterators' creation, throw an error
			if(iteratorModCount != modificationCount) {
				throw new ConcurrentModificationException();
			}
			
			if(!hasNext()) {
				throw new NoSuchElementException();
			}
			
			if(entry == null) {
				return findNextEntry();
			} 
			if(entry.getNext() == null) {
				++currentTableIndex;
				return findNextEntry();
			} 
			
			entry = entry.getNext();
			--count;
			return entry;
		}

		/**
		 * Finds the next entry in the hashtable
		 * @return
		 */
		private TableEntry<K,V> findNextEntry() {
			for(; currentTableIndex < table.length; ++ currentTableIndex) {
				if(table[currentTableIndex] != null ) {
					entry = table[currentTableIndex];
					--count;
					return entry;
				}
			}
			return null;
		}
		
        @Override
        public void remove() {
        	//if we already removed an element, throw an exception
			if(removeCount != 0) {
				throw new IllegalStateException();
			}
			
        	TableEntry<K,V> currentEntry = entry;
    		entry = next();
    		
            SimpleHashtable.this.remove(currentEntry.getKey());
            ++removeCount;
            --modificationCount;
        }
        
		}

}
