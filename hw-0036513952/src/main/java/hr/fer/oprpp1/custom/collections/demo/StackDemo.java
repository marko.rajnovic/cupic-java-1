package hr.fer.oprpp1.custom.collections.demo;

import hr.fer.oprpp1.custom.collections.ObjectStack;

public class StackDemo {

	public static int intFromObjectString(Object obj) {
		if(obj instanceof Integer) {
			return (int)obj;
		}
		return Integer.parseInt((String) obj);
	}
	
	public static void main(String[] args) {
		if(args.length != 1) {
			System.exit(1);
		}
		
		String line = new String(args[0]);
		
		String[] elements = line.split(" ");
		
		
		ObjectStack stack = new ObjectStack();
		for(int i = 0; i < elements.length; i++) {
			int op1;
			int op2;
			
			switch(elements[i]) {
			case "+":
				op1 = intFromObjectString(stack.pop());
				op2 = intFromObjectString(stack.pop());
				stack.push(op1 + op2);
				break;
			case "-":
				op1 = intFromObjectString(stack.pop());
				op2 = intFromObjectString(stack.pop());
				stack.push(op2 - op1);
				break;
			case "*":
				op1 = intFromObjectString(stack.pop());
				op2 = intFromObjectString(stack.pop());
				stack.push(op1 * op2);
				break;
			case "/":
				op1 = intFromObjectString(stack.pop());
				op2 = intFromObjectString(stack.pop());
				stack.push(op2 / op1);
				break;
			default:	
				stack.push(elements[i]);
			}				
		}
		
		if(stack.size() != 1) {
			System.out.println("Error: invalid input");
		} else {
			System.out.println("Result: " + stack.pop());
		}

	}
}
