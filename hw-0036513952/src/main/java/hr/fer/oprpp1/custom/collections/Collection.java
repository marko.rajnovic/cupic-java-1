package hr.fer.oprpp1.custom.collections;

public class Collection {
	
	protected int size;
	
	protected Collection() {
		super();
	}
	
	/**
	 * Checks if the collection contains any objects
	 * @return true if collection contains an object, false otherwise
	 */
	public boolean isEmpty(){
		return size() == 0;
	};
	
	/**
	 * Returns the number of currently stored objects in this collection
	 */
	public int size() {
		return 0;
	};

	/**
	 * Adds the given object into this collection.
	 * @param value to be added into this collection
	 */
	public void add(Object value) {}; 
	
	
	/**
	 * Returns true only if the collection contains given value, as determined by equals method.
	 * @param value which we want to check if the collection contains
	 * @return This implementation always returns false.
	 */
	public boolean contains(Object value) {
		return false;
	}
	
	/**
	 * Returns true only if the collection contains given value as determined by equals method and removes
	 * one occurrence of it (in this class it is not specified which one).
	 * @param value which we want to remove from the collection
	 * @return This implementation always returns false.
	 */
	public boolean remove(Object value) {
		return false;
	};

	/**
	 * Allocates new array with size equals to the size of this collections, fills it with collection content and
	 *	returns the array. This method never returns null.
	 * @return this operation is not supported
	 * @throws UnsupportedOperationException
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	};
	
	/**
	 * Method calls processor.process(.) for each element of this collection. The order in which elements
 	 * will be sent is undefined in this class.
	 * @param processor
	 */
	public void forEach(Processor processor) {};
	
	/**
	 * Method adds into the current collection all elements from the given collection. This other collection
	 * remains unchanged.
	 * @param other
	 */
	public void addAll(Collection other) {
		class AddAllProcessor extends Processor{
			
			public void process(Object value) {
				add(value);
			}
		}
		
		other.forEach(new AddAllProcessor());
	};
	
	/**
	 * Removes all elements from this collection. This implementation does nothing
	 */
	void clear() {}; 

}
