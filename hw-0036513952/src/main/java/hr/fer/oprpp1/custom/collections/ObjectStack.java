package hr.fer.oprpp1.custom.collections;

public class ObjectStack {
	
	private ArrayIndexedCollection stack;
	
	
	public ObjectStack(){
		stack = new ArrayIndexedCollection();
	}
	
	/**
	 * checks if stack is empty
	 * @return
	 */
	public boolean isEmpty() {
		return stack.isEmpty();
	}
	
	/**
	 * returns the size of stack
	 * @return size
	 */
	public int size() {
		return stack.size();
	}
	
	/**
	 * push a value on the front of the stack
	 * @param value
	 */
	public void push(Object value) {
		stack.add(value);
	}
	
	/**
	 * remove a value from the front of the stack and return it
	 * @return Object obj
	 */
	public Object pop() {
		Object obj = new Object();
		obj = stack.get(stack.size() - 1);
		stack.remove(stack.size() - 1);
		return obj;
	}
	
	/**
	 * returns a value from the front of the stack without removing it
	 * @return
	 */
	public Object peek() {
		Object obj = new Object();
		obj = stack.get(stack.size() - 1);
		return obj;
	}
	
	/**
	 * removes all elements from stack
	 */
	public void clear() {
		stack.clear();
	}
	
	
}
