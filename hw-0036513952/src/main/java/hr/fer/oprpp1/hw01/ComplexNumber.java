package hr.fer.oprpp1.hw01;

public class ComplexNumber {
	double real;
	double imaginary;
	
	/**
	 * Constructor for ComplexNumber class. Takes "1+1i" would be written into the function as (1,1)
	 * @param real
	 * @param imaginary
	 */
	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}
	
	
	/**
	 * Creates an instance of ComplexNumber class from single real value
	 * @param real
	 * @return
	 */
	public static ComplexNumber fromReal(double real) {
		return new ComplexNumber(real, 0);
	}
	
	/**
	 * Creates an instance of ComplexNumber class from single imaginary value
	 * @param real
	 * @return
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		return new ComplexNumber(0, imaginary);
		
	}
	
	/**
	 * Creates an instance of ComplexNumber class from a value written in polar coordinate system
	 * @param magnitude
	 * @param angle
	 * @return
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle){
		return new ComplexNumber(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
	}
	
	/**
	 * Takes a string in a form of "a+bi", where a and b are any decimal numbers
	 * @param s
	 * @return
	 */
    public static ComplexNumber parse(String s) {
		ComplexNumber number;
		
		//Stores signs of the calues
		boolean firstPositive = true;
		boolean secondPositive = true;
		
		//if the number contains an imaginary value, it is much more complex to implement. if the number doesn't contain "i", we just pass it to Double.parseDouble
		if(s.contains("i")) {
			
			//checks if the first value is negative
			if (s.charAt(0) == '-') {
			    firstPositive = false;
			}
			
			//checks ig the second value is negative
			if (s.substring(1).contains("-")) {
			    secondPositive = false;
			}
	
			//splits the values into multiple parts
			String[] splitted = s.split("[+-]");
			
			//removes leading + or - that was saved as empty String
			if (splitted[0].equals("")) {
			    splitted[0] = splitted[1];
			    splitted[1] = splitted[2];
			}
			
			double realNumber = 0;
			double imaginaryNumber = 0;
			
			//if first number is imaginary
			if (splitted[0].contains("i")) {
			    imaginaryNumber = Double.parseDouble((firstPositive ? "+" : "-") + splitted[0].substring(0,splitted[0].length() - 1));
			} else {
			    realNumber = Double.parseDouble((firstPositive ? "+" : "-") + splitted[0]);
			}
			
			//if there are 2 separate values
			if (splitted.length > 1) {
			    if (splitted[1].contains("i")) {
			        imaginaryNumber = Double.parseDouble((secondPositive ? "+" : "-") + splitted[1].substring(0,splitted[1].length() - 1));
			    }
			}
			
			number = new ComplexNumber(realNumber, imaginaryNumber);
		} else {
			number = new ComplexNumber(Double.parseDouble(s), 0);
		}
		
		return number;
	}

    /**
     * Get real part of number
     * @return
     */
	public double getReal() {
		return real;
	}

	/**
	 * get imaginary part of number
	 * @return
	 */
	public double getImaginary() {
		return imaginary;
	}
	
	/**
	 * get magnitude of the number in polar coordinate system
	 * @return
	 */
	public  double getMagnitude() {
		return Math.hypot(this.real, this.imaginary);
	}
	
	/**
	 * get the angle of the number in polar coordinate system. angle is [0, 2PI]
	 * @return
	 */
	public  double getAngle() {
		double angle = Math.atan(this.imaginary / this.real);
		
		if(this.real < 0) {
			angle = angle + Math.PI;
		}
		
		
		return angle;
	}
	
	
	/**
	 * add 2 complex numbers
	 * @param c
	 * @return
	 */
	public ComplexNumber add(ComplexNumber c) {
		return new ComplexNumber(this.real + c.real, this.imaginary + c.imaginary);
	}
	
	/**
	 * subtract 2 complex numbers
	 * @param c
	 * @return
	 */
	public ComplexNumber sub(ComplexNumber c) {
		return new ComplexNumber(this.real - c.real, this.imaginary - c.imaginary);
	}
	
	/**
	 * multiply 2 complex numbers
	 * @param c
	 * @return
	 */
	public ComplexNumber mul(ComplexNumber c) {
		double ansImaginary = this.real * c.imaginary + this.imaginary * c.real;
		double ansReal = this.real * c.real - this.imaginary * c.imaginary;
		
		return new ComplexNumber(ansReal, ansImaginary);
	}
	
	/**
	 * divide 2 complex numbers
	 * @param c
	 * @return
	 */
	//Dividing using a formula for division of two complex numbers
	public ComplexNumber div(ComplexNumber c) {
		double numeratorReal = this.real * c.real + this.imaginary * c.imaginary;

		double numeratorImaginary = this.imaginary * c.real - this.real * c.imaginary;
		
		double denominator = Math.pow(c.real, 2) + Math.pow(c.imaginary, 2);
		
		return new ComplexNumber(numeratorReal / denominator, numeratorImaginary / denominator);
	}
	
	/**
	 * raise the complex number to nth power
	 * @param n
	 * @return
	 */
	public ComplexNumber power(int n) {
		double magnitude = this.getMagnitude();
		double angle = this.getAngle();
		
		magnitude = Math.pow(magnitude, n);
		angle = angle * n;
		
		return fromMagnitudeAndAngle(magnitude, angle);
	}
	
	/**
	 * gets nth root of complex number. Return ComplexNumber[] of all possible solutions.
	 * @param n
	 * @return
	 */
	public ComplexNumber[] root(int n) {
		double magnitude = this.getMagnitude();
		double angle = this.getAngle();
		
		magnitude = Math.pow(magnitude, 1/(double)n);
		angle = angle / n;
		
		ComplexNumber[] numbers = new ComplexNumber[n];
		
		for(int i = 0; i < n; i++) {
			numbers[i] = fromMagnitudeAndAngle(magnitude, angle + 2 * i * Math.PI / n);
		}
		
		return numbers;
	}
	
	/**
	 * Gets string value of complex number
	 */
	public String toString() {
		String arg1 = String.valueOf(this.real);
		String arg2 = String.valueOf(this.imaginary);
		
		if(this.real == 0) {
			return arg2 + "i";
		} 
		
		if(this.imaginary == 0) {
			return arg1;
		}
		
		if(this.imaginary > 0) {
			return arg1 + "+" + arg2 + "i";
		} else {
			return arg1 + arg2 + "i";
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(imaginary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(real);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}


	//Used for JUnit tests. Goes only to 10th decimal for testing purposes.
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComplexNumber other = (ComplexNumber) obj;
		if (Math.abs(imaginary - other.getImaginary()) > 1e-10)
			return false;
		if (Math.abs(real - other.getReal()) > 1e-10)
			return false;
		return true;
	}
	
}
