package hr.fer.oprpp1.custom.collections;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ArrayIndexedCollectionTest {
	
	ArrayIndexedCollection array;
	
	@BeforeEach
	public void setUp() {
		array = new ArrayIndexedCollection(5);
		array.add(1);
		array.add(2);
		array.add(3);
		array.add(4);
		array.add(5);
	}
	
	//Test constructors that define empty collections
	@Test
	public void testConstructorIsEmpty() {
		ArrayIndexedCollection array1 = new ArrayIndexedCollection();
		assertTrue(array1.isEmpty());
		
		ArrayIndexedCollection array2 = new ArrayIndexedCollection(25);
		assertTrue(array2.isEmpty());
	}
	
	//Test constructors that copy elements to themselves from other collection
	@Test
	public void testConstructorCopiesElements(){
		
		//add them into the other array through constructor and check if they are correctly added
		ArrayIndexedCollection array1 = new ArrayIndexedCollection(array);
		assertFalse(array1.isEmpty());
		assertTrue(array1.contains(1));
		assertTrue(array1.contains(2));
		assertTrue(array1.contains(3));
		assertTrue(array1.contains(4));
		assertTrue(array1.contains(5));	
	}
	
	
	
	
	@Test
	public void testMethodAdd() {
		array.add(6);
		assertTrue(array.contains(6));
		
		assertThrows(NullPointerException.class, () -> array.add(null));
	}
	
	@Test
	public void testMethodGet() {
		assertEquals(1, array.get(0));
		assertEquals(2, array.get(1));
		assertThrows(IndexOutOfBoundsException.class, () -> array.get(-1));
		assertThrows(IndexOutOfBoundsException.class, () -> array.get(5));

	}
	
	@Test
	public void testMethodClear() {
		array.clear();
		assertTrue(array.isEmpty());
	}
	
	@Test
	public void testMethodInsert() {
		array.insert(0, 0);
		assertEquals(0, array.get(0));
		assertEquals(1, array.get(1));
		
		assertThrows(IndexOutOfBoundsException.class, () -> array.insert(0, -1));
		assertThrows(IndexOutOfBoundsException.class, () -> array.insert(0, 7));
	}
	
	@Test
	public void testMethodIndexOf() {
		assertEquals(0, array.indexOf(1));
		assertEquals(-1, array.indexOf(-1));
		assertEquals(-1, array.indexOf(null));
	}
	
	@Test
	public void testMethodRemove(){
		array.remove(0);
		assertEquals(2, array.get(0));
		assertThrows(IndexOutOfBoundsException.class, () -> array.get(4));
		
		
		array.remove(1);
		assertEquals(4, array.get(1));
	}

}
