package hr.fer.oprpp1.hw01;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;



public class ComplexNumberTest{
	
	public static ArrayList<ComplexNumber> numbers;
	public static int NUMBER_OF_TESTS = 5;
	
	@BeforeAll
	public static void setUp() {
		numbers = new ArrayList<ComplexNumber>();
		
		numbers.add(new ComplexNumber(1,1));
		numbers.add(new ComplexNumber(-1,1));
		numbers.add(new ComplexNumber(2,4));
		numbers.add(new ComplexNumber(3,5));
		numbers.add(new ComplexNumber(10,11));
	}
	
	
	@Test
	public void getRealTest() {
		assertEquals(1, numbers.get(0).getReal());
		assertEquals(-1, numbers.get(1).getReal());
		assertEquals(2, numbers.get(2).getReal());
		assertEquals(3, numbers.get(3).getReal());
		assertEquals(10, numbers.get(4).getReal());

	}
	
	@Test
	public void getImaginaryTest() {
		assertEquals(1, numbers.get(0).getImaginary());
		assertEquals(1, numbers.get(1).getImaginary());
		assertEquals(4, numbers.get(2).getImaginary());
		assertEquals(5, numbers.get(3).getImaginary());
		assertEquals(11, numbers.get(4).getImaginary());
	}
	
	@Test
	public void getMagnitudeTest() {
		assertEquals(1.4142135623730951, numbers.get(0).getMagnitude());
		assertEquals(1.4142135623730951, numbers.get(1).getMagnitude());
		assertEquals(4.47213595499958, numbers.get(2).getMagnitude());

	}
	@Test
	public void getAngleTest() {
		assertEquals(0.7853981633974483, numbers.get(0).getAngle());
		assertEquals(Math.PI - 0.7853981633974483, numbers.get(1).getAngle());
		assertEquals(1.1071487177940904, numbers.get(2).getAngle());

	}

	@Test
	public void addTest() {
		assertEquals(new ComplexNumber(2,2), numbers.get(0).add(new ComplexNumber(1,1)));
		assertEquals(new ComplexNumber(1,5), numbers.get(1).add(new ComplexNumber(2,4)));
		assertEquals(new ComplexNumber(3,5), numbers.get(2).add(new ComplexNumber(1,1)));
		
	}

	@Test
	public void subTest() {
		assertEquals(new ComplexNumber(0,0), numbers.get(0).sub(new ComplexNumber(1,1)));
		assertEquals(new ComplexNumber(-3,-3), numbers.get(1).sub(new ComplexNumber(2,4)));
		assertEquals(new ComplexNumber(1,3), numbers.get(2).sub(new ComplexNumber(1,1)));
		
	}

	@Test
	public void mulTest() {
		assertEquals(new ComplexNumber(0,2), numbers.get(0).mul(new ComplexNumber(1,1)));
		assertEquals(new ComplexNumber(-6,-2), numbers.get(1).mul(new ComplexNumber(2,4)));
		assertEquals(new ComplexNumber(-2,6), numbers.get(2).mul(new ComplexNumber(1,1)));
	}

	@Test
	public void divTest() {
		assertEquals(new ComplexNumber(1,0), numbers.get(0).div(new ComplexNumber(1,1)));
		assertEquals(new ComplexNumber(0.1,0.3), numbers.get(1).div(new ComplexNumber(2,4)));
		assertEquals(new ComplexNumber(3,1), numbers.get(2).div(new ComplexNumber(1,1)));
	}

	@Test
	public void powerTest() {
		assertEquals(new ComplexNumber(-21.0, 220.0), numbers.get(4).power(2));
		assertEquals(new ComplexNumber(-2630,1969), numbers.get(4).power(3));
		assertEquals(new ComplexNumber(-47959,-9240), numbers.get(4).power(4));

	}

	@Test
	public void rootTest() {
		ComplexNumber results[] = new ComplexNumber[3];
		
		results[0] = new ComplexNumber(1.69477038994546,0.606106530769629);
		results[1] = new ComplexNumber(-1.37228884801888,1.16466094588961);
	    results[2] = new ComplexNumber(-0.322481541926577,-1.77076747665924);
		
    	ComplexNumber[] resultsActual = numbers.get(3).root(3);
	    for(int i = 0; i < 3; i++) {
			assertEquals(results[i], resultsActual[i]);
	    }
	}

	@Test
	public void toStringTest() {
		assertEquals(new String("1.0+1.0i"), numbers.get(0).toString());
		assertEquals(new String("-1.0+1.0i"), numbers.get(1).toString());

	}
	
	@Test
	public void parseTest() {
		assertEquals(numbers.get(0).toString(),new String("1.0+1.0i"));
		assertEquals(numbers.get(1).toString(),new String("-1.0+1.0i"));
		assertEquals(numbers.get(2).toString(),new String("2.0+4.0i"));


	}
	
}
