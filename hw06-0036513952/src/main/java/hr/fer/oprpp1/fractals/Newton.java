package hr.fer.oprpp1.fractals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.oprpp1.math.Complex;
import hr.fer.oprpp1.math.ComplexPolynomial;
import hr.fer.oprpp1.math.ComplexRootedPolynomial;
import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;

public class Newton {
	

	
	/**
	 * Function which the viewer uses to generate the fractals
	 * @author Marko
	 *
	 */
	public static class MojProducer implements IFractalProducer {
		ComplexRootedPolynomial rootedPolynomial;
		ComplexPolynomial polynomial;
		ComplexPolynomial derived;
		
		public MojProducer(ComplexRootedPolynomial p) {
			this.rootedPolynomial = p;
			this.polynomial = p.toComplexPolynom();
			this.derived = polynomial.derive();
		}
		
		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax,
				int width, int height, long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {
			
			System.out.println("Zapocinjem izracun...");
			
			//the number of max iterations for each point
			int m = 16*16*16;
			short[] data = new short[width * height];

			//we go through all the pixels on the x-y plane
			for(int y = 0; y < height; y++) {
				if(cancel.get()) break;
				 for(int x = 0; x < width; x++) {
					 
					 //we see which imaginary number exists at that point of the imaginary plane
					 double cre = x / (width-1.0) * (reMax - reMin) + reMin;
					 double cim = (height-1.0-y) / (height-1) * (imMax - imMin) + imMin;
					 Complex zn = new Complex(cre, cim);
					 Complex znold;
					 int iter = 0;
					 double module;
					 
					 //while the module of the complex number is greater than 0.001 and we haven't reached the max number of iterations, repeat for this number
					 do {
						
						//calculation
						Complex numerator = polynomial.apply(zn);
						Complex denominator = derived.apply(zn);
						znold = zn;
						Complex fraction = numerator.divide(denominator);
						zn = zn.sub(fraction);
						module = znold.sub(zn).module();
						iter++;
						
					 } while(module > 0.001 && iter<m);
					 
					 //find the closest root to this number we have gotten and add it to the data short[]
					 int index = rootedPolynomial.indexOfClosestRootFor(zn, 0.002);
					 data[x+ y * width]=(short) (index+1);
					 
					 }
			}
						
			System.out.println("Racunanje gotovo. Idem obavijestiti promatraca tj. GUI!");
			observer.acceptResult(data, (short)(polynomial.order() + 1), requestNo);
		}
	
	
	public static void main(String[] args) {
		
		List<Complex> complexNumbers = new ArrayList<Complex>();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input;
		try {
			int rootNo = 1;
			//writes the prompt symbol and then asks for console input
			System.out.print("Root " +rootNo + " > ");
			input = br.readLine();
			
			//while we have input that end with a symbol that asks for more input, continue reading
			while(!input.equals("done")) {
				++rootNo;
				System.out.print("Root " +rootNo + " > ");
				complexNumbers.add(Complex.parse(input));
				input = br.readLine();
			}
		} catch (IOException e) {
			System.out.println("Error opening reader");
		}
		
		
		ComplexRootedPolynomial p = new ComplexRootedPolynomial(Complex.ONE, complexNumbers.toArray(new Complex[complexNumbers.size()]));
		
		FractalViewer.show(new MojProducer(p));
	}
	}
}