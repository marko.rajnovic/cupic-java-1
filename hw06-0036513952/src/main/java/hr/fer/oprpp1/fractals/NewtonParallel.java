package hr.fer.oprpp1.fractals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.oprpp1.math.Complex;
import hr.fer.oprpp1.math.ComplexPolynomial;
import hr.fer.oprpp1.math.ComplexRootedPolynomial;
import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;

public class NewtonParallel {
	
	public static class PosaoIzracuna implements Runnable {
			double reMin;
			double reMax;
			double imMin;
			double imMax;
			int width;
			int height;
			int yMin;
			int yMax;
			int m;
			short[] data;
			AtomicBoolean cancel;
			ComplexRootedPolynomial rootedPolynomial;
			ComplexPolynomial polynomial;
			ComplexPolynomial derived;
			public static PosaoIzracuna NO_JOB = new PosaoIzracuna();
			
			private PosaoIzracuna() {
			}
			
			public PosaoIzracuna(double reMin, double reMax, double imMin,
					double imMax, int width, int height, int yMin, int yMax, 
					int m, short[] data, AtomicBoolean cancel, ComplexRootedPolynomial p) {
				super();
				this.reMin = reMin;
				this.reMax = reMax;
				this.imMin = imMin;
				this.imMax = imMax;
				this.width = width;
				this.height = height;
				this.yMin = yMin;
				this.yMax = yMax;
				this.m = m;
				this.data = data;
				this.cancel = cancel;
				this.rootedPolynomial = p;
				this.polynomial = p.toComplexPolynom();
				this.derived = polynomial.derive();
				
			}
			
			@Override
			public void run() {
					
				//System.out.println("Thread:" + Thread.currentThread().getName());
				
				//go from yMin to yMax that the current thread was given and calculate the required data
				for(int y = yMin; y <= yMax; y++) {
					if(cancel.get()) break;
					 for(int x = 0; x < width; x++) {
						 
						//we see which imaginary number exists at that point of the imaginary plane
						 double cre = x / (width-1.0) * (reMax - reMin) + reMin;
						 double cim = (height-1.0-y) / (height-1) * (imMax - imMin) + imMin;
						 Complex zn = new Complex(cre, cim);
						 Complex znold;
						 int iter = 0;
						 double module;
						 
						 //while the module of the complex number is greater than 0.001 and we haven't reached the max number of iterations, repeat for this number
						 do {
							 
							//calculation
							Complex numerator = polynomial.apply(zn);
							Complex denominator = derived.apply(zn);
							znold = zn;
							Complex fraction = numerator.divide(denominator);
							zn = zn.sub(fraction);
							module = znold.sub(zn).module();
							iter++;
							
						 } while(module > 0.001 && iter<m);
						 
						//find the closest root to this number we have gotten and add it to the data short[]
						 int index = rootedPolynomial.indexOfClosestRootFor(zn, 0.002);
						 data[x+ y * width]=(short) (index+1);
						 
						 }
				}
		}
	}
	
	public static class MojProducer implements IFractalProducer {
		ComplexRootedPolynomial rootedPolynomial;
		ComplexPolynomial polynomial;
		int noWorkers;
		int noTracks;
		
		public MojProducer(ComplexRootedPolynomial p, int noWorkers, int noTracks) {
			this.rootedPolynomial = p;
			this.polynomial = p.toComplexPolynom();
			this.noWorkers = noWorkers;
			this.noTracks = noTracks;
		}
		
		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax,
				int width, int height, long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {
			System.out.println("Zapocinjem izracun...");
			
			//the number of max iterations for each point
			int m = 16*16*16;
			short[] data = new short[width * height];
			
			//we separate the height into n tracks by which the threads will go
			final int brojTraka = noTracks;
			int brojYPoTraci = height / brojTraka;
			
			//the queue that works with multiple threads
			final BlockingQueue<PosaoIzracuna> queue = new LinkedBlockingQueue<>();

			//we initialize the workers
			Thread[] radnici = new Thread[noWorkers];
			for(int i = 0; i < radnici.length; i++) {
				radnici[i] = new Thread(new Runnable() {
					@Override
					public void run() {
						while(true) {
							PosaoIzracuna p = null;
							
							//each worker tries to take a job, and if they take the poison pill, they end
							try {
								p = queue.take();
								if(p==PosaoIzracuna.NO_JOB) break;
							} catch (InterruptedException e) {
								continue;
							}
							//run the the calculation for the Posao izracuna
							p.run();
						}
					}
				});
			}
			
			//we start all the threads, they will wait until we queue the jobs
			for(int i = 0; i < radnici.length; i++) {
				radnici[i].start();
			}
			
			//we queue the jobs with the required values, such as the yMin and yMax that each thread recieves differently, as well as the rootedPolynomial
			for(int i = 0; i < brojTraka; i++) {
				int yMin = i*brojYPoTraci;
				int yMax = (i+1)*brojYPoTraci-1;
				if(i==brojTraka-1) {
					yMax = height-1;
				}
				PosaoIzracuna posao = new PosaoIzracuna(reMin, reMax, imMin,
						imMax, width, height, yMin, yMax, 
						m, data, cancel, rootedPolynomial);
				while(true) {
					try {
						queue.put(posao);
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			
			//we then place the poison pills to stop the threads from taking any more jobs
			for(int i = 0; i < radnici.length; i++) {
				while(true) {
					try {
						queue.put(PosaoIzracuna.NO_JOB);
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			
			
			//we wait until all the threads stop with their work to join them to the main one
			for(int i = 0; i < radnici.length; i++) {
				while(true) {
					try {
						radnici[i].join();
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			
			//calculation is done :)
			System.out.println("Racunanje gotovo. Idem obavijestiti promatraca tj. GUI!");
			observer.acceptResult(data, (short)(polynomial.order() + 1), requestNo);
		}
		
		public static void main(String[] args) {
			int noWorkers = Runtime.getRuntime().availableProcessors();
			int noTracks = -1;
			
			//parse the inputs for the number of workers and the number of tracks
			for(int i = 0; i < args.length; i++) {
				if(args[i].contains("--tracks")) {
					noTracks = Integer.parseInt(args[i].split("=")[1]);
				}
				if(args[i].contains("--workers")) {
					noWorkers = Integer.parseInt(args[i].split("=")[1]);
				}
				
				if(args[i].equals("-t")) {
					noTracks = Integer.parseInt(args[i+1]);
				}
				
				if(args[i].equals("-w")) {
					noWorkers = Integer.parseInt(args[i+1]);
				}
			}
			
			//if the number of tracks wasn't inputted, we default it to 4 * number of Processors
			if(noTracks == -1) {
				noTracks = 4 * noWorkers;
			}
			
			List<Complex> complexNumbers = new ArrayList<Complex>();
			
			//read the complex numbers and parse them from standart input
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String input;
			try {
				int rootNo = 1;
				//writes the prompt symbol and then asks for console input
				System.out.print("Root " +rootNo + " > ");
				input = br.readLine();
				
				//while we have input that end with a symbol that asks for more input, continue reading
				while(!input.equals("done")) {
					++rootNo;
					System.out.print("Root " +rootNo + " > ");
					complexNumbers.add(Complex.parse(input));
					input = br.readLine();
				}
			} catch (IOException e) {
				System.out.println("Error opening reader");
			}
			
			
			//make the complex rooted polynomial from the complex numbers we inputted
			ComplexRootedPolynomial p = new ComplexRootedPolynomial(Complex.ONE, complexNumbers.toArray(new Complex[complexNumbers.size()]));
			
			System.out.println("Number of threads: " + noWorkers);
			System.out.println("Number of jobs: " + noTracks);
			
			//send the polynomial p, number of workers and number of tracks to the producer, and initialize the FractalViewer
			FractalViewer.show(new MojProducer(p, noWorkers, noTracks));
		}
	}
	

}
