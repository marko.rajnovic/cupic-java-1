package hr.fer.oprpp1.math;


public class ComplexPolynomial {
	Complex[] factors;
	
	
	// constructor
	public ComplexPolynomial(Complex ...factors) {
		this.factors = factors;
	}
	
	/**
	 *  returns order of this polynom; eg. For (7+2i)z^3+2z^2+5z+1 returns 3
	 * @return
	 */
	public short order() {
		return (short) (factors.length - 1);
	}
	
	/**
	 *  computes a new polynomial this*p
	 * @param p
	 * @return
	 */
	public ComplexPolynomial multiply(ComplexPolynomial p) {
		int m = this.factors.length;
		int n = p.factors.length;
		
		//the multiplied polynomial will have m + n - 1 elements
		Complex[] result = new Complex[m + n - 1];
		
		//we initialize them all to 0
		for(int i = 0; i < m + n - 1; i++) {
			result[i] = new Complex(0,0);
		}
		
		//we multiply each with each and put them in their appropriate place
		for (int i = 0; i < m; i++)  { 
           for (int j = 0; j < n; j++)  
           { 
               result[i + j] = result[i+j].add(p.factors[j].multiply(this.factors[i])); 
           } 
        }
		
		return new ComplexPolynomial(result);
		   
	}
	
	/**
	 *  computes first derivative of this polynomial; for example, for
	 *  (7+2i)z^3+2z^2+5z+1 returns (21+6i)z^2+4z+5
	 * @return
	 */
	public ComplexPolynomial derive() {
		//result will have one element less
		Complex[] result = new Complex[factors.length - 1];
		
		//we multiply them by their power and make them have one power less
		for(int i = 0; i < factors.length - 1; i++) {
			result[i] = factors[i+1].multiply(new Complex(i+1,0));
		}
		
		return new ComplexPolynomial(result);
	}
	
	/**
	 *  computes polynomial value at given point z
	 * @param z
	 * @return
	 */
	public Complex apply(Complex z) {
		Complex result = new Complex(0,0);
		
		//we go through all the polynomials and power them by their power, and multiply them by the factor
		for(int i = 0; i < factors.length; i++) {
			Complex powered = z.power(i+1);
			Complex multiplied = factors[i].multiply(powered);
			
			result = result.add(multiplied);
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		String result = new String();
		for(int i = factors.length - 1; i > 0; i--) {
			result = result + "(" + factors[i] + ")" + "*z^" + (i) + "+";
		}
		
		result = result  + "(" + factors[0] + ")";
		return result;
	}
}
