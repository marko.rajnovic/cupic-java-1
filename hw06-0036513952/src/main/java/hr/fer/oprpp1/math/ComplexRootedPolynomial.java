package hr.fer.oprpp1.math;

public class ComplexRootedPolynomial {
	Complex constant;
	Complex[] roots;
	
	// constructor
	public ComplexRootedPolynomial(Complex constant, Complex ... roots) {
		this.constant = constant;
		this.roots = roots;
	}
	
	/**
	 *  computes polynomial value at given point z
	 * @param z
	 * @return
	 */
	public Complex apply(Complex z) {
		
		Complex[] subbedRoots = new Complex[roots.length];
		
		//we do (z - xn) for each n
		for(int i = 0; i < roots.length; i++) {
			subbedRoots[i] = z.sub(roots[i]);
		}
		
		Complex result = constant;
		
		//we multiply all of the subbed roots
		for(Complex root : subbedRoots) {
			result = result.multiply(root);
		}
		
		return result;
	}
	
	/**
	 *  converts this representation to ComplexPolynomial type
	 * @return
	 */
	public ComplexPolynomial toComplexPolynom() {
		ComplexPolynomial result = new ComplexPolynomial(constant);
		
		//we multiply all of the complex rooted polynomials by creating additional complex polynom and multiplying them one by one
		for(int i = 0; i < roots.length; i++) {
			ComplexPolynomial c = new ComplexPolynomial(roots[i], Complex.ONE);
			result = result.multiply(c);
		}
		return result;
	}
	
	@Override
	public String toString() {
		String result = new String("(" + constant + ")");
		
		for(Complex root : roots) {
			result = result + "*(z-(" + root + "))";
		}
		
		return result;
	}
	
	/**
	 *  finds index of closest root for given complex number z that is within
	 *  treshold; if there is no such root, returns -1
	 *  first root has index 0, second index 1, etc
	 * @param z
	 * @param treshold
	 * @return
	 */ 
	public int indexOfClosestRootFor(Complex z, double treshold) {
		
		//we set the min value to the max allowable
		double min = Double.MAX_VALUE;
		int index = 0;
		
		//we go through all the roots and check their distance from the point z and take the smallest one
		for(int i = 0; i < roots.length; i++) {
			double result = Math.pow(roots[i].getReal() - z.getReal(), 2) + Math.pow(roots[i].getImaginary() - z.getImaginary(), 2);
			
			if(min > result) {
				min = result;
				index = i;
			}
		}
		
		//we take sqrt of the smallest value
		double sqrtMin = Math.sqrt(min);
		
		//check if it is smaller than the treshold
		if(sqrtMin < treshold) {
			return index;
		} else {
			//if it isn't, return one
			return -1;
		}
	}
	}

