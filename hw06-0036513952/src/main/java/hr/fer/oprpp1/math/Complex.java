package hr.fer.oprpp1.math;

import java.util.Arrays;
import java.util.List;

public class Complex {
	double real;
	double imaginary;
	
	public static final Complex ZERO = new Complex(0,0);
	public static final Complex ONE = new Complex(1,0);
	public static final Complex ONE_NEG = new Complex(-1,0);
	public static final Complex IM = new Complex(0,1);
	public static final Complex IM_NEG = new Complex(0,-1);
	
	/**
	 * Constructor for Complex class. Takes "1+i1" would be written into the function as (1,1)
	 * @param real
	 * @param imaginary
	 */
	public Complex(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}
		
	public Complex() {
	}
	
	
	//GETTERS
	public double getReal() {
		return real;
	}

	public double getImaginary() {
		return imaginary;
	}

	/**
	 *  returns module of complex number
	 * @return
	 */
	public double module() {
		double module =  Math.sqrt(this.real * this.real + this.imaginary*this.imaginary);
		return module;
	}
	
	/**
	 * returns this*c
	 * @param c
	 * @return
	 */
	public Complex multiply(Complex c) {
		double ansImaginary = this.real * c.imaginary + this.imaginary * c.real;
		double ansReal = this.real * c.real - this.imaginary * c.imaginary;
		
		return new Complex(ansReal, ansImaginary);
	}
	
	/**
	 * returns this/c
	 * @param c
	 * @return
	 */
	public Complex divide(Complex c) {
		double numeratorReal = this.real * c.real + this.imaginary * c.imaginary;

		double numeratorImaginary = this.imaginary * c.real - this.real * c.imaginary;
		
		double denominator = Math.pow(c.real, 2) + Math.pow(c.imaginary, 2);
		
		return new Complex(numeratorReal / denominator, numeratorImaginary / denominator);
	}
	
	/**
	 *  returns this+c
	 * @param c
	 * @return
	 */
	public Complex add(Complex c) {
		return new Complex(this.real + c.real, this.imaginary + c.imaginary);
	}
	
	/**
	 * returns this-c
	 * @param c
	 * @return
	 */
	public Complex sub(Complex c) {
		return new Complex(this.real - c.real, this.imaginary - c.imaginary);
	}
	
	/**
	 * returns -this
	 * @return
	 */
	public Complex negate() {
		return new Complex(-this.real, -this.imaginary);
	}
	
	/**
	 *  returns this^n, n is non-negative integer
	 * @param n
	 * @return
	 */
	public Complex power(int n) {
		
		//we will use polar coordinates of a complex number to calculate its power
		double magnitude = this.getMagnitude();
		double angle = this.getAngle();
		
		magnitude = Math.pow(magnitude, n);
		angle = angle * n;
		
		//return the carthesian representation
		return fromMagnitudeAndAngle(magnitude, angle);
	}
	
	/**
	 *  returns n-th root of this, n is positive integer
	 * @param n
	 * @return
	 */
	public List<Complex> root(int n) {
		
		if(n <= 0) {
			throw new IllegalArgumentException("The power of the root must be a positive int");
		}
		
		//we will use polar coordinates of the complex number to calculate its roots
		double magnitude = this.getMagnitude();
		double angle = this.getAngle();
		
		//we adjust magnitude and angle based on the mathematical formulas for root calculation
		magnitude = Math.pow(magnitude, 1/(double)n);
		angle = angle / n;
		
		Complex[] numbers = new Complex[n];
		
		//there are n roots of a complex number c^(1/n)
		for(int i = 0; i < n; i++) {
			//return them all to carthesian representation
			numbers[i] = fromMagnitudeAndAngle(magnitude, angle + 2 * i * Math.PI / n);
		}
		
		//return all those roots as a list
		List<Complex> flat = Arrays.asList(numbers);
		return  flat;
	}
	
	/**
	 * Takes a string in a form of "a+bi", where a and b are any decimal numbers
	 * @param s
	 * @return
	 */
    public static Complex parse(String s) {
		Complex number;
		
		//Stores signs of the values
		boolean firstPositive = true;
		boolean secondPositive = true;
		
		//if the number contains an imaginary value, it is much more complex to implement. if the number doesn't contain "i", we just pass it to Double.parseDouble
		if(s.contains("i")) {
			
			//basic values that we can return immediately, as they are very common
			if(s.equals("i") || s.equals("i1")) {
				return Complex.IM;
			}
			if(s.equals("-i")) {
				return Complex.IM_NEG;
			}
			
			//checks if the first value is negative
			if (s.charAt(0) == '-') {
			    firstPositive = false;
			}
			
			//checks if the second value is negative
			if (s.substring(1).contains("-")) {
			    secondPositive = false;
			}
	
			//splits the values into multiple parts
			String[] splitted = s.split("[+-]");
			
			//removes leading + or - that was saved as empty String
			if (splitted[0].equals("")) {
			    splitted[0] = splitted[1];
			    
			    if(splitted.length > 2) {
			    	splitted[1] = splitted[2];
			    }
			}
			
			double realNumber = 0;
			double imaginaryNumber = 0;
			
			//if first number is imaginary
			if (splitted[0].contains("i")) {
			    imaginaryNumber = Double.parseDouble((firstPositive ? "+" : "-") + splitted[0].substring(1,splitted[0].length()));
			} else {
			    realNumber = Double.parseDouble((firstPositive ? "+" : "-") + splitted[0]);
			}
			
			//if there are 2 separate values
			if (splitted.length > 1 && !splitted[0].contains("i")) {
			    if (splitted[1].contains("i")) {
			    	
			    	if(splitted[1].trim().equals("i") || splitted[1].trim().equals("i1")) {
			    		imaginaryNumber = Double.parseDouble((secondPositive ? "+" : "-") + "1");
			    	} else if(splitted[1].trim().equals("i0")) {
			    		imaginaryNumber = Double.parseDouble((secondPositive ? "+" : "-") + "0");
			    	}
			    	else {
				        imaginaryNumber = Double.parseDouble((secondPositive ? "+" : "-") + splitted[1].substring(1,splitted[1].length()));
			    	}
			    	
			    }
			}
			
			number = new Complex(realNumber, imaginaryNumber);
		} else {
			number = new Complex(Double.parseDouble(s), 0);
		}
		
		return number;
	}
	
	@Override
	public String toString() {
		String arg1 = String.valueOf(this.real);
		String arg2 = String.valueOf(this.imaginary);
		
		if(this.real == 0) {
			return "0.0+" + "i" + arg2;
		} 
		
		if(this.imaginary == 0) {
			return arg1 + "+i0.0";
		}
		
		if(this.imaginary > 0) {
			return arg1 + "+i" + arg2;
		} else {
			return arg1 + "i" + arg2 ;
		}
	}


	/**
	 * get magnitude of the number in polar coordinate system
	 * @return
	 */
	private  double getMagnitude() {
		return Math.hypot(this.real, this.imaginary);
	}
	
	/**
	 * get the angle of the number in polar coordinate system. angle is [0, 2PI]
	 * @return
	 */
	private  double getAngle() {
		double angle = Math.atan2(this.imaginary, this.real);
		
		return angle;
	}
	
	/**
	 * Creates an instance of ComplexNumber class from a value written in polar coordinate system
	 * @param magnitude
	 * @param angle
	 * @return
	 */
	private static Complex fromMagnitudeAndAngle(double magnitude, double angle){
		return new Complex(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(imaginary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(real);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Complex other = (Complex) obj;
		if (Double.doubleToLongBits(imaginary) != Double.doubleToLongBits(other.imaginary))
			return false;
		if (Double.doubleToLongBits(real) != Double.doubleToLongBits(other.real))
			return false;
		return true;
	}
	
	
	
}