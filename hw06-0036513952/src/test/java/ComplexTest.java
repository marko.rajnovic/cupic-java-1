

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.math.Complex;

public class ComplexTest {
	
	@Test
	public void ComplexParseTest(){
		Complex one = Complex.parse("1");
		Complex oneIm = Complex.parse("i");
		Complex two = Complex.parse("1 +i1");
		Complex minus = Complex.parse("-17-i16");
		
		assertEquals(Complex.ONE, one);
		assertEquals(Complex.IM, oneIm);
		assertEquals(new Complex(1,1), two);
		assertEquals(new Complex(-17, -16), minus);
		
	}
	
}
