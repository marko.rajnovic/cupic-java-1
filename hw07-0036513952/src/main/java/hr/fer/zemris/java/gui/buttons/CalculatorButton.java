package hr.fer.zemris.java.gui.buttons;


import javax.swing.JButton;

/**
 * Class which all calculator buttons inherit from. Used to make sweeping changes to button style. 
 * @author Marko
 *
 */
public class CalculatorButton extends JButton{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public CalculatorButton(String text) {
		super(text);
	}


}
