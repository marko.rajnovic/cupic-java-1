package hr.fer.zemris.java.gui.calc.model;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleBinaryOperator;

/**
 * Class which acts as the "backend" of our Calculator
 * @author Marko
 *
 */
public class CalcModelImpl implements CalcModel {
	
	private boolean isEditable;
	private boolean isPositive;
	private String numberText;
	private Double numberInput;
	private String frozenValue;
	private Double activeOperand = null;
	DoubleBinaryOperator pendingOperation;
	
	List<CalcValueListener> listeners;
	
	public CalcModelImpl() {
		isEditable = true;
		isPositive = true;
		numberText = "";
		numberInput = 0.0;
		frozenValue = null;
		
		listeners = new ArrayList<>();
	}

	@Override
	public void addCalcValueListener(CalcValueListener l) {
		listeners.add(l);
	}

	@Override
	public void removeCalcValueListener(CalcValueListener l) {
		listeners.remove(l);
	}
	
	private void callListeners() {
		for(CalcValueListener l : listeners) {
			l.valueChanged(this);
		}
	}

	@Override
	public double getValue() {
		return numberInput;
	}

	@Override
	public void setValue(double value) {
		
		//we set the value to the desired one. if the value is "strange", act accordingly
		this.numberInput = value;
		
		if(value == Double.NaN) {
			this.numberText = "NaN";
		} else if (value == Double.POSITIVE_INFINITY) {
			this.numberText = "Infinity";
		} else if (value == Double.NEGATIVE_INFINITY) {
			this.numberText = "-Infinity";
		} else {
			this.numberText = String.valueOf(value);
		}
		
		//we can no longer edit
		this.isEditable = false;
		
		callListeners();
	}

	@Override
	public boolean isEditable() {
		return isEditable;
	}

	@Override
	public void clear() {
		this.numberInput = 0.0;
		this.numberText = "";
		this.isEditable = true;
		
		callListeners();
	}

	@Override
	public void clearAll() {
		clear();
		this.activeOperand = null;
		this.pendingOperation = null;
		callListeners();
	}

	@Override
	public void swapSign() throws CalculatorInputException {
		
		//change the signs on the numeric values
		this.isPositive = !this.isPositive;
		this.numberInput = -this.numberInput;
		
		if(!isEditable) {
			throw new CalculatorInputException("Cannot swap sign while the number isn't editable");
		}
		
		//change the sign on the string value
		if(this.numberText.contains("-")) {
			this.numberText = this.numberText.replace("-", "");
		} else if(!this.numberText.equals("")) {
			this.numberText = "-" + this.numberText;
		}
		
		//the value is no longer frozen
		this.frozenValue = null;
		
		callListeners();
	}

	@Override
	public void insertDecimalPoint() throws CalculatorInputException {
		
		if(!isEditable) {
			throw new CalculatorInputException("CalcModel is not currently editable");
		}
		
		String tempNumberText = new String(numberText);
		
		//tries to parse the double. If there is some sort of error, throws a CalculatorInputException
		try {
		tempNumberText += ".";
		numberInput = Double.parseDouble(tempNumberText);
		} catch(NumberFormatException e) {
			throw new CalculatorInputException(e.getMessage());
		}
		numberText = tempNumberText;
		this.frozenValue = null;
		
		callListeners();
	}

	@Override
	public void insertDigit(int digit) throws CalculatorInputException, IllegalArgumentException {
		if(!isEditable) {
			throw new CalculatorInputException("CalcModel is not currently editable");
		}
		
		String tempNumberText = numberText;
		
		//tries to add the recieved digit
		try {
		tempNumberText += digit;
		numberInput = Double.parseDouble(tempNumberText);
		
		//if we got an infinite value here, that means the number overflowed, and we need to throw an exception
		if(numberInput.isInfinite()) {
			throw new CalculatorInputException("Double value overflowed");
		}
		} catch(NumberFormatException e) {
			throw new CalculatorInputException(e.getMessage());
		}
		
		//add the proper numberText
		numberText = tempNumberText.replaceFirst("^0+(?!$)", "");
		if(numberText.startsWith(".")) {
			numberText = "0" + numberText;
		}
		this.frozenValue = null;
		
		callListeners();
	}

	@Override
	public boolean isActiveOperandSet() {
		return activeOperand != null;
	}

	@Override
	public double getActiveOperand() throws IllegalStateException {
		if(!isActiveOperandSet()) {
			throw new IllegalStateException("Active operand is not set");
		}
		
		return activeOperand;
	}

	@Override
	public void setActiveOperand(double activeOperand) {
		this.activeOperand = activeOperand;
		
		callListeners();
	}

	@Override
	public void clearActiveOperand() {
		this.activeOperand = null;
		
		callListeners();
	}

	@Override
	public DoubleBinaryOperator getPendingBinaryOperation() {
		return pendingOperation;
	}

	@Override
	public void setPendingBinaryOperation(DoubleBinaryOperator op) {
		this.pendingOperation = op;
		
		callListeners();
	}
	
	@Override
	public String toString() {
		
		//if we have a frozen value in the calculator, print it
		String s = new String();
		if(hasFrozenValue()) {
			if(!frozenValue.contains("-")) {
				s = (isPositive ? "" : "-") + frozenValue;
			} else {
				s = frozenValue;
			}
			
			return s;
		}
		
		//otherwise, print the numberText, or 0, -0
		if(!numberText.equals("")) {
			return numberText;
		} else if (isPositive) {
			return "0";
		} else {
			return "-0";
		}
	}

	@Override
	public void freezeValue(String value) {
		this.frozenValue = value;		
		
		callListeners();
	}

	@Override
	public boolean hasFrozenValue() {
		return frozenValue != null;
	}

}
