package hr.fer.zemris.java.gui.buttons;

import java.util.function.DoubleUnaryOperator;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

/**
 * Button which stores aunary operation
 * @author Marko
 *
 */
public class UnaryOperatorButton extends CalculatorButton{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * the primary operator
	 */
	DoubleUnaryOperator operator = null;
	
	/**
	 * the inverse operator
	 */
	DoubleUnaryOperator invOperator = null;
	
	/**
	 * The primary and inverse text
	 */
	String text, invText;
	

	/**
	 * boolean which stores if the binary operation has been inverted
	 */
	boolean inverted = false;
	
	/**
	 * Constructor which taxes the text, inverse text and the CalcModel on which we are doing our operation
	 * @param text
	 * @param invText
	 * @param model
	 */
	public UnaryOperatorButton(String text, String invText, CalcModel model) {
		super(text);
		this.text = text;
		this.invText = invText;
		
		//adds the listener that, on button press, will execute the unary operation, choosing between the primary and inverted one
		this.addActionListener((l) -> {
			double result = 0;
			if(inverted) {
				result = invOperator.applyAsDouble(model.getValue());
			} else {
				result = operator.applyAsDouble(model.getValue());
			}
			model.setValue(result);
			model.clearActiveOperand();
		});
	}
	
	/**
	 * Set the unary operator we wish to use on our button
	 * @param operator
	 */
	public void setUnaryOperator(DoubleUnaryOperator operator) {
		this.operator = operator;
	}
	
	/**
	 * Set the inverse unary operator we wish to use on our button
	 * @param operator
	 */
	public void setInvUnaryOperator(DoubleUnaryOperator invOperator) {
		this.invOperator = invOperator;
	}
	
	
	/**
	 * inverts the text and operator which will be executed
	 */
	public void invert() {
		inverted = !inverted;
		
		if(inverted) {
			this.setText(invText);
		} else {
			this.setText(text);
		}

	}
	
	

}
