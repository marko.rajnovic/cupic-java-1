package hr.fer.zemris.java.gui.buttons;

import java.util.List;

import javax.swing.JCheckBox;

/**
 * Checkbox which inverts all the buttons that are listening to it. It isn't a button, but for the sake of this program, it pretty much behaves like one
 * @author Marko
 *
 */
public class InvButton extends JCheckBox{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor which takes all the buttons that will listen to it, both Unary and Binary
	 * @param b
	 * @param l
	 */
	public InvButton(List<BinaryOperatorButton> b, List<UnaryOperatorButton> l) {
		
		this.setText("Inv");
		
		this.addItemListener((listener)->{
			for(BinaryOperatorButton button : b) {
				button.invert();
			}

			
			for(UnaryOperatorButton button : l) {
				button.invert();
			}
		});

	}

}
