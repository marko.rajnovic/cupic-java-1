package hr.fer.zemris.java.gui.calc.operators;

import java.util.function.DoubleBinaryOperator;

/**
 * Class which stores the Binary operations our calculator can do
 * @author Marko
 *
 */
public class BinaryOperators {
	public static final DoubleBinaryOperator ADD = (val1, val2) -> val1 + val2;
	public static final DoubleBinaryOperator SUB = (val1, val2) -> val1 - val2;
	public static final DoubleBinaryOperator MUL = (val1, val2) -> val1 * val2;
	public static final DoubleBinaryOperator DIV = (val1, val2) -> val1 / val2;
	
	public static final DoubleBinaryOperator X_TO_NTH = (val1, val2) -> Math.pow(val1, val2);
	public static final DoubleBinaryOperator NTH_ROOT_X = (val1,val2) -> Math.pow(val1, 1/val2);
	
}
