package hr.fer.zemris.java.gui.layouts;

/**
 * Exception thrown by the Calculator program
 * @author Marko
 *
 */
public class CalcLayoutException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CalcLayoutException() {
		super();
	}

	public CalcLayoutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CalcLayoutException(String message, Throwable cause) {
		super(message, cause);
	}

	public CalcLayoutException(String message) {
		super(message);
	}

	public CalcLayoutException(Throwable cause) {
		super(cause);
	}

	
	
	
}
