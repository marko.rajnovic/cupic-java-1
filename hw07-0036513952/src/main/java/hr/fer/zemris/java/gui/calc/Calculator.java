package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.buttons.BinaryOperatorButton;
import hr.fer.zemris.java.gui.buttons.CalculatorButton;
import hr.fer.zemris.java.gui.buttons.EqualsButton;
import hr.fer.zemris.java.gui.buttons.InvButton;
import hr.fer.zemris.java.gui.buttons.NumberButton;
import hr.fer.zemris.java.gui.buttons.UnaryOperatorButton;
import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalcModelImpl;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;
import hr.fer.zemris.java.gui.calc.operators.BinaryOperators;
import hr.fer.zemris.java.gui.calc.operators.UnaryOperators;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * JFrame that models a calculator
 * @author Marko
 *
 */
public class Calculator extends JFrame{
	
	/**
	 * CalcModel that will be used to implement the "backend" of the calculator
	 */
	CalcModel calcModel;
	
	/**
	 * Panel which will act as the "frontend" of the calculator
	 */
	JPanel panel;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * List which stores all the invertable unary buttons
	 */
	public List<UnaryOperatorButton> invertableUnaryButtons;
	
	/**
	 * List which stores all the invertable binary buttons
	 */
	public List<BinaryOperatorButton> invertableBinaryButtons;
	
	/**
	 * Stack used to push and pop
	 */
	Stack<Double> stack;
	
	public Calculator() {
		calcModel = new CalcModelImpl();
		stack = new Stack<Double>(); 
		invertableUnaryButtons = new ArrayList<UnaryOperatorButton>();
		invertableBinaryButtons = new ArrayList<BinaryOperatorButton>();
		
		//initialize the gui
		initGUI();
	}
	

	/**
	 * Method which initializes the graphical user interface of the calculator
	 */
	private void initGUI() {
		
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		panel = new JPanel(new CalcLayout(3));
		
		getContentPane().add(panel);
		
		addDisplay();
		addNumberButtons();
		addBinaryOperatorButtons();
		addUnaryOperatorButtons();
		addMiscButtons();
		pack();
	}
	
	
	/**
	 * Adds all the buttons that do the unary operations.
	 */
	private void addUnaryOperatorButtons() {
		
		
		//set the text, inv text and calcModel of the log button
		UnaryOperatorButton log = new UnaryOperatorButton("log", "10^x", calcModel);
		
		//sets the primary operator of the unary button
		log.setUnaryOperator(UnaryOperators.LOG);
		
		//sets the inverse operator of the unary button
		log.setInvUnaryOperator(UnaryOperators.TENTH_POWER);
		
		//add it to the panel
		panel.add(log, new RCPosition(3,1));
		
		UnaryOperatorButton ln = new UnaryOperatorButton("ln", "e^x", calcModel);
		ln.setUnaryOperator(UnaryOperators.LN);
		ln.setInvUnaryOperator(UnaryOperators.EXP_POWER);
		panel.add(ln, new RCPosition(4,1));
		
		UnaryOperatorButton sin = new UnaryOperatorButton("sin", "arcsin", calcModel);
		sin.setUnaryOperator(UnaryOperators.SIN);
		sin.setInvUnaryOperator(UnaryOperators.ASIN);
		panel.add(sin, new RCPosition(2,2));
		
		UnaryOperatorButton cos = new UnaryOperatorButton("cos", "arccos", calcModel);
		cos.setUnaryOperator(UnaryOperators.COS);
		cos.setInvUnaryOperator(UnaryOperators.ACOS);
		panel.add(cos, new RCPosition(3,2));
		
		UnaryOperatorButton tan = new UnaryOperatorButton("tan", "arctan", calcModel);
		tan.setUnaryOperator(UnaryOperators.TAN);
		tan.setInvUnaryOperator(UnaryOperators.ATAN);
		panel.add(tan, new RCPosition(4,2));
		
		UnaryOperatorButton ctg = new UnaryOperatorButton("ctg", "arcctg", calcModel);
		ctg.setUnaryOperator(UnaryOperators.CTG);
		ctg.setInvUnaryOperator(UnaryOperators.ACTG);
		panel.add(ctg, new RCPosition(5,2));
		
		UnaryOperatorButton reciprocal = new UnaryOperatorButton("1/x", null, calcModel);
		reciprocal.setUnaryOperator(UnaryOperators.RECIPROCAL);
		panel.add(reciprocal, new RCPosition(2,1));
		
		//adds all the invertable buttons to the list, so we can all make them listen to the InvButton
		invertableUnaryButtons.add(log);
		invertableUnaryButtons.add(ln);
		invertableUnaryButtons.add(sin);
		invertableUnaryButtons.add(cos);
		invertableUnaryButtons.add(tan);
		invertableUnaryButtons.add(ctg);
	}

	/**
	 * Adds all the buttons that don't specifically all follow a singular pattern
	 */
	private void addMiscButtons() {
		
		//the equals button
		EqualsButton equals = new EqualsButton(calcModel);
		panel.add(equals, new RCPosition(1,6));
		
		//the button responsible for inversion
		panel.add(new InvButton(invertableBinaryButtons, invertableUnaryButtons), new RCPosition(5,7));
		
		//push and pop buttons
		CalculatorButton popButton = new CalculatorButton("pop");
		popButton.addActionListener((l) -> {
			
			try {
			calcModel.setValue(stack.pop());
			} catch (EmptyStackException e) {
				System.out.println("No value in stack");
			}
		});
		panel.add(popButton, new RCPosition(4,7));
		
		CalculatorButton pushButton = new CalculatorButton("push");
		pushButton.addActionListener((l) -> {
			stack.push(calcModel.getValue());
		});
		panel.add(pushButton, new RCPosition(3,7));
		
		//clear button
		CalculatorButton clearButton = new CalculatorButton("clr");
		clearButton.addActionListener((l) -> {
			calcModel.clear();
		});
		panel.add(clearButton, new RCPosition(1,7));
		
		//reset buttons
		CalculatorButton resetButton = new CalculatorButton("reset");
		resetButton.addActionListener((l) -> {
			calcModel.clearAll();
		});
		panel.add(resetButton, new RCPosition(2,7));
		
		//button which swaps the sign
		CalculatorButton swapButton = new CalculatorButton("+/-");
		swapButton.addActionListener((l) -> {
			try {
			calcModel.swapSign();
			} catch (CalculatorInputException e) {
				System.out.println(e.getMessage());
			}
		});
		panel.add(swapButton, new RCPosition(5,4));
		
		//button which adds the decimal dot
		CalculatorButton dotButton = new CalculatorButton("." );
		dotButton.addActionListener((l) -> {
			try {
			calcModel.insertDecimalPoint();
			} catch (CalculatorInputException e) {
				System.out.println("Cannot add decimal point right now.");
			}
		});
		panel.add(dotButton, new RCPosition(5,5));
	}

	/**
	 * Method which adds the buttons of the binary operators
	 */
	private void addBinaryOperatorButtons() {
		BinaryOperatorButton add = new BinaryOperatorButton("+", null, calcModel);
		add.setBinaryOperator(BinaryOperators.ADD);
		panel.add(add, new RCPosition(5,6));
		
		BinaryOperatorButton sub = new BinaryOperatorButton("-", null, calcModel);
		sub.setBinaryOperator(BinaryOperators.SUB);
		panel.add(sub, new RCPosition(4,6));
		
		BinaryOperatorButton mul = new BinaryOperatorButton("*", null, calcModel);
		mul.setBinaryOperator(BinaryOperators.MUL);
		panel.add(mul, new RCPosition(3,6));
		
		BinaryOperatorButton div = new BinaryOperatorButton("/", null, calcModel);
		div.setBinaryOperator(BinaryOperators.DIV);
		panel.add(div, new RCPosition(2,6));
		
		//this is the only one with the inverse
		BinaryOperatorButton xToNth = new BinaryOperatorButton("x^n", "x^(1/n)", calcModel);
		xToNth.setBinaryOperator(BinaryOperators.X_TO_NTH);
		xToNth.setInvBinaryOperator(BinaryOperators.NTH_ROOT_X);
		panel.add(xToNth, new RCPosition(5,1));
		
		//add it here so it can be picked up by the InvButton
		invertableBinaryButtons.add(xToNth);
	}

	/**
	 * add the display 
	 */
	private void addDisplay() {
		JLabel display = new JLabel();
		display.setBackground(Color.yellow);
		display.setHorizontalAlignment(SwingConstants.RIGHT);
		display.setOpaque(true);
		display.setFont(display.getFont().deriveFont(30f));
		
		//listens for the CalcModel value changes
		this.calcModel.addCalcValueListener((m) -> {
			display.setText(m.toString());
		});
		
		panel.add(display, new RCPosition(1,1));
		
	}

	/**
	 * Add all the buttons responsible for adding digits
	 */
	private void addNumberButtons() {
		panel.add(new NumberButton(0, calcModel), new RCPosition(5,3));
		panel.add(new NumberButton(1, calcModel), new RCPosition(4,3));
		panel.add(new NumberButton(2, calcModel), new RCPosition(4,4));
		panel.add(new NumberButton(3, calcModel), new RCPosition(4,5));
		panel.add(new NumberButton(4, calcModel), new RCPosition(3,3));
		panel.add(new NumberButton(5, calcModel), new RCPosition(3,4));
		panel.add(new NumberButton(6, calcModel), new RCPosition(3,5));
		panel.add(new NumberButton(7, calcModel), new RCPosition(2,3));
		panel.add(new NumberButton(8, calcModel), new RCPosition(2,4));
		panel.add(new NumberButton(9, calcModel), new RCPosition(2,5));
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new Calculator().setVisible(true);
			}
		});
	}
}
