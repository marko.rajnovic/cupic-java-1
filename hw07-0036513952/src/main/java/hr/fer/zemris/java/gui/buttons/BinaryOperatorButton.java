package hr.fer.zemris.java.gui.buttons;

import java.util.function.DoubleBinaryOperator;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

/**
 * Button which stores a binary operation
 * @author Marko
 *
 */
public class BinaryOperatorButton extends CalculatorButton{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * the primary operator
	 */
	DoubleBinaryOperator operator = null;
	
	/**
	 * the inverse operator
	 */
	DoubleBinaryOperator invOperator = null;
	

	/**
	 * The primary and inverse text
	 */
	String text, invText;
	
	/**
	 * boolean which stores if the binary operation has been inverted
	 */
	boolean inverted = false;
	
	/**
	 * Constructor which taxes the text, inverse text and the CalcModel on which we are doing our operation
	 * @param text
	 * @param invText
	 * @param model
	 */
	public BinaryOperatorButton(String text, String invText, CalcModel model) {
		super(text);
		this.text = text;
		this.invText = invText;
		
		//add the listener which executes the operation on click
		this.addActionListener((l) -> {
			
			//checks if there is already a pending operation. If there is, executes it and moves on;
			if(model.getPendingBinaryOperation() != null) {
				double result = model.getPendingBinaryOperation().applyAsDouble(model.getActiveOperand(), model.getValue());
				model.setValue(result);
				model.clearActiveOperand();
				model.setPendingBinaryOperation(null);
			}
			
			//sets the active operand in the model
			model.setActiveOperand(model.getValue());
			
			//takes the primary or inverse operation, depending on if we inverted the values
			if(inverted) {
				model.setPendingBinaryOperation(this.invOperator);
			} else {
				model.setPendingBinaryOperation(this.operator);
			}
			
			//freeze the value in place
			model.freezeValue(model.toString());
			model.clear();
		});
	}
	
	/**
	 * Set the binary operator we wish to use on our button
	 * @param operator
	 */
	public void setBinaryOperator(DoubleBinaryOperator operator) {
		this.operator = operator;
	}
	
	/**
	 * Set the inverse binary operator we wish to use on our button
	 * @param invOperator
	 */
	public void setInvBinaryOperator(DoubleBinaryOperator invOperator) {
		this.invOperator = invOperator;
	}
	
	/**
	 * inverts the text and operator which will be executed
	 */
	public void invert() {
		
		//invert the boolean
		inverted = !inverted;
		
		//change the text on the button to the new value
		if(inverted) {
			this.setText(invText);
		} else {
			this.setText(text);
		}

	}
}
