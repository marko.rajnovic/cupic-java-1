package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * Class which contains the values that should be charted on screen
 * @author Marko
 *
 */
public class BarChart {
	List<XYValue> values;
	String xLab;
	String yLab;
	int yMin;
	int yMax;
	int yStep;

	/**
	 * 
	 * @param values of clas XYValue, used to represent the columns of the bar chart
	 * @param xLab label of x axis
	 * @param yLab label of y axis
	 * @param yMin minimum y which is shown
	 * @param yMax maximum y which is shown
	 * @param yStep step of y by which the graph jumps
	 */
	public BarChart(List<XYValue> values, String xLab, 
			String yLab, int yMin, int yMax, int yStep) {
		
		if(yMin < 0 || yMax < 0 || yMax < yMin) {
			throw new IllegalArgumentException("yMin and yMax cannot be negative, and yMax cannot be smaller than yMin");
		}
		
		for(XYValue val : values) {
			if(yMin > val.getY()) {
				throw new IllegalArgumentException("yMin is larger than some y values");
			}
		}
		
		this.values = values;
		this.xLab = xLab;
		this.yLab = yLab;
		this.yMin = yMin;
		this.yMax = yMax;
		this.yStep = yStep;
	}

	
	//GETTERS AND SETTERS
	public List<XYValue> getValues() {
		return values;
	}

	public String getxLab() {
		return xLab;
	}

	public String getyLab() {
		return yLab;
	}

	public int getyMin() {
		return yMin;
	}

	public int getyMax() {
		return yMax;
	}

	public int getyStep() {
		return yStep;
	}
	
	
	

}
