package hr.fer.zemris.java.gui.calc.operators;

import java.util.function.DoubleUnaryOperator;

/**
 * Class which stores the Unary operations our calculator can do
 * @author Marko
 *
 */
public class UnaryOperators {
	public static final DoubleUnaryOperator SIN = (val) -> Math.sin(val);
	public static final DoubleUnaryOperator COS = (val) -> Math.cos(val);
	public static final DoubleUnaryOperator TAN = (val) -> Math.tan(val);
	public static final DoubleUnaryOperator CTG = (val) -> 1/Math.tan(val);
	
	public static final DoubleUnaryOperator ASIN = (val) -> Math.asin(val);
	public static final DoubleUnaryOperator ACOS = (val) -> Math.acos(val);
	public static final DoubleUnaryOperator ATAN = (val) -> Math.atan(val);
	public static final DoubleUnaryOperator ACTG = (val) -> 1/Math.atan(val);
	
	public static final DoubleUnaryOperator RECIPROCAL =(val) -> 1/val;
	public static final DoubleUnaryOperator LOG =(val) -> Math.log10(val);
	public static final DoubleUnaryOperator LN =(val) -> 1/Math.atan(val);
	
	public static final DoubleUnaryOperator TENTH_POWER =(val) -> Math.pow(10, val);
	public static final DoubleUnaryOperator EXP_POWER =(val) -> Math.exp(val);
}
