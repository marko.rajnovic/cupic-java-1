package hr.fer.zemris.java.gui.buttons;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

/**
 * Button which mimics the equals button on regular calculators
 * @author Marko
 *
 */
public class EqualsButton extends CalculatorButton{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor which takes the model of the CalcModel we are using
	 * @param model
	 */
	public EqualsButton(CalcModel model) {
		super("=");
		
		//get the result using the operand we specified earlier
		this.addActionListener((l) -> {
			if(model.getPendingBinaryOperation() != null) {
				try {
				double result = model.getPendingBinaryOperation().applyAsDouble(model.getActiveOperand(), model.getValue());
				model.setValue(result);
				model.clearActiveOperand();
				model.setPendingBinaryOperation(null);
				} catch (IllegalStateException e) {
					System.out.println(e.getMessage());
				}
			}
		});
	}

}
