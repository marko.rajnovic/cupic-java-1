package hr.fer.zemris.java.gui.charts;

/**
 * Stores the xy value of the column for the bar chart
 * @author Marko
 *
 */
public class XYValue {
	int x;
	int y;
	
	public XYValue(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	

}
