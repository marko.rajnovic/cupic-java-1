package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.List;

import javax.swing.JComponent;

/**
 * JComponent which models a Bar Chart graph
 * @author Marko
 *
 */
public class BarChartComponent extends JComponent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * gap between elements
	 */
	private static final int gap = 10;
	
	/**
	 * gap between two columns
	 */
	private static final int rectGap = 1;
	
	/**
	 * number of columns
	 */
	private int noColumns;
	
	/**
	 * Number of steps
	 */
	private int noSteps;
	
	/**
	 * number of vertical lines
	 */
	private int noVerticalLines;
	
	/**
	 * number of horizontal lines
	 */
	private int noHorizontalLines;
	
	/**
	 * Start of the graph x axis
	 */
	private int xStart;
	
	/**
	 * Start of the graph y axis
	 */
	private int yStart;
	
	/**
	 * Height of the text that will be displayed
	 */
	private int textHeight;
	
	/**
	 * Size of the largest number in the bunch
	 */
	private int numberTextSize;
	
	private Dimension columnSegment;
	private Dimension windowSize;
	private BarChart chart;
	
	public BarChartComponent(BarChart chart) {
		this.chart = chart;
		
		//konvertirajmo ove komponente u nešto korisnije.
		noColumns = noVerticalLines = chart.getValues().size();
		noSteps = noHorizontalLines = (chart.getyMax() - chart.getyMin()) / chart.getyStep() + 1;
	}
	
	
	
	@Override
	protected void paintComponent(Graphics g) {
		
		
		//get text height
		textHeight = g.getFontMetrics().getHeight();
		
		
		
		//get width of the largest number on the y plane
		numberTextSize = g.getFontMetrics().stringWidth(String.valueOf(chart.getyMax()));
		
		//get the size of the window
		windowSize = getSize();
		
		//determine the point at which the graph starts
		xStart = gap + textHeight + gap + numberTextSize + gap;
		yStart = windowSize.height - (gap + textHeight + gap + textHeight + gap);
		
		columnSegment = new Dimension((windowSize.width - xStart) / noColumns - gap/2 , yStart / noSteps);
		

		
		
		drawGrid(g);
		drawNumbers(g);
		drawRectangles(g);
		drawXYLabels(g);
	}






	/**
	 * Draws the columns of the graph
	 * @param g
	 */
	private void drawRectangles(Graphics g) {
		
		//they will be red
		g.setColor(Color.RED);
		
		//get all the values of the graph
		List<XYValue> values = chart.getValues();
		
		//draw the rectangles appropriately
		for(XYValue value : values) {
			
			//chooses between the highest value allowable and the value of the current Y. Then it calculates the ratio of the rectangle based on the step
			double rectangleRatio = (((value.getY() < chart.getyMax()) ? value.getY() : chart.getyMax())  - chart.getyMin()) / (double) chart.getyStep();
			
			g.fill3DRect(xStart + (value.getX() - 1) * columnSegment.width + rectGap, 
					yStart - (int) Math.round(rectangleRatio * columnSegment.height) , 
					columnSegment.width - rectGap * 2, 
					(int) Math.round(rectangleRatio * columnSegment.height), true );
		
		}
		g.setColor(Color.black);
	}



	/**
	 * Draws the labels that are present on the x and y axis
	 * @param g
	 */
	private void drawXYLabels(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		
		//determines where the text on the x axis will start
		int xTextStart = (int) Math.round(xStart + ((windowSize.getWidth() - xStart) / 2) - g2d.getFontMetrics().stringWidth(chart.getxLab()) / 2);
		
		//draw the text on the x axis
		g2d.drawString(chart.xLab, xTextStart , windowSize.height - gap);
		
		//determiens where thex text on the y axis will start
		int yTextStart = (int) Math.round((yStart)/2 + g2d.getFontMetrics().stringWidth(chart.getyLab()) / 2);
		
		//rotate the text for y axis counter clockwise
		AffineTransform at = new AffineTransform();
		at.rotate(3 * Math.PI / 2);
		 
		//draw the text
		g2d.setTransform(at);
		g2d.drawString(chart.yLab, -yTextStart, gap/2 + textHeight );
		
	}


	/**
	 * Draws the numbers on the x and y axes
	 * @param g
	 */
	private void drawNumbers(Graphics g) {
		
		//sets the font to bold
		g.setFont(new Font("default", Font.BOLD, 16));
		
		//add the y values
		int currentYValue = chart.getyMin();
		for(int i = 0; i < noHorizontalLines; i++) {
			
			g.drawString(String.valueOf(currentYValue), xStart - g.getFontMetrics().stringWidth(String.valueOf(currentYValue)) - gap - gap/2 , yStart - i * columnSegment.height + g.getFontMetrics().getHeight() / 3);
			currentYValue += chart.getyStep();
		}
		
		//add the x values
		int currentXValue = 1;
		for(int i = 0; i < noVerticalLines; i++) {
			g.drawString(String.valueOf(currentXValue), (xStart + columnSegment.width / 2) + i * columnSegment.width, yStart + textHeight + gap);
			currentXValue += 1;
		}
		
		//resets the font
		g.setFont(new Font("default", Font.PLAIN, 16));
	}

	/**
	 * Draws the meshed grid across the graph, as well as the x and y axes
	 * @param g
	 */
	private void drawGrid(Graphics g) {
	
		g.drawLine(xStart, yStart, xStart, yStart - noHorizontalLines * columnSegment.height);
		g.drawLine(xStart, yStart, xStart + noVerticalLines * columnSegment.width + gap*2, yStart);
		
		//draws the triangle on the y axis
		int[] x1 = {xStart, xStart - gap/2, xStart + gap/2};
		int[] y1 = {yStart - noHorizontalLines * columnSegment.height, yStart - noHorizontalLines * columnSegment.height + gap/2, yStart - noHorizontalLines * columnSegment.height + gap/2};
		g.fillPolygon(x1, y1, 3);
		
		//draws the triangle on the x axis
		int[] x2 = {xStart + noVerticalLines * columnSegment.width + gap*2, xStart + noVerticalLines * columnSegment.width + gap * 3/2,  xStart + noVerticalLines * columnSegment.width + gap * 3/2};
		int[] y2 = {yStart, yStart + gap/2, yStart - gap/2};
		g.fillPolygon(x2,y2,3);
		
		//sets the color of the grid
		g.setColor(Color.orange);
		
		//draw horizontal lines
		for(int i = 1; i < noHorizontalLines; i++) {
			g.drawLine(xStart - gap, yStart - i * columnSegment.height , xStart + columnSegment.width * noColumns + gap, yStart - i* columnSegment.height);
		}
		
		//draw vertical lines
		for(int i = 1; i < noVerticalLines + 1; i++) {
			g.drawLine(xStart + i * columnSegment.width, yStart + gap , xStart + i * columnSegment.width, yStart - columnSegment.height * (noSteps - 1) - gap);
		}
		
		//set the color back to black
		g.setColor(Color.BLACK);
	}

}
