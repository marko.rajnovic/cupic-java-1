package hr.fer.zemris.java.gui.prim;

/**
 * Class which generates primary numbers, one after the other, after its function is called
 * @author Marko
 *
 */
public class PrimNumGenerator {
	private long currentNumber;

	public PrimNumGenerator() {
		currentNumber = 1;
	}
	
	/**
	 * Gets the next prime in sequence
	 * @return
	 */
	public long nextPrime() {
		if(currentNumber == 1 || currentNumber == 2) {
			return currentNumber++;
		}
		
		boolean foundPrime = false;
		while(true) {
			for(int i = 2; i < currentNumber; i++) {
				if(currentNumber % i != 0) {
					if(i == currentNumber - 1) {
						foundPrime = true;
					}
					continue;
				} else {
					break;
				}
			}
			if(foundPrime) {
				return currentNumber++;
			}
			++currentNumber;
		}
	}

}
