package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.nio.file.Paths;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * Class which executes the bar chart
 * @author Marko
 *
 */
public class BarChartDemo extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	BarChart model;
	
	public BarChartDemo(BarChart model) {
		this.model = model;
		initGUI();
	}
	
	/**
	 * Create the BarChartComponent and pass it the model.
	 */
	private void initGUI() {
		BarChartComponent k = new BarChartComponent(model);
		
		this.setTitle("BarChart");
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(k, BorderLayout.CENTER);
		this.setSize(500,500);
	}

	/**
	 * show the bar chart model on screen
	 * @param args
	 */
	public static void main(String[] args) {
		BarChart model = BarChartParser.parseChart(Paths.get(args[0]));
		
		 SwingUtilities.invokeLater(() -> {
	            JFrame frame = new BarChartDemo(model);
	            frame.setVisible(true);
		 });
	}
}
