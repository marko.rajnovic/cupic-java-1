package hr.fer.zemris.java.gui.layouts;

/**
 * Simple class that stores rows and columns. Includes a static parser
 * @author Marko
 *
 */
public class RCPosition {
	private int row;
	private int column;
	
	
	public RCPosition(int row, int column) {
		this.row = row;
		this.column = column;
		
	}


	public int getRow() {
		return row;
	}


	public int getColumn() {
		return column;
	}
	
	
	public static RCPosition parse(String text) {
		String[] splitted = text.split(",");
		
		int r = 0;
		int c = 0;
		
		try {
		r = Integer.parseInt(splitted[0].trim());
		c = Integer.parseInt(splitted[1].trim());
		} catch(NumberFormatException e) {
			throw new CalcLayoutException("String is not parsable");
		}
		return new RCPosition(r,c);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RCPosition other = (RCPosition) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
	
	
	
}
