package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager2;
import java.util.HashMap;
import java.util.Map;


/**
 * Layout that models the layout of a calculator
 * @author Marko
 *
 */
public class CalcLayout implements LayoutManager2 {
	
	/**
	 * The separation between different elements. Given to us by the user
	 */
	private int separationWidth;
	
	/**
	 * Number of columns
	 */
	private static final int COLUMN_COUNT = 7;
	
	/**
	 * Number of rows
	 */
	private static final int ROW_COUNT = 5;
	
	/**
	 * the 1,1 position which is different from all the rest
	 */
	private static final RCPosition START_POSITION = new RCPosition(1,1);
	
	/**
	 * All the positions of certain components
	 */
	private Map<Component, RCPosition> positions;
	
	
	public CalcLayout(int borderWidth) {
		this.separationWidth = borderWidth;
		
		positions = new HashMap<Component, RCPosition>();
	}
	
	public CalcLayout() {
		this(1);
	}
	
	@Override
	public void addLayoutComponent(String name, Component comp) {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Adds the component into the internal data structure of the CalcLayout class. It can later be assigned a place on the screen
	 */
	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		
		//check if any of the objects are ull
		if(constraints == null || comp == null) {
			throw new NullPointerException();
		}
		
		//if we didn't recieve a string or an RCPosition, throw an error
		if(!(constraints instanceof String || constraints instanceof RCPosition)) {
			throw new IllegalArgumentException("Constraint mora biti tipa String ili RCPosition");
		}
		
		RCPosition pos = null;
		
		//if we got a string, parse it. Otherwise, downcast the RCPosition
		if(constraints instanceof String) {
			pos = RCPosition.parse((String) constraints);
		} else {
			pos = (RCPosition) constraints;
		}
		
		//check if rows and columns are in bounds
		if(pos.getRow() < 1 || pos.getRow() > ROW_COUNT || pos.getColumn() < 1 || pos.getColumn() > COLUMN_COUNT) {
			throw new CalcLayoutException("Row and/or column number out of bounds");
		}
		
		//check if the position is encroaching on the place reserved for RCPosition(1,1)
		if(pos.getRow() == 1 && (pos.getColumn() > 1 && pos.getColumn() < 6)) {
			throw new CalcLayoutException("Not allowed to place anything in first row in columns 2-5");
		}
		
		//check if the position is already taken
		if(positions.containsValue(pos)) {
			throw new CalcLayoutException("There already is a component at that position");
		}
		
		positions.put(comp, pos);
	}
	
	
	@Override
	public void removeLayoutComponent(Component comp) {
		if(positions.containsKey(comp)) {
			positions.remove(comp);
		}
	}
	
	/**
	 * Returns the max of the whole windows' min, preferred max size, depending on the DimensionTransformer
	 * @param parent
	 * @param trans
	 * @return
	 */
	private Dimension getLayoutSizeOnParam(Container parent, DimensionTransformer trans) {
		int width = 0;
		int height = 0;
		
		//go through all the positions
		for(var entry : positions.entrySet()) {
			
			//get the position and dimensions of the component
			RCPosition pos = entry.getValue();
			Dimension dim = trans.getSomeSize(entry.getKey());
			
			if(pos.equals(START_POSITION)) {
				continue;
			}
			
			//find the max
			height = Math.max(height, dim.height);
			width = Math.max(width, dim.width);
		}
		
		//returns the dimensions of the layout that will satisfy the desired component size of the largest element
		return new Dimension(
				parent.getInsets().left + (COLUMN_COUNT - 1) * separationWidth + COLUMN_COUNT * width + parent.getInsets().right,
				parent.getInsets().top + (ROW_COUNT - 1) * separationWidth + ROW_COUNT * height + parent.getInsets().bottom
				);
	}
	
	/**
	 * Gets the preferred size of the components
	 * @param trans
	 * @return
	 */
	private Dimension getComponentLayoutSizeOnParam(DimensionTransformer trans) {
		int width = 0;
		int height = 0;
		
		//go through each component
		for(var entry : positions.entrySet()) {
			
			RCPosition pos = entry.getValue();
			Dimension dim = trans.getSomeSize(entry.getKey());
			
			if(pos.equals(START_POSITION)) {
				continue;
			}
			
			//finds max preferred layout size of the largest component
			height = Math.max(height, dim.height);
			width = Math.max(width, dim.width);
		}
		
		return new Dimension(width, height);
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		return getLayoutSizeOnParam(parent, (c) -> c.getPreferredSize());
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		return getLayoutSizeOnParam(parent, (c) -> c.getMinimumSize());
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		return getLayoutSizeOnParam(target, (c) -> c.getMaximumSize());
	}

	@Override
	public void layoutContainer(Container parent) {
		
		//dimensions of the components
		Dimension dimComponent = getComponentLayoutSizeOnParam((c) -> c.getPreferredSize());
		
		//preferred dimensions of the parent
		Dimension dimPreferred = preferredLayoutSize(parent);
		
		//divide the actual parent dimensions with its preferred ones. Get the ratio. We will use that ratio to set the size of the individual components
		double ratioX = (double) parent.getWidth() / dimPreferred.getWidth();
		double ratioY = (double) parent.getHeight() / dimPreferred.getHeight();
		
		//sets the new component size based on the recieved ratio
		dimComponent.setSize(dimComponent.getWidth() * ratioX, dimComponent.getHeight() * ratioY);
		
		//goes through all the components
		for(Map.Entry<Component, RCPosition> entry : positions.entrySet()) {
			
			//checks for the special case of the start component
			if(entry.getValue().equals(START_POSITION)) {
				entry.getKey().setBounds(0, 0, (int) Math.round(5 * dimComponent.getWidth() + 4 * separationWidth), (int) Math.round(dimComponent.getHeight()));
			} else {
				
				// set the bounds of all the components
				entry.getKey().setBounds(
					(entry.getValue().getColumn() - 1) * (dimComponent.width +separationWidth),   
					(entry.getValue().getRow()-1) * (dimComponent.height + separationWidth),
					dimComponent.width,
					dimComponent.height
					);
			}
		}
	}
	
	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}

	@Override
	public void invalidateLayout(Container target) {
	}

}
