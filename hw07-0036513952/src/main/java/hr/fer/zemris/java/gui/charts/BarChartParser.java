package hr.fer.zemris.java.gui.charts;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Class which takes a path and returns a BarChart based on the text document
 * @author Marko
 *
 */
public class BarChartParser {
	
	/**
	 * Number of lines we read. Anything below this won't be read
	 */
	public static final int NO_LINES = 6;

	
	/**
	 * Static function which takes a path and returns a bar chart
	 * @param path
	 * @return
	 */
	public static BarChart parseChart(Path path) {
		List<String> list = new ArrayList<>();
		
		//read the lines
		try (BufferedReader br = Files.newBufferedReader(path)) {
			for(int i = 0; i < NO_LINES; i++) {
				list.add(br.readLine());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//separate the lines
		BarChart chart = separateChart(list);
		return chart;
	}

	
	/**
	 * Private function which separates the elements in the chart
	 * @param list
	 * @return
	 */
	private static BarChart separateChart(List<String> list) {
		
		//get the labels
		String xLab = list.get(0);
		String yLab = list.get(1);
		
		List<XYValue> values = new ArrayList<>();
		
		//split the XYValues
		String[] splitted = list.get(2).split(" ");
		
		//go through all the XYValues and make XYValue objects
		for(String s : splitted) {
			String[] splittedValue = s.split(",");
			
			values.add(new XYValue(Integer.parseInt(splittedValue[0]), Integer.parseInt(splittedValue[1])));
		}
		
		//get min, max and step
		int yMin = Integer.parseInt(list.get(3));
		int yMax = Integer.parseInt(list.get(4));
		int yStep = Integer.parseInt(list.get(5));
		
		//return the newly created barchart
		return new BarChart(values,xLab, yLab, yMin, yMax, yStep);
	}

}
