package hr.fer.zemris.java.gui.buttons;


import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;

/**
 * Button whcih models a digit. When button is pressed, adds that button to the CalcModel
 * @author Marko
 *
 */
public class NumberButton extends CalculatorButton {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int number;

	/**
	 * Constructor which takes the number and the calcModel assigned to it
	 * @param number
	 * @param calcModel
	 */
	public NumberButton(int number, CalcModel calcModel) {
		//turn int to string and send it further
		super(Double.toString(number));
		this.number = number;
		
		//set the text and font
		this.setText(String.valueOf(number));
		this.setFont(this.getFont().deriveFont(30f));
		
		//insert digit to CalcModel on button press
		this.addActionListener((l) -> {
			try {
				calcModel.insertDigit(number);
			} catch (CalculatorInputException e) {
				System.out.println("Cannot currently add digits");
			}
		});
	}

	public int getNumber() {
		return number;
	}
	
}
