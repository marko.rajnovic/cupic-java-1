package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Dimension;

/**
 * Class used to model a transformer that will take a component object and return the Dimension, based on the function given to it.
 * @author Marko
 *
 */
public interface DimensionTransformer {
	public Dimension getSomeSize(Component c);
}
