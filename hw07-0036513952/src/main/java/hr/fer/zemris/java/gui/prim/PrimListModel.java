package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * List model that gets the next prime in sequence when its function "next" is called
 * @author Marko
 *
 */
public class PrimListModel implements ListModel<Long>{

	private PrimNumGenerator gen;
	private List<Long> elements = new ArrayList<>();
	private List<ListDataListener> observers = new ArrayList<>();
	
	@Override
	public void addListDataListener(ListDataListener l) {
		observers.add(l);
	}
	@Override
	public void removeListDataListener(ListDataListener l) {
		observers.remove(l);
	}
	
	@Override
	public int getSize() {
		return elements.size();
	}
	@Override
	public Long getElementAt(int index) {
		return elements.get(index);
	}
	
	
	/**
	 * Retrieves the next prime in sequence
	 */
	public void next() {
		int pos = elements.size();
		elements.add(gen.nextPrime());
		
		//notifies the objects listening that an element was added
		ListDataEvent event = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, pos, pos);
		for(ListDataListener l : observers) {
			l.intervalAdded(event);
		}
	}
	
	public PrimListModel() {
		gen = new PrimNumGenerator();
	}

}
