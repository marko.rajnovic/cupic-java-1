package hr.fer.zemris.java.gui.prim;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PrimTest {

	@Test
	public void primNumGeneratorTest() {
		PrimNumGenerator gen = new PrimNumGenerator();
		
		assertEquals(1, gen.nextPrime());
		assertEquals(2, gen.nextPrime());
		assertEquals(3, gen.nextPrime());
		assertEquals(5, gen.nextPrime());
		assertEquals(7, gen.nextPrime());
		assertEquals(11, gen.nextPrime());
		assertEquals(13, gen.nextPrime());
		assertEquals(17, gen.nextPrime());
	}

}
